#include "common_rpre.h"

struct CSR_REU {
  int n;
  int nnz; // total nnz
	int nnz0; // drop
	
	// FP32
	int* iafp32;
	int* jafp32;
	float* afp32;
	int nnzfp32;
	// FP64
	int* ia64;
	int* ja64;
	double* a64;
	int nnz64;
	// RPRE56
	int* ia56;
	int* ja56;
	rpre56barray a56;
	int nnz56;

	//POSITIVE
	// RPRE8
	int* ia8;
	int* ja8;
	rpre8barray a8;
	int nnz8;
	// RPRE16
	int* ia16;
	int* ja16;
	rpre16barray a16;
	int nnz16;
	// RPRE24
	int* ia24;
	int* ja24;
	rpre24barray a24;
	int nnz24;
	// RPRE32
	int* ia32;
	int* ja32;
	rpre32barray a32;
	int nnz32;
	// RPRE40
	int* ia40;
	int* ja40;
	rpre40barray a40;
	int nnz40;
	// RPRE48
	int* ia48;
	int* ja48;
	rpre48barray a48;
	int nnz48;

	//NEGATIVE
	// RPRE8
	int* ia8_1;
	int* ja8_1;
	rpre8barray a8_1;
	int nnz8_1;
	// RPRE16
	int* ia16_1;
	int* ja16_1;
	rpre16barray a16_1;
	int nnz16_1;
	// RPRE24
	int* ia24_1;
	int* ja24_1;
	rpre24barray a24_1;
	int nnz24_1;
	// RPRE32
	int* ia32_1;
	int* ja32_1;
	rpre32barray a32_1;
	int nnz32_1;
	// RPRE40
	int* ia40_1;
	int* ja40_1;
	rpre40barray a40_1;
	int nnz40_1;
	// RPRE48
	int* ia48_1;
	int* ja48_1;
	rpre48barray a48_1;
	int nnz48_1;
};

void alloc_CSR_REU (struct CSR_REU* Ap, const int n, const int nnz) {
  Ap->n = n;
  Ap->nnz = nnz;
  
	// FP32
	Ap->afp32 = (float*) malloc ((nnz+1) * sizeof(float));
	Ap->iafp32 = (int*) malloc ((n+1) * sizeof(int));
	Ap->jafp32 = (int*) malloc ((nnz) * sizeof(int));
	// FP64
	Ap->a64 = (double*) malloc ((nnz+1) * sizeof(double));
	Ap->ia64 = (int*) malloc ((n+1) * sizeof(int));
	Ap->ja64 = (int*) malloc ((nnz) * sizeof(int));
	// RPRE56
	rpreMalloc (Ap->a56, nnz+1);
	Ap->ia56 = (int*) malloc ((n+1) * sizeof(int));
	Ap->ja56 = (int*) malloc ((nnz) * sizeof(int));
	
	// RPRE8
	rpreMalloc (Ap->a8, nnz+1);
	Ap->ia8 = (int*) malloc ((n+1) * sizeof(int));
	Ap->ja8 = (int*) malloc ((nnz) * sizeof(int));
	// RPRE16
	rpreMalloc (Ap->a16, nnz+1);
	Ap->ia16 = (int*) malloc ((n+1) * sizeof(int));
	Ap->ja16 = (int*) malloc ((nnz) * sizeof(int));
	// RPRE24
	rpreMalloc (Ap->a24, nnz+1);
	Ap->ia24 = (int*) malloc ((n+1) * sizeof(int));
	Ap->ja24 = (int*) malloc ((nnz) * sizeof(int));
	// RPRE32
	rpreMalloc (Ap->a32, nnz+1);
	Ap->ia32 = (int*) malloc ((n+1) * sizeof(int));
	Ap->ja32 = (int*) malloc ((nnz) * sizeof(int));
	// RPRE40
	rpreMalloc (Ap->a40, nnz+1);
	Ap->ia40 = (int*) malloc ((n+1) * sizeof(int));
	Ap->ja40 = (int*) malloc ((nnz) * sizeof(int));
	// RPRE48
	rpreMalloc (Ap->a48, nnz+1);
	Ap->ia48 = (int*) malloc ((n+1) * sizeof(int));
	Ap->ja48 = (int*) malloc ((nnz) * sizeof(int));
	
	//NEGATIVE
	// RPRE8
	rpreMalloc (Ap->a8_1, nnz+1);
	Ap->ia8_1 = (int*) malloc ((n+1) * sizeof(int));
	Ap->ja8_1 = (int*) malloc ((nnz) * sizeof(int));
	// RPRE16
	rpreMalloc (Ap->a16_1, nnz+1);
	Ap->ia16_1 = (int*) malloc ((n+1) * sizeof(int));
	Ap->ja16_1 = (int*) malloc ((nnz) * sizeof(int));
	// RPRE24
	rpreMalloc (Ap->a24_1, nnz+1);
	Ap->ia24_1 = (int*) malloc ((n+1) * sizeof(int));
	Ap->ja24_1 = (int*) malloc ((nnz) * sizeof(int));
	// RPRE32
	rpreMalloc (Ap->a32_1, nnz+1);
	Ap->ia32_1 = (int*) malloc ((n+1) * sizeof(int));
	Ap->ja32_1 = (int*) malloc ((nnz) * sizeof(int));
	// RPRE40
	rpreMalloc (Ap->a40_1, nnz+1);
	Ap->ia40_1 = (int*) malloc ((n+1) * sizeof(int));
	Ap->ja40_1 = (int*) malloc ((nnz) * sizeof(int));
	// RPRE48
	rpreMalloc (Ap->a48_1, nnz+1);
	Ap->ia48_1 = (int*) malloc ((n+1) * sizeof(int));
	Ap->ja48_1 = (int*) malloc ((nnz) * sizeof(int));
}

void init_CSR_REU(struct CSR_REU* Ap, uint64_t* bias64, uint32_t* bias32) {
	Ap->nnz0 = 0; // drop
  Ap->nnz = 0; // total nnz
	// FP32
	Ap->afp32[0] = 1.;
	Ap->iafp32[0] = 0;
	Ap->jafp32[0] = 0;
	Ap->nnzfp32 = 0;
	// FP64
	Ap->a64[0] = 1.;
	Ap->ia64[0] = 0;
	Ap->ja64[0] = 0;
	Ap->nnz64 = 0;
	// RPRE56
	FpToRpArray_s (1., Ap->a56, 0, bias64[6]);
	Ap->ia56[0] = 0;
	Ap->ja56[0] = 0;
	Ap->nnz56 = 0;
	
	//POSITIVE
	// RPRE8B
	Fp32ToRpArray_s(float(1.), Ap->a8, 0, bias32[0]);
	Ap->ia8[0] = 0;
	Ap->ja8[0] = 0;
	Ap->nnz8 = 0;
	// RPRE16B
	Fp32ToRpArray_s(float(1.), Ap->a16, 0, bias32[1]);
	Ap->ia16[0] = 0;
	Ap->ja16[0] = 0;
	Ap->nnz16 = 0;
	// RPRE24B
	Fp32ToRpArray_s (float(1.), Ap->a24, 0, bias32[2]);
	Ap->ia24[0] = 0;
	Ap->ja24[0] = 0;
	Ap->nnz24 = 0;
	// RPRE32B
	FpToRpArray_s (1., Ap->a32, 0, bias64[3]);
	Ap->ia32[0] = 0;
	Ap->ja32[0] = 0;
	Ap->nnz32 = 0;
	// RPRE40B
	FpToRpArray_s (1., Ap->a40, 0, bias64[4]);
	Ap->ia40[0] = 0;
	Ap->ja40[0] = 0;
	Ap->nnz40 = 0;
	// RPRE48B
	FpToRpArray_s (1., Ap->a48, 0, bias64[5]);
	Ap->ia48[0] = 0;
	Ap->ja48[0] = 0;
	Ap->nnz48 = 0;
	
	//NEGATIVE
	// RPRE8B
	Fp32ToRpArray_s(float(1.), Ap->a8_1, 0, bias32[0]);
	Ap->ia8_1[0] = 0;
	Ap->ja8_1[0] = 0;
	Ap->nnz8_1 = 0;
	// RPRE16B
	Fp32ToRpArray_s(float(1.), Ap->a16_1, 0, bias32[1]);
	Ap->ia16_1[0] = 0;
	Ap->ja16_1[0] = 0;
	Ap->nnz16_1 = 0;
	// RPRE24B
	Fp32ToRpArray_s (float(1.), Ap->a24_1, 0, bias32[2]);
	Ap->ia24_1[0] = 0;
	Ap->ja24_1[0] = 0;
	Ap->nnz24_1 = 0;
	// RPRE32B
	FpToRpArray_s (1., Ap->a32_1, 0, bias64[3]);
	Ap->ia32_1[0] = 0;
	Ap->ja32_1[0] = 0;
	Ap->nnz32_1 = 0;
	// RPRE40B
	FpToRpArray_s (1., Ap->a40_1, 0, bias64[4]);
	Ap->ia40_1[0] = 0;
	Ap->ja40_1[0] = 0;
	Ap->nnz40_1 = 0;
	// RPRE48B
	FpToRpArray_s (1., Ap->a48_1, 0, bias64[5]);
	Ap->ia48_1[0] = 0;
	Ap->ja48_1[0] = 0;
	Ap->nnz48_1 = 0;
}


void free_CSR_REU (struct CSR_REU* Ap) {
	// FP32
	// FP32
	free(Ap->afp32);
	free(Ap->iafp32);
	free(Ap->jafp32);
	// FP64
	free(Ap->a64);
	free(Ap->ia64);
	free(Ap->ja64);
	// RPRE56
	rpreFree(Ap->a56);
	free(Ap->ia56);
	free(Ap->ja56);
	
	// RPRE8
	rpreFree(Ap->a8);
	free(Ap->ia8);
	free(Ap->ja8);
	// RPRE16
	rpreFree(Ap->a16);
	free(Ap->ia16);
	free(Ap->ja16);
	// RPRE24
	rpreFree(Ap->a24);
	free(Ap->ia24);
	free(Ap->ja24);
	// RPRE32
	rpreFree(Ap->a32);
	free(Ap->ia32);
	free(Ap->ja32);
	// RPRE40
	rpreFree(Ap->a40);
	free(Ap->ia40);
	free(Ap->ja40);
	// RPRE48
	rpreFree(Ap->a48);
	free(Ap->ia48);
	free(Ap->ja48);
	
	//NEGATIVE
	// RPRE8
	rpreFree(Ap->a8_1);
	free(Ap->ia8_1);
	free(Ap->ja8_1);
	// RPRE16
	rpreFree(Ap->a16_1);
	free(Ap->ia16_1);
	free(Ap->ja16_1);
	// RPRE24
	rpreFree(Ap->a24_1);
	free(Ap->ia24_1);
	free(Ap->ja24_1);
	// RPRE32
	rpreFree(Ap->a32_1);
	free(Ap->ia32_1);
	free(Ap->ja32_1);
	// RPRE40
	rpreFree(Ap->a40_1);
	free(Ap->ia40_1);
	free(Ap->ja40_1);
	// RPRE48
	rpreFree(Ap->a48_1);
	free(Ap->ia48_1);
	free(Ap->ja48_1);
}

void convert_element_CSR_REU(int i, int jcnk, double Ak, struct CSR_REU* Ap, int k, double* bornes, uint64_t* bias64, uint32_t* bias32, double bornefp32) {
  int kX;
	if (fabs(Ak) < bornes[0]){
		//DROP
		Ap->nnz0++;
		return;
	}
	if (bornes[0] <= fabs(Ak) and fabs(Ak) < bornes[1]){
		//RPRE8
		if (Ak>0){
			kX = Ap->ia8[i + 1];
			Ap->ia8[i + 1] = Ap->ia8[i + 1] + 1;
			Ap->ja8[kX] = jcnk;
			Fp32ToRpArray_s(float(Ak), Ap->a8, kX, bias32[0]);
			Ap->nnz8++;
		}
		else{
			kX = Ap->ia8_1[i + 1];
			Ap->ia8_1[i + 1] = Ap->ia8_1[i + 1] + 1;
			Ap->ja8_1[kX] = jcnk;
			Fp32ToRpArray_s(float(Ak), Ap->a8_1, kX, bias32[0]);
			Ap->nnz8_1++;			
		}
		Ap->nnz = Ap->nnz + 1;
		return;
	}
	if (bornes[1] <= fabs(Ak) and fabs(Ak) < bornes[2]){
		//RPRE16
		if(Ak>0){
			kX = Ap->ia16[i + 1];
			Ap->ia16[i + 1] = Ap->ia16[i + 1] + 1;
			Ap->ja16[kX] = jcnk;
			Fp32ToRpArray_s(float(Ak), Ap->a16, kX, bias32[1]);
			Ap->nnz16++;
		}
		else{
			kX = Ap->ia16_1[i + 1];
			Ap->ia16_1[i + 1] = Ap->ia16_1[i + 1] + 1;
			Ap->ja16_1[kX] = jcnk;
			Fp32ToRpArray_s(float(Ak), Ap->a16_1, kX, bias32[1]);
			Ap->nnz16_1++;		
		}
		Ap->nnz = Ap->nnz + 1;
		return;
	}
#ifdef USERE24
	if (bornes[2] <= fabs(Ak) and fabs(Ak) < bornes[3]){
		// RPRE24 21 bits de mantisse
		if(Ak>0){
		kX = Ap->ia24[i + 1];
		Ap->ia24[i + 1] = Ap->ia24[i + 1] + 1;
		Ap->ja24[kX] = jcnk;
		Fp32ToRpArray_s(float(Ak), Ap->a24, kX, bias32[2]);
		Ap->nnz24++;
		}
		else{
			kX = Ap->ia24_1[i + 1];
			Ap->ia24_1[i + 1] = Ap->ia24_1[i + 1] + 1;
			Ap->ja24_1[kX] = jcnk;
			Fp32ToRpArray_s(float(Ak), Ap->a24_1, kX, bias32[2]);
			Ap->nnz24_1++;
		}
		Ap->nnz = Ap->nnz + 1;
		return;
	}
#endif
#ifdef USEFP32
	if (fabs(Ak) < bornefp32){
		// FP32 24 bits de mantisse
		kX = Ap->iafp32[i + 1];
		Ap->iafp32[i + 1] = Ap->iafp32[i + 1] + 1;
		Ap->jafp32[kX] = jcnk;
		Ap->afp32[kX] = float(Ak);
		Ap->nnzfp32++;
		Ap->nnz = Ap->nnz + 1;
		return;
	}
#endif
	if (bornes[3] <= fabs(Ak) and fabs(Ak) < bornes[4]){
		// RPRE32 29 bits de mantisse
		if(Ak>0){
		kX = Ap->ia32[i + 1];
		Ap->ia32[i + 1] = Ap->ia32[i + 1] + 1;
		Ap->ja32[kX] = jcnk;
		FpToRpArray_s(Ak, Ap->a32, kX, bias64[3]);
		Ap->nnz32++;
		}
		else{
		kX = Ap->ia32_1[i + 1];
		Ap->ia32_1[i + 1] = Ap->ia32_1[i + 1] + 1;
		Ap->ja32_1[kX] = jcnk;
		FpToRpArray_s(Ak, Ap->a32_1, kX, bias64[3]);
		Ap->nnz32_1++;		
		}
		Ap->nnz = Ap->nnz + 1;
		return;
	}
	if (bornes[4] <= fabs(Ak) and fabs(Ak) < bornes[5]){
		// RPRE40
		if(Ak>0){
		kX = Ap->ia40[i + 1];
		Ap->ia40[i + 1] = Ap->ia40[i + 1] + 1;
		Ap->ja40[kX] = jcnk;
		FpToRpArray_s(Ak, Ap->a40, kX, bias64[4]);
		Ap->nnz40++;
		}
		else{
		kX = Ap->ia40_1[i + 1];
		Ap->ia40_1[i + 1] = Ap->ia40_1[i + 1] + 1;
		Ap->ja40_1[kX] = jcnk;
		FpToRpArray_s(Ak, Ap->a40_1, kX, bias64[4]);
		Ap->nnz40_1++;			
		}
		Ap->nnz = Ap->nnz + 1;
		return;
	}
	if (bornes[5] <= fabs(Ak) and fabs(Ak) < bornes[6]){
		// RPRE48
		if(Ak>0){
		kX = Ap->ia48[i + 1];
		Ap->ia48[i + 1] = Ap->ia48[i + 1] + 1;
		Ap->ja48[kX] = jcnk;
		FpToRpArray_s(Ak, Ap->a48, kX, bias64[5]);
		Ap->nnz48++;
		}
		else{
		kX = Ap->ia48_1[i + 1];
		Ap->ia48_1[i + 1] = Ap->ia48_1[i + 1] + 1;
		Ap->ja48_1[kX] = jcnk;
		FpToRpArray_s(Ak, Ap->a48_1, kX, bias64[5]);
		Ap->nnz48_1++;
		}
		Ap->nnz = Ap->nnz + 1;
		return;
	}
	#ifdef USERE56
	if (bornes[6] <= fabs(Ak) and fabs(Ak) < bornes[7]){
		// RPRE56
		kX = Ap->ia56[i + 1];
		Ap->ia56[i + 1] = Ap->ia56[i + 1] + 1;
		Ap->ja56[kX] = jcnk;
		FpToRpArray(Ak, Ap->a56, kX, bias64[6]);
		Ap->nnz56++;
		Ap->nnz = Ap->nnz + 1;
		return;
	}
	#endif
	// FP64
	kX = Ap->ia64[i + 1];
	Ap->ia64[i + 1] += 1;
	Ap->ja64[kX] = jcnk;
	Ap->a64[kX] = Ak;
	Ap->nnz64++;
	Ap->nnz += 1;
	return;
}

void convert_CSR_RPREU(struct CSR_REU* Ap, int* irn, int nnz, double* A, int* jcn, int t, uint64_t* bias64, uint32_t* bias32) {
	double bornes[8]={0.,0.,0.,0.,0.,0.,0.,0.};
	bornes[0] = pow(2, -t);
	bornes[1] = pow(2,6-t);
	bornes[2] = pow(2,14-t);
	bornes[3] = pow(2,22-t);
	bornes[4] = pow(2,30-t);
	bornes[5] = pow(2,38-t);
	bornes[6] = pow(2,46-t);
	bornes[7] = pow(2,53-t);
	bias64[0] = rpre_getbias64(bornes[0]);
	bias32[0] = rpre_getbias32(float(bornes[0]));
	bias64[1] = bias64[0]+6;
	bias32[1] = bias32[0]+6;
	for (int i = 2; i<7; i++){
		bias64[i] = bias64[i-1]+8;
		bias32[i] = bias32[i-1]+8;
	}
	double bornefp32 = pow(2,24-t);

  for (int i = 0; i < Ap->n; i++) {
		Ap->iafp32[i + 1] = Ap->iafp32[i];
		Ap->ia64[i + 1] = Ap->ia64[i];
		Ap->ia56[i + 1] = Ap->ia56[i];
		//POSITIVE
		Ap->ia8[i + 1] = Ap->ia8[i];
		Ap->ia16[i + 1] = Ap->ia16[i];
		Ap->ia24[i + 1] = Ap->ia24[i];
		Ap->ia32[i + 1] = Ap->ia32[i];
		Ap->ia40[i + 1] = Ap->ia40[i];
		Ap->ia48[i + 1] = Ap->ia48[i];
		//NEGATIVE
		Ap->ia8_1[i + 1] = Ap->ia8_1[i];
		Ap->ia16_1[i + 1] = Ap->ia16_1[i];
		Ap->ia24_1[i + 1] = Ap->ia24_1[i];
		Ap->ia32_1[i + 1] = Ap->ia32_1[i];
		Ap->ia40_1[i + 1] = Ap->ia40_1[i];
		Ap->ia48_1[i + 1] = Ap->ia48_1[i];

    for (int k = irn[i]; k < irn[i + 1]; k++) {
      convert_element_CSR_REU(i, jcn[k], A[k], Ap, k, bornes, bias64, bias32, bornefp32);
		}
	}
}

// Adaptive-precision SpMV
void ax_rpre_s (int n, CSR_REU Ap, double* x, double* y, uint64_t* bias64, uint32_t* bias32) {
  #pragma omp parallel for
  for (int i = 0; i < n; i++) {
		double tmpfp32 = 0.;
		double tmpfp64 = 0.;
		double tmpre56 = 0.;
		
		double tmpre8 = 0.;
		double tmpre16 = 0.;
		double tmpre24 = 0.;	
		double tmpre32 = 0.;	
		double tmpre40 = 0.;
		double tmpre48 = 0.;
		
		double tmpre8_1 = 0.;
		double tmpre16_1 = 0.;
		double tmpre24_1 = 0.;
		double tmpre32_1 = 0.;
		double tmpre40_1 = 0.;
		double tmpre48_1 = 0.;
			
		// RPRE8
    for (int k = Ap.ia8[i]; k < Ap.ia8[i+1]; k++) {
        float aij_r = RpArrayToFp32_s(Ap.a8, k);
        tmpre8 += aij_r * x[Ap.ja8[k]];
		}
    for (int k = Ap.ia8_1[i]; k < Ap.ia8_1[i+1]; k++) {
        float aij_r = RpArrayToFp32_s(Ap.a8_1, k);
        tmpre8_1 += aij_r * x[Ap.ja8_1[k]];
		}
		// RPRE16
    for (int k = Ap.ia16[i]; k < Ap.ia16[i+1]; k++) {
        float aij_r = RpArrayToFp32_s(Ap.a16, k);
        tmpre16 += aij_r * x[Ap.ja16[k]];
    }
    for (int k = Ap.ia16_1[i]; k < Ap.ia16_1[i+1]; k++) {
        float aij_r = RpArrayToFp32_s(Ap.a16_1, k);
        tmpre16_1 += aij_r * x[Ap.ja16_1[k]];
    }
		// RPRE24
		#ifdef USERE24
    for (int k = Ap.ia24[i]; k < Ap.ia24[i+1]; k++) {
        float aij_r = RpArrayToFp32_s(Ap.a24, k);
        tmpre24 += aij_r * x[Ap.ja24[k]];
		}
    for (int k = Ap.ia24_1[i]; k < Ap.ia24_1[i+1]; k++) {
        float aij_r = RpArrayToFp32_s(Ap.a24_1, k);
        tmpre24_1 += aij_r * x[Ap.ja24_1[k]];
		}
		#endif
		// RPRE32
    for (int k = Ap.ia32[i]; k < Ap.ia32[i+1]; k++) {
        double aij = RpArrayToFp_s(Ap.a32, k);
        tmpre32 += aij * x[Ap.ja32[k]];
		}
    for (int k = Ap.ia32_1[i]; k < Ap.ia32_1[i+1]; k++) {
        double aij = RpArrayToFp_s(Ap.a32_1, k);
        tmpre32_1 += aij * x[Ap.ja32_1[k]];
		}
		// RPRE40
    for (int k = Ap.ia40[i]; k < Ap.ia40[i+1]; k++) {
        double aij = RpArrayToFp_s(Ap.a40, k);
        tmpre40 += aij * x[Ap.ja40[k]];
		}
    for (int k = Ap.ia40_1[i]; k < Ap.ia40_1[i+1]; k++) {
        double aij = RpArrayToFp_s(Ap.a40_1, k);
        tmpre40_1 += aij * x[Ap.ja40_1[k]];
		}
		// RPRE48
    for (int k = Ap.ia48[i]; k < Ap.ia48[i+1]; k++) {
        double aij = RpArrayToFp_s(Ap.a48, k);
        tmpre48 += aij * x[Ap.ja48[k]];
		}
    for (int k = Ap.ia48_1[i]; k < Ap.ia48_1[i+1]; k++) {
        double aij = RpArrayToFp_s(Ap.a48_1, k);
        tmpre48_1 += aij * x[Ap.ja48_1[k]];
		}

	
		// FP32
		#ifdef USEFP32
		for (int k = Ap.iafp32[i]; k < Ap.iafp32[i+1]; k++) {
			float aij_r = Ap.afp32[k];
			tmpfp32 += aij_r * x[Ap.jafp32[k]];
		}
		#endif
		
		#ifdef USERE56
		// RPRE56
    for (int k = Ap.ia56[i]; k < Ap.ia56[i+1]; k++) {
        double aij = RpArrayToFp(Ap.a56, k);
        tmpre56 += aij * x[Ap.ja56[k]];
		}
		#endif
		// FP64
		for (int k = Ap.ia64[i]; k < Ap.ia64[i+1]; k++) {
			double aij = Ap.a64[k];
			tmpfp64 += aij * x[Ap.ja64[k]];
		}
		
		tmpre8 = rpre_rebias64(tmpre8, bias64[0]);
		tmpre8_1 = rpre_rebias64(tmpre8_1, bias64[0]);
		tmpre16 = rpre_rebias64(tmpre16, bias64[1]);
		tmpre16_1 = rpre_rebias64(tmpre16_1, bias64[1]);
		#ifdef USERE24
		tmpre24 = rpre_rebias64(tmpre24, bias64[2]);
		tmpre24_1 = rpre_rebias64(tmpre24_1, bias64[2]);
		#endif
		tmpre32 = rpre_rebias64(tmpre32, bias64[3]);
		tmpre32_1 = rpre_rebias64(tmpre32_1, bias64[3]);
		tmpre40 = rpre_rebias64(tmpre40, bias64[4]);
		tmpre40_1 = rpre_rebias64(tmpre40_1, bias64[4]);
		tmpre48 = rpre_rebias64(tmpre48, bias64[5]);
		tmpre48_1 = rpre_rebias64(tmpre48_1, bias64[5]);
		#ifdef USERE56
		tmpre56 = rpre_rebias64(tmpre56, bias64[6]);
		#endif
		y[i] = tmpre8 + tmpre16 + tmpre24 + tmpre32 + tmpre40 + tmpre48 + tmpre56 - tmpre8_1 - tmpre16_1 - tmpre24_1 - tmpre32_1 - tmpre40_1 - tmpre48_1 + tmpfp32 + tmpfp64;
	}
}
