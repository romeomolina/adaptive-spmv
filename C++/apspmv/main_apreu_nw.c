#include "common_rpreu.h"

int main(int argc, char* argv[]) {
	uint64_t repetition = REP;
	double timesRPRE_s[REP];
	double t0, gbs, time, error_NW;
	uint64_t i, n, m, nnz, j;
	char mtx_file[256];

	// ---------------------------------------------
	int epsi = atoi(argv[2]);
	strcpy (mtx_file, argv[1]);
	struct sparse_matrix_t* hst_A__ = load_sparse_matrix (MATRIX_MARKET, mtx_file);
	sparse_matrix_expand_symmetric_storage (hst_A__);
	int32_t errcode = sparse_matrix_convert (hst_A__, CSR);
	if (errcode != 0) {
	  fprintf (stderr, "err: conversion failed.\n");
		free (hst_A__);
		exit (1);
	}
	struct csr_matrix_t* hst_A_ = (struct csr_matrix_t*) hst_A__->repr;
	n = hst_A_->n;
	m = hst_A_->m;
	if (m != n) {
		printf ("error m != n\n");
		exit(1);
	}
	nnz = hst_A_->nnz;
	int* ja = hst_A_->colidx;
	int* ia = hst_A_->rowptr;
	double* A = (double*) hst_A_->values;
  double* x = (double*)malloc(n * sizeof(double));
  double* A_scaled = (double*)malloc(nnz * sizeof(double));

	#pragma omp parallel for
    for (i = 0; i < n; i++) 
        x[i] = 1.0;
	// ---------------------------------------------
	// norm computation
	// ---------------------------------------------
    double normA = 0.0;
    double normx = 0.0;
    double* absAabsx = (double*)malloc(n * sizeof(double));
    double* absAe = (double*)malloc(n * sizeof(double));
	#pragma omp parallel for
    for (i = 0; i < n; i++) 
        absAabsx[i] = absAe[i] = 0.0;
    for (i = 0; i < n; i++) {
        for (int k = ia[i]; k < ia[i + 1]; k++) {
            absAabsx[i] += fabs(A[k] * x[ja[k]]);
            absAe[i] += fabs(A[k]);
        }
    }
    for (i = 0; i < n; i++) {
        if (absAe[i] > normA) normA = absAe[i];
        if (fabs(x[i]) > normx) normx = fabs(x[i]);
    }
    double normAnormx = normA * normx;


	// ---------------------------------------------
	// FP128-SpMV (uniform-precision)
	// ---------------------------------------------
	__float128* Aq = (__float128*) malloc (nnz * sizeof(__float128));
    __float128* xq = (__float128*) malloc (n * sizeof(__float128));
    __float128* yq = (__float128*) malloc (n * sizeof(__float128));
	#pragma omp parallel for
  for (i = 0; i < nnz; i++) 
		Aq[i] = (__float128)A[i];
	#pragma omp parallel for
  for (i = 0; i < n; i++) 
    xq[i] = (__float128)x[i];
  ax_fp128(n, nnz, ia, ja, Aq, xq, yq);
  free(Aq);
  free(xq);

	// ---------------------------------------------
	// Adaptive-precision SpMV (NW) RPREU
	// ---------------------------------------------
	#pragma omp parallel for
	for(i=0; i<nnz;i++)
			A_scaled[i] = A[i]/normA;
	double* yAp_REU = (double*)malloc(n * sizeof(double));
	struct CSR_REU Ap_REU;
	alloc_CSR_REU (&Ap_REU, n, nnz);
  uint64_t bias64[8] = {0, 0, 0, 0, 0, 0, 0, 0};
  uint32_t bias32[8] = {0, 0, 0, 0, 0, 0, 0, 0};
	init_CSR_REU(&Ap_REU, bias64, bias32);
	convert_CSR_RPREU(&Ap_REU, ia, nnz, A_scaled, ja, epsi, bias64, bias32);
	for(int i = 0; i<8; i++){
		bias32[i] = bias32[i] << 23;
		bias64[i] = bias64[i] << 52;
	}
	for (i = 0; i < repetition; i++) {
		t0 = gettime();
		ax_rpre_s ( n, Ap_REU, x, yAp_REU, bias64, bias32);
		#pragma omp parallel for
		for(j=0; j<n;j++)
			yAp_REU[j] = yAp_REU[j]*normA;
		timesRPRE_s[i] = gettime() - t0;
	}
	time = getMinTime (timesRPRE_s, repetition);
	error_NW = spmv_error_NW (n, yAp_REU, yq, normAnormx);
	//CALCUL BYTES
	uint64_t bytes = 1*Ap_REU.nnz8 + 2*Ap_REU.nnz16 + 3*Ap_REU.nnz24 + 4*Ap_REU.nnz32 + 5*Ap_REU.nnz40 + 6*Ap_REU.nnz48; //vecteur d'éléments de A (positifs)
	bytes += 1*Ap_REU.nnz8_1 + 2*Ap_REU.nnz16_1 + 3*Ap_REU.nnz24_1 + 4*Ap_REU.nnz32_1 + 5*Ap_REU.nnz40_1 + 6*Ap_REU.nnz48_1; //vecteur d'éléments de A (négatifs)
	bytes += + 4*Ap_REU.nnzfp32 + 8*Ap_REU.nnz64; //vecteur d'éléments de A (natifs)
	bytes += + 4*Ap_REU.nnzfp32 + 7*Ap_REU.nnz56; //vecteur d'éléments de A (natifs)
	bytes += 4*Ap_REU.nnz; //vecteur d'indices j
	bytes += 4*n* ((Ap_REU.nnz8!=0) + (Ap_REU.nnz16!=0) + (Ap_REU.nnz24!=0) + (Ap_REU.nnz32!=0) + (Ap_REU.nnz40!=0) + (Ap_REU.nnz48!=0)); //vecteurs d'indices i (positifs)
	bytes += 4*n* ((Ap_REU.nnz8_1!=0) + (Ap_REU.nnz16_1!=0) + (Ap_REU.nnz24_1!=0) + (Ap_REU.nnz32_1!=0) + (Ap_REU.nnz40_1!=0) + (Ap_REU.nnz48_1!=0)); //vecteurs d'indices i (négatifs)
	bytes += 4*n* ((Ap_REU.nnzfp32!=0) + (Ap_REU.nnz64!=0)); //vecteurs d'indices i (natifs)
	bytes += 4*n* (Ap_REU.nnz56!=0); //vecteurs d'indices i (natifs)
  //END CALCUL BYTES
	gbs = 1.0e-9 * (double) bytes / time;
	printf("%zd\t%zd\t%zd\t%1.3e\t%1.3e\t%zd\t%1.3e\t", n, m, nnz, time, gbs, bytes, error_NW);
  printf("%d\t%d\t%d\t%d\t", Ap_REU.nnz0, Ap_REU.nnz8+Ap_REU.nnz8_1, Ap_REU.nnz16+Ap_REU.nnz16_1, Ap_REU.nnz24+Ap_REU.nnz24_1);
  printf("%d\t%d\t%d\t%d\t%d\t%d\t%d\n", Ap_REU.nnzfp32, Ap_REU.nnz32+Ap_REU.nnz32_1, Ap_REU.nnz40+Ap_REU.nnz40_1, Ap_REU.nnz48+Ap_REU.nnz48_1, Ap_REU.nnz56, Ap_REU.nnz64, Ap_REU.nnz);

	free_CSR_REU (&Ap_REU);
	free(yAp_REU);

	// Free
	free(yq);
 	free(x);
 	free(absAabsx);
	free(absAe);
	destroy_sparse_matrix(hst_A__);

  return 0;
}

