#include "common_rpfp.h"

int main(int argc, char* argv[]) {
	uint64_t repetition = REP;
	double times[REP];
	double t0, time, gbs, error_NW;
  uint64_t i, n, m, nnz, j;
	char mtx_file[256];

	// ---------------------------------------------
	int epsi = atoi(argv[2]);
	int nbprec = atoi(argv[3]);
	strcpy (mtx_file, argv[1]);
	struct sparse_matrix_t* hst_A__ = load_sparse_matrix (MATRIX_MARKET, mtx_file);
	sparse_matrix_expand_symmetric_storage (hst_A__);
	int32_t errcode = sparse_matrix_convert (hst_A__, CSR);
	if (errcode != 0) {
	    fprintf (stderr, "err: conversion failed.\n");
		free (hst_A__);
		exit (1);
	}
	struct csr_matrix_t* hst_A_ = (struct csr_matrix_t*) hst_A__->repr;
	n = hst_A_->n;
	m = hst_A_->m;
	if (m != n) {
		printf ("error m != n\n");
		exit(1);
	}
	nnz = hst_A_->nnz;
	int* ja = hst_A_->colidx;
	int* ia = hst_A_->rowptr;
	double* A = (double*) hst_A_->values;
  double* x = (double*)malloc(n * sizeof(double));
  double* A_scaled = (double*)malloc(nnz * sizeof(double));
	#pragma omp parallel for
    for (i = 0; i < n; i++) 
        x[i] = 1.0;

	// ---------------------------------------------
	// norm computation
	// ---------------------------------------------
    double normA = 0.0;
    double normx = 0.0;
    double* absAabsx = (double*)malloc(n * sizeof(double));
    double* absAe = (double*)malloc(n * sizeof(double));
	#pragma omp parallel for
    for (i = 0; i < n; i++) 
        absAabsx[i] = absAe[i] = 0.0;
    for (i = 0; i < n; i++) {
        for (int k = ia[i]; k < ia[i + 1]; k++) {
            absAabsx[i] += fabs(A[k] * x[ja[k]]);
            absAe[i] += fabs(A[k]);
        }
    }
    for (i = 0; i < n; i++) {
        if (absAe[i] > normA) normA = absAe[i];
        if (fabs(x[i]) > normx) normx = fabs(x[i]);
    }
    double normAnormx = normA * normx;

	// ---------------------------------------------
	// FP128-SpMV (uniform-precision)
	// ---------------------------------------------
	__float128* Aq = (__float128*) malloc (nnz * sizeof(__float128));
    __float128* xq = (__float128*) malloc (n * sizeof(__float128));
    __float128* yq = (__float128*) malloc (n * sizeof(__float128));
	#pragma omp parallel for
    for (i = 0; i < nnz; i++) 
		Aq[i] = (__float128)A[i];
	#pragma omp parallel for
    for (i = 0; i < n; i++) 
        xq[i] = (__float128)x[i];
    ax_fp128(n, nnz, ia, ja, Aq, xq, yq);
    free(Aq);
    free(xq);

	// ---------------------------------------------
	// Adaptive-precision SpMV (NW)
	// ---------------------------------------------
	#pragma omp parallel for
	for(i=0; i<nnz;i++)
		A_scaled[i] = A[i]/normA;
  double* yAp = (double*)malloc(n * sizeof(double));
  struct rpfpCSR Ap_NW;
	allocMultiCSR (&Ap_NW, n, nnz);
  init_A_Xprec(&Ap_NW, nbprec);
  convert_rpfpCSR(&Ap_NW, ia, nnz, A_scaled, ja, absAabsx, 1, epsi, false); // false=NW
  for (i = 0; i < repetition; i++) {
  	t0 = gettime();
    ax_adapt (n, Ap_NW, x, yAp);
		#pragma omp parallel for
		for(j=0; j<n;j++)
			yAp[j] = yAp[j]*normA;
		times[i] = gettime() - t0;
	}
	time = getMinTime (times, repetition);
  error_NW = spmv_error_NW (n, yAp, yq, normAnormx);
	uint64_t bytes = 1*Ap_NW.nnz8 + 2*Ap_NW.nnz16 + 2*Ap_NW.nnz16f + 3*Ap_NW.nnz24 + 4*Ap_NW.nnzfp32 + 5*Ap_NW.nnz40 + 6*Ap_NW.nnz48 +  7*Ap_NW.nnz56 + 8*Ap_NW.nnz64; //vecteur d'éléments de A
	bytes += 4*Ap_NW.nnz; //vecteur d'indices j
	bytes += 4*n*(Ap_NW.nnz8!=0) + 4*n*(Ap_NW.nnz16!=0) + 4*n*(Ap_NW.nnz16f!=0) + 4*n*(Ap_NW.nnz24!=0) + 4*n*(Ap_NW.nnzfp32!=0) + 4*n*(Ap_NW.nnz40!=0) + 4*n*(Ap_NW.nnz48!=0) + 4*n*(Ap_NW.nnz56!=0) + 4*n*(Ap_NW.nnz64!=0); //vecteurs d'indices i
	gbs = 1.0e-9 * (double) bytes / time;
	printf("%zd\t%zd\t%zd\t%1.3e\t%1.3e\t%zd\t%1.3e\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", n, m, nnz, time, gbs, bytes, error_NW, Ap_NW.nnz0, Ap_NW.nnz16, Ap_NW.nnz24, Ap_NW.nnzfp32, Ap_NW.nnz40, Ap_NW.nnz48, Ap_NW.nnz56, Ap_NW.nnz64, Ap_NW.nnz);
	freeMultiCSR (&Ap_NW);
    free(yAp);

	// ---------------------------
    free(yq);
    free(x);
    free(absAabsx);
    free(absAe);
	destroy_sparse_matrix(hst_A__);

    return 0;
}

