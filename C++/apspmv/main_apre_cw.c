#include "common_rpre.h"

int main(int argc, char* argv[]) {
	uint64_t repetition = REP;
	double timesRPRE[REP];
	double t0, time, gbs, error_CW;
  uint64_t i, j;
  uint64_t n, m, nnz;
	char mtx_file[256];

	// ---------------------------------------------
	int epsi = atoi(argv[2]);
	strcpy (mtx_file, argv[1]);
	struct sparse_matrix_t* hst_A__ = load_sparse_matrix (MATRIX_MARKET, mtx_file);
	sparse_matrix_expand_symmetric_storage (hst_A__);
	int32_t errcode = sparse_matrix_convert (hst_A__, CSR);
	if (errcode != 0) {
	  fprintf (stderr, "err: conversion failed.\n");
		free (hst_A__);
		exit (1);
	}
	struct csr_matrix_t* hst_A_ = (struct csr_matrix_t*) hst_A__->repr;
	n = hst_A_->n;
	m = hst_A_->m;
	if (m != n) {
		printf ("error m != n\n");
		exit(1);
	}
	nnz = hst_A_->nnz;
	int* ja = hst_A_->colidx;
	int* ia = hst_A_->rowptr;
	double* A = (double*) hst_A_->values;
  double* x = (double*)malloc(n * sizeof(double));
  double* A_scaled = (double*)malloc(nnz * sizeof(double));

	#pragma omp parallel for
    for (i = 0; i < n; i++) 
        x[i] = 1.0;

	// ---------------------------------------------
	// FP128-SpMV (uniform-precision)
	// ---------------------------------------------
	__float128* Aq = (__float128*) malloc (nnz * sizeof(__float128));
    __float128* xq = (__float128*) malloc (n * sizeof(__float128));
    __float128* yq = (__float128*) malloc (n * sizeof(__float128));
	#pragma omp parallel for
  for (i = 0; i < nnz; i++) 
		Aq[i] = (__float128)A[i];
	#pragma omp parallel for
  for (i = 0; i < n; i++) 
    xq[i] = (__float128)x[i];
  ax_fp128(n, nnz, ia, ja, Aq, xq, yq);
  free(Aq);
  free(xq);

	// ---------------------------------------------
	// original norm computation
	// ---------------------------------------------
  double normA = 0.0;
  double normx = 0.0;
  double* absAabsx = (double*)malloc(n * sizeof(double));
  double* absAe = (double*)malloc(n * sizeof(double));
	#pragma omp parallel for
  for (i = 0; i < n; i++) 
      absAabsx[i] = absAe[i] = 0.0;
  for (i = 0; i < n; i++) {
      for (int k = ia[i]; k < ia[i + 1]; k++) {
          absAabsx[i] += fabs(A[k] * x[ja[k]]);
          absAe[i] += fabs(A[k]);
      }
  }
  for (i = 0; i < n; i++) {
      if (absAe[i] > normA) normA = absAe[i];
      if (fabs(x[i]) > normx) normx = fabs(x[i]);
  }
  //double normAnormx = normA * normx;

	// ---------------------------------------------
	// Adaptive-precision SpMV (CW) RPRE
	// ---------------------------------------------
	#pragma omp parallel for
	for(j=0; j<n;j++){
		for (int k = ia[j]; k < ia[j + 1]; k++) {
			A_scaled[k] = A[k]/absAabsx[j];
		}
	}
	double* yAp_RE = (double*)malloc(n * sizeof(double));
	struct CSR_RE Ap_RE;
	alloc_CSR_RE (&Ap_RE, n, nnz);
	uint64_t bias64[8] = {0, 0, 0, 0, 0, 0, 0, 0};
	uint32_t bias32[8] = {0, 0, 0, 0, 0, 0, 0, 0};
	init_CSR_RE(&Ap_RE, bias64, bias32);
	convert_CSR_RE(&Ap_RE, ia, nnz, A_scaled, ja, epsi, bias64, bias32); 
	for(i = 0; i<8; i++){
		bias32[i] = bias32[i] << 23;
		bias64[i] = bias64[i] << 52;
	}
	for (i = 0; i < repetition; i++) {
		t0 = gettime();
		ax_rpre ( n, Ap_RE, x, yAp_RE, bias64);
		#pragma omp parallel for	
		for(j=0; j<n;j++)
			yAp_RE[j] = yAp_RE[j]*absAabsx[j];
		timesRPRE[i] = gettime() - t0;
	}

	time = getMinTime (timesRPRE, repetition);
	error_CW = spmv_error_CW (n, yAp_RE, yq, absAabsx);
	//CALCUL BYTES
	uint64_t bytes = 1*Ap_RE.nnz8 + 2*Ap_RE.nnz16 + 3*Ap_RE.nnz24 + 4*Ap_RE.nnzfp32 + 4*Ap_RE.nnz32 + 5*Ap_RE.nnz40 + 6*Ap_RE.nnz48 + 7*Ap_RE.nnz56 + 8*Ap_RE.nnz64; //vecteur d'éléments de A
	bytes += 4*Ap_RE.nnz; //vecteur d'indices j
	bytes += 4*n* ((Ap_RE.nnz8!=0) + (Ap_RE.nnz16!=0) + (Ap_RE.nnz24!=0) + (Ap_RE.nnzfp32!=0) + (Ap_RE.nnz32!=0) + (Ap_RE.nnz40!=0) + (Ap_RE.nnz48!=0) + (Ap_RE.nnz56!=0) + (Ap_RE.nnz64!=0)); //vecteurs d'indices i
  //END CALCUL BYTES
	gbs = 1.0e-9 * (double) bytes / time;
	printf("%zd\t%zd\t%zd\t%1.3e\t%1.3e\t%zd\t%1.3e\t\t", n, m, nnz, time, gbs, bytes, error_CW);
	printf("%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", Ap_RE.nnz0, Ap_RE.nnz8, Ap_RE.nnz16, Ap_RE.nnz24, Ap_RE.nnzfp32, Ap_RE.nnz32, Ap_RE.nnz40, Ap_RE.nnz48, Ap_RE.nnz56, Ap_RE.nnz64, Ap_RE.nnz);
						printf("haha\n");
	free_CSR_RE (&Ap_RE);
	free(yAp_RE);

  free(yq);
  free(x);
  free(absAabsx);
  free(absAe);
	destroy_sparse_matrix(hst_A__);

  return 0;
}

