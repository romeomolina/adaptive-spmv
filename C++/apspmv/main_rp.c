#include "common_rpfp.h"
#include "ax_unif.h"
#include <fenv.h>



int main(int argc, char* argv[]) {
	_MM_SET_FLUSH_ZERO_MODE (_MM_FLUSH_ZERO_ON);
	uint64_t repetition = REP;
	double times[REP];
	double t0, time, gbs, error_NW, error_CW;
  uint64_t i, n, m, nnz, j;
	char mtx_file[256];

	// ---------------------------------------------
	int epsi = atoi(argv[2]);
//	int nbprec = atoi(argv[3]);
	strcpy (mtx_file, argv[1]);
	struct sparse_matrix_t* hst_A__ = load_sparse_matrix (MATRIX_MARKET, mtx_file);
	sparse_matrix_expand_symmetric_storage (hst_A__);
	int32_t errcode = sparse_matrix_convert (hst_A__, CSR);
	if (errcode != 0) {
	    fprintf (stderr, "err: conversion failed.\n");
		free (hst_A__);
		exit (1);
	}
	struct csr_matrix_t* hst_A_ = (struct csr_matrix_t*) hst_A__->repr;
	n = hst_A_->n;
	m = hst_A_->m;
	if (m != n) {
		printf ("error m != n\n");
		exit(1);
	}
	nnz = hst_A_->nnz;
	int* ja = hst_A_->colidx;
	int* ia = hst_A_->rowptr;
	double* A = (double*) hst_A_->values;
  double* x = (double*)malloc(n * sizeof(double));
  double* A_scaled = (double*)malloc(nnz * sizeof(double));
	#pragma omp parallel for
  for (i = 0; i < n; i++)
		x[i] = 1.0;

	// ---------------------------------------------
	// norm computation
	// ---------------------------------------------
  double normA = 0.0;
  double normx = 0.0;
  double* absAabsx = (double*)malloc(n * sizeof(double));
  double* absAe = (double*)malloc(n * sizeof(double));
	#pragma omp parallel for
  for (i = 0; i < n; i++) 
  	absAabsx[i] = absAe[i] = 0.0;
  for (i = 0; i < n; i++) {
		for (int k = ia[i]; k < ia[i + 1]; k++) {
	    absAabsx[i] += fabs(A[k] * x[ja[k]]);
	    absAe[i] += fabs(A[k]);
		}
  }
  for (i = 0; i < n; i++) {
    if (absAe[i] > normA) normA = absAe[i];
    if (fabs(x[i]) > normx) normx = fabs(x[i]);
  }
  double normAnormx = normA * normx;

	// ---------------------------------------------
	// FP128-SpMV (uniform-precision)
	// ---------------------------------------------
	__float128* Aq = (__float128*) malloc (nnz * sizeof(__float128));
  __float128* xq = (__float128*) malloc (n * sizeof(__float128));
  __float128* yq = (__float128*) malloc (n * sizeof(__float128));
	#pragma omp parallel for
  for (i = 0; i < nnz; i++) 
		Aq[i] = (__float128)A[i];
	#pragma omp parallel for
  for (i = 0; i < n; i++) 
  	xq[i] = (__float128)x[i];
  ax_fp128(n, nnz, ia, ja, Aq, xq, yq);
  free(Aq);
  free(xq);

	// ---------------------------------------------
	// Reduced-precision SpMV (uniform-precision)
	// ---------------------------------------------
	#pragma omp parallel for
	for(i=0; i<nnz;i++)
			A_scaled[i] = A[i]/normA;
  double* yRp = (double*)malloc(n * sizeof(double));
	int ind = 0;
	if (epsi <= 24) {
		ind = std::ceil((epsi+8.)/8.)*8;
	} else if (epsi <= 53) {
		ind = std::ceil((epsi+11.)/8.)*8;
	}
	else {
		printf ("error\n");
		exit(1);
	}

	if (ind == 64) {
		printf ("use fp64\n");
		exit(1);
	} 
	
	else if (ind == 56) {
		// FP56-SpMV
		rpfp64in56barray A56;
		rpfpMalloc (A56, nnz);
		#pragma omp parallel for
	  for (i = 0; i < nnz; i++) 
			FpToRpArray (A_scaled[i], A56, i);
		for (i = 0; i < repetition; i++) {
			t0 = gettime();
			ax_fp56(n, nnz, ia, ja, A56, x, yRp);
			#pragma omp parallel for
			for(j=0; j<n;j++)
				yRp[j] = yRp[j]*normA;
			times[i] = gettime() - t0;
		}
		time = getMinTime (times, repetition);
		rpfpFree(A56);
	} 
	
	else if (ind == 48) {
		// FP48-SpMV
		rpfp64in48barray A48;
		rpfpMalloc (A48, nnz);
		#pragma omp parallel for
	  for (i = 0; i < nnz; i++) 
			FpToRpArray (A_scaled[i], A48, i);
		for (i = 0; i < repetition; i++) {
			t0 = gettime();
			ax_fp48(n, nnz, ia, ja, A48, x, yRp);
			#pragma omp parallel for
			for(j=0; j<n;j++)
				yRp[j] = yRp[j]*normA;
			times[i] = gettime() - t0;
		}
		time = getMinTime (times, repetition);
		rpfpFree(A48);
	} 
	
	else if (ind == 40) {
		// FP40-SpMV
		rpfp64in40barray A40;
		rpfpMalloc (A40, nnz);
		#pragma omp parallel for
	  for (i = 0; i < nnz; i++) 
			FpToRpArray (A_scaled[i], A40, i);
		for (i = 0; i < repetition; i++) {
			t0 = gettime();
			ax_fp40(n, nnz, ia, ja, A40, x, yRp);
			#pragma omp parallel for
			for(j=0; j<n;j++)
				yRp[j] = yRp[j]*normA;
			times[i] = gettime() - t0;
		}
		time = getMinTime (times, repetition);
		rpfpFree(A40);
	} 
	
	else if (ind == 32) {
		printf ("use fp32\n");
		exit(1);
		
	} 
	
	else if (ind == 24) {
		// FP24-SpMV
		rpfp32in24barray A24;
		rpfpMalloc (A24, nnz);
		#pragma omp parallel for
	  for (i = 0; i < nnz; i++) 
			FpToRpArray (A_scaled[i], A24, i);
		for (i = 0; i < repetition; i++) {
			t0 = gettime();
			ax_fp24(n, nnz, ia, ja, A24, x, yRp);
			#pragma omp parallel for
			for(j=0; j<n;j++)
				yRp[j] = yRp[j]*normA;
			times[i] = gettime() - t0;
		}
		time = getMinTime (times, repetition);
		rpfpFree(A24);
	} 
	
	else if (ind == 16) {
		// FP16-SpMV
		rpfp32in16barray A16;
		rpfpMalloc (A16, nnz);
		#pragma omp parallel for
	  for (i = 0; i < nnz; i++) 
			FpToRpArray (A_scaled[i], A16, i);
		for (i = 0; i < repetition; i++) {
			t0 = gettime();
			ax_fp16(n, nnz, ia, ja, A16, x, yRp);
			#pragma omp parallel for
			for(j=0; j<n;j++)
				yRp[j] = yRp[j]*normA;
			times[i] = gettime() - t0;
		}
		time = getMinTime (times, repetition);
		rpfpFree(A16);
	}
		
  time = getMinTime (times, repetition);
  error_NW = spmv_error_NW (n, yRp, yq, normAnormx);
  error_CW = spmv_error_CW (n, yRp, yq, absAabsx);
	uint64_t bytes = (ind/8)*nnz; //vecteur d'éléments de A
	bytes += 4*nnz; //vecteur d'indices j
	bytes += 4*n; //vecteurs d'indices i
	gbs = 1.0e-9 * (double) bytes / time;
	printf("%zd\t%zd\t%zd\t%1.3e\t%1.3e\t%zd\t%1.3e\t%1.3e\n", n, m, nnz, time, gbs, bytes, error_NW, error_CW);

	// ---------------------------
 	free(yRp);
  free(yq);
  free(x);
  free(absAabsx);
  free(absAe);
	destroy_sparse_matrix(hst_A__);

  return 0;
}

