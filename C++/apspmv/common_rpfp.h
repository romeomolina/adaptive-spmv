#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <sys/time.h>
extern "C" {
#include <bebop/smc/sparse_matrix.h>
#include <bebop/smc/sparse_matrix_ops.h>
#include <bebop/smc/csr_matrix.h>
}
#include <cassert>
#include <algorithm>
#include <functional>
#include <quadmath.h>
#include <omp.h>
#include <rpfp.h>
#include "half.hpp" 

#define REP 5 // number of repeat

// accessor for RP8
__inline__ half_float::half
RpArrayToFp (uint8_t* sa, size_t i){
	uint16_t u16;
	u16 = (uint16_t)sa[i]; // |0(8)s(1)e(5)f(2)| 
	u16 = u16 << 8; // |s(1)e(5)f(2)0(8)| 
	half_float::half h = *reinterpret_cast<const half_float::half*>(&u16);
	return h;
}
__inline__ void
FpToRpArray (half_float::half f16, uint8_t* sa, size_t i){
	uint16_t u16 = *reinterpret_cast<const uint16_t*>(&f16); // |s(1)e(5)f(10)|
	sa[i] = (uint8_t)(u16 >> 8); // |0(8)s(1)e(5)f(2)| 
}

double gettime () {
	struct timeval tv;
	gettimeofday (&tv, NULL);
	return tv.tv_sec + (double) tv.tv_usec * 1.0e-6;
}

double getMinTime (
	const double* times,
	const int nloop
) {
	double min = times[0];
	for (int32_t i = 1; i < nloop; i++) {
		double tmp = times[i];
		if (min > tmp) min = tmp;
	}
	return min;
}

struct rpfpCSR {
    int n;
    int nnz; // total nnz
    int nbprec;

	// drop
	int nnz0;

	// FP8
	int* ia8;
	int* ja8;
	uint8_t* a8;
	int nnz8;

	// FP16
	int* ia16f;
	int* ja16f;
	half_float::half* a16f;
	int nnz16f;

	// BF16
	int* ia16;
	int* ja16;
	rpfp32in16barray a16;
	int nnz16;

	// FP24
	int* ia24;
	int* ja24;
	rpfp32in24barray a24;
	int nnz24;

	// FP32
	int* iafp32;
	int* jafp32;
	float* afp32;
	int nnzfp32;

	// FP40
	int* ia40;
	int* ja40;
	rpfp64in40barray a40;
	int nnz40;

	// FP48
	int* ia48;
	int* ja48;
	rpfp64in48barray a48;
	int nnz48;

	// FP56
	int* ia56;
	int* ja56;
	rpfp64in56barray a56;
	int nnz56;

	// FP64
	int* ia64;
	int* ja64;
	double* a64;
	int nnz64;
};
 
void init_A_Xprec(struct rpfpCSR* Ap, int nbprec) {
	//2 prec : FP32, FP64
	//3 prec : RP16, FP32, FP64
	//4 prec : RP16, FP32, RP48, FP64
	//7 prec : RP8, RP16, RP24, FP32, RP40, RP48, RP56, RP64
	Ap->nbprec = nbprec;
	
	// drop
	Ap->nnz0 = 0;

	// FP8
	half_float::half h(1.);
	FpToRpArray (h, Ap->a8, 0);
	Ap->ia8[0] = 0;
	Ap->ja8[0] = 0;
	Ap->nnz8 = 0;

	// FP16
	Ap->a16f[0] = 1.;
	Ap->ia16f[0] = 0;
	Ap->ja16f[0] = 0;
	Ap->nnz16f = 0;

	// BF16
	FpToRpArray (1., Ap->a16, 0);
	Ap->ia16[0] = 0;
	Ap->ja16[0] = 0;
	Ap->nnz16 = 0;

	// FP24
	FpToRpArray (1., Ap->a24, 0);
	Ap->ia24[0] = 0;
	Ap->ja24[0] = 0;
	Ap->nnz24 = 0;

	// FP32
	Ap->afp32[0] = 1.;
	Ap->iafp32[0] = 0;
	Ap->jafp32[0] = 0;
	Ap->nnzfp32 = 0;

	// FP40
	FpToRpArray (1., Ap->a40, 0);
	Ap->ia40[0] = 0;
	Ap->ja40[0] = 0;
	Ap->nnz40 = 0;

	// FP48
	FpToRpArray (1., Ap->a48, 0);
	Ap->ia48[0] = 0;
	Ap->ja48[0] = 0;
	Ap->nnz48 = 0;

	// FP56
	FpToRpArray (1., Ap->a56, 0);
	Ap->ia56[0] = 0;
	Ap->ja56[0] = 0;
	Ap->nnz56 = 0;

	// FP64
	Ap->a64[0] = 1.;
	Ap->ia64[0] = 0;
	Ap->ja64[0] = 0;
	Ap->nnz64 = 0;

	// total nnz 
    Ap->nnz = 0;
}

// FP128 SpMV
void ax_fp128(int n, int nnz, int* ia, int* ja, __float128* A, __float128* x, __float128* w) {
	#pragma omp parallel for
    for (int i = 0; i < n; i++) {
		__float128 tmp = 0.;
        for (int k = ia[i]; k < ia[i + 1]; k++) {
            tmp += A[k] * x[ja[k]];
        }
		w[i] = tmp;
    }
}

// Adaptive-precision SpMV
void ax_adapt(int n, rpfpCSR Ap, const double* x, double* y) {
	int nbprec = Ap.nbprec;
    #pragma omp parallel for
    for (int i = 0; i < n; i++) {
		double tmp = 0.;
		if (nbprec >= 8) {
			// FP8
	    for (int k = Ap.ia8[i]; k < Ap.ia8[i+1]; k++) {
        float aij_r = RpArrayToFp(Ap.a8, k);
        tmp += aij_r * x[Ap.ja8[k]];
			}
		}
		if (nbprec >= 3) {
			// BF16
	    for (int k = Ap.ia16[i]; k < Ap.ia16[i+1]; k++) {
        float aij_r = RpArrayToFp(Ap.a16, k);
        tmp += aij_r * x[Ap.ja16[k]];
	    }
		}
		if (nbprec >= 9) {
			// FP16
    	for (int k = Ap.ia16f[i]; k < Ap.ia16f[i+1]; k++) {
  	    float aij_r = Ap.a16f[k];
  	    tmp += aij_r * x[Ap.ja16f[k]];
			}
		}
		if (nbprec >= 7) {
			// FP24
      for (int k = Ap.ia24[i]; k < Ap.ia24[i+1]; k++) {
        float aij_r = RpArrayToFp(Ap.a24, k);
        tmp += aij_r * x[Ap.ja24[k]];
			}
		}
		// FP32
	    for (int k = Ap.iafp32[i]; k < Ap.iafp32[i+1]; k++) {
	        tmp += Ap.afp32[k] * x[Ap.jafp32[k]];
		}
		if (nbprec >= 7) {
			// FP40
      for (int k = Ap.ia40[i]; k < Ap.ia40[i+1]; k++) {
        double aij = RpArrayToFp(Ap.a40, k);
        tmp += aij * x[Ap.ja40[k]];
			}
		}
		if (nbprec >= 4) {
			// FP48
      for (int k = Ap.ia48[i]; k < Ap.ia48[i+1]; k++) {
        double aij = RpArrayToFp(Ap.a48, k);
        tmp += aij * x[Ap.ja48[k]];
			}
		}
		if (nbprec >= 7) {
			// FP56
      for (int k = Ap.ia56[i]; k < Ap.ia56[i+1]; k++) {
        double aij = RpArrayToFp(Ap.a56, k);
        tmp += aij * x[Ap.ja56[k]];
			}
		}
		// FP64
		for (int k = Ap.ia64[i]; k < Ap.ia64[i+1]; k++) {
			double aij = Ap.a64[k];
			tmp += aij * x[Ap.ja64[k]];
		}
		y[i] = tmp;
	}
}

double spmv_error_NW (int n, double* w_nw, __float128* refw, double normAnormx) {
    double error_NW = 0.0;
    for (int i = 0; i < n; i++) {
        error_NW = std::max(error_NW, (double)fabsq(w_nw[i] - refw[i]));
	}
    error_NW = error_NW / normAnormx;
	return error_NW;
}

double spmv_error_CW (int n, double* w_cw, __float128* refw, double* absAabsx) {
    double error_CW = 0.0;
    for (int i = 0; i < n; i++) {
        error_CW = std::max(error_CW, (double)fabsq(w_cw[i] - refw[i]) / absAabsx[i]);
	}
	return error_CW;
}

#define FP8E5M2_MIN 6.10e-5
#define FP8E5M2_MAX 57344.
#define HALF_MIN 6.10352e-5
#define HALF_MAX 65504.
void convert_element_rpfpCSR(int i, int jcnk, double Ak, struct rpfpCSR* Ap, int k, double* bornesRPFP) {
  int kX;
  int nbprec = Ap->nbprec;
	if (fabs(Ak) < bornesRPFP[0]){
		//DROP
		Ap->nnz0++;
		return;
	}
	if (nbprec >= 8 and fabs(Ak) < bornesRPFP[1] && fabs(Ak) <= FP8E5M2_MAX && fabs(Ak) >= FP8E5M2_MIN){
		//FP8
		kX = Ap->ia8[i + 1];
		Ap->ia8[i + 1] = Ap->ia8[i + 1] + 1;
		Ap->ja8[kX] = jcnk;
		half_float::half h(Ak);
    FpToRpArray (h, Ap->a8, kX);
		Ap->nnz8++;
		Ap->nnz = Ap->nnz + 1;
		return;
	}
	if (nbprec >= 3 and fabs(Ak) < bornesRPFP[2]){
		//BF16
		kX = Ap->ia16[i + 1];
		Ap->ia16[i + 1] = Ap->ia16[i + 1] + 1;
		Ap->ja16[kX] = jcnk;
		FpToRpArray (Ak, Ap->a16, kX);
		Ap->nnz16++;
		Ap->nnz = Ap->nnz + 1;
		return;
	}
	if (nbprec >= 9 and fabs(Ak) < bornesRPFP[3] && fabs(Ak) <= HALF_MAX && fabs(Ak) >= HALF_MIN){
		//FP16
		kX = Ap->ia16f[i + 1];
		Ap->ia16f[i + 1] = Ap->ia16f[i + 1] + 1;
		Ap->ja16f[kX] = jcnk;
    Ap->a16f[kX] = Ak;
		Ap->nnz16f++;
		Ap->nnz = Ap->nnz + 1;
		return;
	}
	if (nbprec >= 7 and fabs(Ak) < bornesRPFP[4]){
		//FP24
		kX = Ap->ia24[i + 1];
		Ap->ia24[i + 1] = Ap->ia24[i + 1] + 1;
		Ap->ja24[kX] = jcnk;
		FpToRpArray (Ak, Ap->a24, kX);
		Ap->nnz24++;
		Ap->nnz = Ap->nnz + 1;
		return;
	}
	if (fabs(Ak) < bornesRPFP[5]){
		//FP32
		kX = Ap->iafp32[i + 1];
		Ap->iafp32[i + 1] = Ap->iafp32[i + 1] + 1;
		Ap->jafp32[kX] = jcnk;
		Ap->afp32[kX] = (float) Ak;
		Ap->nnzfp32++;
		Ap->nnz = Ap->nnz + 1;
		return;
	}
	if (nbprec >= 7 and fabs(Ak) < bornesRPFP[6]){
		//FP40
		kX = Ap->ia40[i + 1];
		Ap->ia40[i + 1] = Ap->ia40[i + 1] + 1;
		Ap->ja40[kX] = jcnk;
		FpToRpArray (Ak, Ap->a40, kX);
		Ap->nnz40++;
		Ap->nnz = Ap->nnz + 1;
		return;
	}
	if (nbprec >= 4 and fabs(Ak) < bornesRPFP[7]){
		//FP48
		kX = Ap->ia48[i + 1];
		Ap->ia48[i + 1] = Ap->ia48[i + 1] + 1;
		Ap->ja48[kX] = jcnk;
		FpToRpArray (Ak, Ap->a48, kX);
		Ap->nnz48++;
		Ap->nnz = Ap->nnz + 1;
		return;
	}
	if (nbprec >= 7 and fabs(Ak) < bornesRPFP[8]){
		//FP56
		kX = Ap->ia56[i + 1];
		Ap->ia56[i + 1] = Ap->ia56[i + 1] + 1;
		Ap->ja56[kX] = jcnk;
		FpToRpArray (Ak, Ap->a56, kX);
		Ap->nnz56++;
		Ap->nnz = Ap->nnz + 1;
		return;
	}
	//FP64
	kX = Ap->ia64[i + 1];
	Ap->ia64[i + 1] += 1;
	Ap->ja64[kX] = jcnk;
	Ap->a64[kX] = Ak;
	Ap->nnz64++;
	Ap->nnz += 1;
	return;
}

void convert_rpfpCSR(struct rpfpCSR* Ap, int* irn, int nnz, double* A, int* jcn, double* absAabsx, double normA, int t, bool crit) {
	double bornesRPFP[9];
	if(crit){ //CW
		for (int i = 0; i < Ap->n; i++) {
			bornesRPFP[0] = pow(2,-t)*absAabsx[i];
			bornesRPFP[1] = pow(2,3-t)*absAabsx[i];
			bornesRPFP[2] = pow(2,8-t)*absAabsx[i];
			bornesRPFP[3] = pow(2,11-t)*absAabsx[i];
			bornesRPFP[4] = pow(2,16-t)*absAabsx[i];
			bornesRPFP[5] = pow(2,24-t)*absAabsx[i];
			bornesRPFP[6] = pow(2,29-t)*absAabsx[i];
			bornesRPFP[7] = pow(2,37-t)*absAabsx[i];
			bornesRPFP[8] = pow(2,45-t)*absAabsx[i];
		
			Ap->ia8[i + 1] = Ap->ia8[i];
			Ap->ia16[i + 1] = Ap->ia16[i];
			Ap->ia16f[i + 1] = Ap->ia16f[i];
			Ap->ia24[i + 1] = Ap->ia24[i];
			Ap->iafp32[i + 1] = Ap->iafp32[i];
			Ap->ia40[i + 1] = Ap->ia40[i];
			Ap->ia48[i + 1] = Ap->ia48[i];
			Ap->ia56[i + 1] = Ap->ia56[i];
			Ap->ia64[i + 1] = Ap->ia64[i];
		  for (int k = irn[i]; k < irn[i + 1]; k++) {
		    convert_element_rpfpCSR(i, jcn[k], A[k], Ap, k, bornesRPFP);
			}
		}
	}
	else{ //NW
			bornesRPFP[0] = pow(2,-t)*normA;
			bornesRPFP[1] = pow(2,3-t)*normA;
			bornesRPFP[2] = pow(2,8-t)*normA;
			bornesRPFP[3] = pow(2,11-t)*normA;
			bornesRPFP[4] = pow(2,16-t)*normA;
			bornesRPFP[5] = pow(2,24-t)*normA;
			bornesRPFP[6] = pow(2,29-t)*normA;
			bornesRPFP[7] = pow(2,37-t)*normA;
			bornesRPFP[8] = pow(2,45-t)*normA;
		for (int i = 0; i < Ap->n; i++) {
			Ap->ia8[i + 1] = Ap->ia8[i];
			Ap->ia16[i + 1] = Ap->ia16[i];
			Ap->ia16f[i + 1] = Ap->ia16f[i];
			Ap->ia24[i + 1] = Ap->ia24[i];
			Ap->iafp32[i + 1] = Ap->iafp32[i];
			Ap->ia40[i + 1] = Ap->ia40[i];
			Ap->ia48[i + 1] = Ap->ia48[i];
			Ap->ia56[i + 1] = Ap->ia56[i];
			Ap->ia64[i + 1] = Ap->ia64[i];
		  for (int k = irn[i]; k < irn[i + 1]; k++) {
		    convert_element_rpfpCSR(i, jcn[k], A[k], Ap, k, bornesRPFP);
			}
		}
	}
}


void allocMultiCSR (struct rpfpCSR* Ap, const int n, const int nnz) {
  Ap->n = n;
  Ap->nnz = nnz;

	// FP8
	Ap->a8 = (uint8_t*) malloc ((nnz+1) * sizeof(uint8_t));
	Ap->ia8 = (int*) malloc ((n+1) * sizeof(int));
	Ap->ja8 = (int*) malloc ((nnz) * sizeof(int));

	// FP16
	Ap->a16f = (half_float::half*) malloc ((nnz+1) * sizeof(half_float::half));
	Ap->ia16f = (int*) malloc ((n+1) * sizeof(int));
	Ap->ja16f = (int*) malloc ((nnz) * sizeof(int));

	// BF16
	rpfpMalloc (Ap->a16, nnz+1);
	Ap->ia16 = (int*) malloc ((n+1) * sizeof(int));
	Ap->ja16 = (int*) malloc ((nnz) * sizeof(int));

	// FP24
	rpfpMalloc (Ap->a24, nnz+1);
	Ap->ia24 = (int*) malloc ((n+1) * sizeof(int));
	Ap->ja24 = (int*) malloc ((nnz) * sizeof(int));

	// FP32
	Ap->afp32 = (float*) malloc ((nnz+1) * sizeof(float));
	Ap->iafp32 = (int*) malloc ((n+1) * sizeof(int));
	Ap->jafp32 = (int*) malloc ((nnz) * sizeof(int));

	// FP40
	rpfpMalloc (Ap->a40, nnz+1);
	Ap->ia40 = (int*) malloc ((n+1) * sizeof(int));
	Ap->ja40 = (int*) malloc ((nnz) * sizeof(int));

	// FP48
	rpfpMalloc (Ap->a48, nnz+1);
	Ap->ia48 = (int*) malloc ((n+1) * sizeof(int));
	Ap->ja48 = (int*) malloc ((nnz) * sizeof(int));

	// FP56
	rpfpMalloc (Ap->a56, nnz+1);
	Ap->ia56 = (int*) malloc ((n+1) * sizeof(int));
	Ap->ja56 = (int*) malloc ((nnz) * sizeof(int));

	// FP64
	Ap->a64 = (double*) malloc ((nnz+1) * sizeof(double));
	Ap->ia64 = (int*) malloc ((n+1) * sizeof(int));
	Ap->ja64 = (int*) malloc ((nnz) * sizeof(int));
}

void freeMultiCSR (struct rpfpCSR* Ap) {
	// FP8
	free(Ap->a8);
	free(Ap->ia8);
	free(Ap->ja8);

	// FP16
	free(Ap->a16f);
	free(Ap->ia16f);
	free(Ap->ja16f);

	// BF16
	rpfpFree(Ap->a16);
	free(Ap->ia16);
	free(Ap->ja16);

	// FP24
	rpfpFree(Ap->a24);
	free(Ap->ia24);
	free(Ap->ja24);

	// FP32
	free(Ap->afp32);
	free(Ap->iafp32);
	free(Ap->jafp32);

	// FP40
	rpfpFree(Ap->a40);
	free(Ap->ia40);
	free(Ap->ja40);

	// FP48
	rpfpFree(Ap->a48);
	free(Ap->ia48);
	free(Ap->ja48);

	// FP56
	rpfpFree(Ap->a56);
	free(Ap->ia56);
	free(Ap->ja56);

	// FP64
	free(Ap->a64);
	free(Ap->ia64);
	free(Ap->ja64);
}
	
