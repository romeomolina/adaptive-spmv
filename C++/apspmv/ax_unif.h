#pragma once
#include "common_rpfp.h"

// FP64 SpMV
void ax_fp64(int n, int nnz, int* ia, int* ja, double* A, double* x, double* w) {
	#pragma omp parallel for 
    for (int i = 0; i < n; i++) {
		double tmp = 0.;
        for (int k = ia[i]; k < ia[i + 1]; k++) 
            tmp += A[k] * x[ja[k]];
		w[i] = tmp;
    }
}

// FP56 SpMV
void ax_fp56(int n, int nnz, int* ia, int* ja, rpfp64in56barray A, double* x, double* w) {
	#pragma omp parallel for 
    for (int i = 0; i < n; i++) {
		double tmp = 0.;
        for (int k = ia[i]; k < ia[i + 1]; k++) 
            tmp += RpArrayToFp(A, k) * x[ja[k]];
		w[i]=tmp;
    }
}

// FP48 SpMV
void ax_fp48(int n, int nnz, int* ia, int* ja, rpfp64in48barray A, double* x, double* w) {
	#pragma omp parallel for 
    for (int i = 0; i < n; i++) {
		double tmp = 0.;
        for (int k = ia[i]; k < ia[i + 1]; k++) 
            tmp += RpArrayToFp(A, k) * x[ja[k]];
		w[i]=tmp;
    }
}

// FP40 SpMV
void ax_fp40(int n, int nnz, int* ia, int* ja, rpfp64in40barray A, double* x, double* w) {
	#pragma omp parallel for 
    for (int i = 0; i < n; i++) {
		double tmp = 0.;
        for (int k = ia[i]; k < ia[i + 1]; k++) 
            tmp += RpArrayToFp(A, k) * x[ja[k]];
		w[i]=tmp;
    }
}

// FP32 SpMV
void ax_fp32(int n, int nnz, int* ia, int* ja, float* A, double* x, double* w) {
	#pragma omp parallel for 
	for (int i = 0; i < n; i++) {
		double tmp = 0.;
		for (int k = ia[i]; k < ia[i + 1]; k++) 
			tmp += A[k] * x[ja[k]];
		w[i] = tmp;
	}
}

// FP24 SpMV
void ax_fp24(int n, int nnz, int* ia, int* ja, rpfp32in24barray A, double* x, double* w) {
	#pragma omp parallel for 
    for (int i = 0; i < n; i++) {
		double tmp = 0.;
        for (int k = ia[i]; k < ia[i + 1]; k++) 
            tmp += RpArrayToFp(A, k) * x[ja[k]];
		w[i]=tmp;
    }
}

// FP16 SpMV
void ax_fp16(int n, int nnz, int* ia, int* ja, rpfp32in16barray A, double* x, double* w) {
	#pragma omp parallel for 
    for (int i = 0; i < n; i++) {
		double tmp = 0.;
        for (int k = ia[i]; k < ia[i + 1]; k++) 
            tmp += RpArrayToFp(A, k) * x[ja[k]];
		w[i]=tmp;
    }
}
