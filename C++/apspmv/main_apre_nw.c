#include "common_rpre.h"

int main(int argc, char* argv[]) {
	uint64_t repetition = REP;
	double timesRPRE[REP];
	double t0, time, gbs, error_NW;
  	uint64_t i, n, m, nnz, j;
	char mtx_file[256];

	// ---------------------------------------------
	int epsi = atoi(argv[2]);
	strcpy (mtx_file, argv[1]);
	struct sparse_matrix_t* hst_A__ = load_sparse_matrix (MATRIX_MARKET, mtx_file);
	sparse_matrix_expand_symmetric_storage (hst_A__);
	int32_t errcode = sparse_matrix_convert (hst_A__, CSR);
	if (errcode != 0) {
	  fprintf (stderr, "err: conversion failed.\n");
		free (hst_A__);
		exit (1);
	}
	struct csr_matrix_t* hst_A_ = (struct csr_matrix_t*) hst_A__->repr;
	n = hst_A_->n;
	m = hst_A_->m;
	if (m != n) {
		printf ("error m != n\n");
		exit(1);
	}
	nnz = hst_A_->nnz;
	int* ja = hst_A_->colidx;
	int* ia = hst_A_->rowptr;
	double* A = (double*) hst_A_->values;
  double* x = (double*)malloc(n * sizeof(double));
  double* A_scaled = (double*)malloc(nnz * sizeof(double));

	#pragma omp parallel for
    for (i = 0; i < n; i++) 
        x[i] = 1.0;

	// ---------------------------------------------
	// FP128-SpMV (uniform-precision)
	// ---------------------------------------------
	__float128* Aq = (__float128*) malloc (nnz * sizeof(__float128));
    __float128* xq = (__float128*) malloc (n * sizeof(__float128));
    __float128* yq = (__float128*) malloc (n * sizeof(__float128));
	#pragma omp parallel for
  for (i = 0; i < nnz; i++) 
		Aq[i] = (__float128)A[i];
	#pragma omp parallel for
  for (i = 0; i < n; i++) 
    xq[i] = (__float128)x[i];
  ax_fp128(n, nnz, ia, ja, Aq, xq, yq);
  free(Aq);
  free(xq);

	// ---------------------------------------------
	// original norm computation
	// ---------------------------------------------
  double normA = 0.0;
  double normx = 0.0;
  double* absAabsx = (double*)malloc(n * sizeof(double));
  double* absAe = (double*)malloc(n * sizeof(double));
	#pragma omp parallel for
  for (i = 0; i < n; i++) 
      absAabsx[i] = absAe[i] = 0.0;
  for (i = 0; i < n; i++) {
      for (int k = ia[i]; k < ia[i + 1]; k++) {
          absAabsx[i] += fabs(A[k] * x[ja[k]]);
          absAe[i] += fabs(A[k]);
      }
  }
  for (i = 0; i < n; i++) {
      if (absAe[i] > normA) normA = absAe[i];
      if (fabs(x[i]) > normx) normx = fabs(x[i]);
  }
  double normAnormx = normA * normx;

	// ---------------------------------------------
	// Adaptive-precision SpMV (NW) RPRE
	// ---------------------------------------------
	#pragma omp parallel for
	for(i=0; i<nnz;i++)
			A_scaled[i] = A[i]/normA;

	double* yAp_RE = (double*)malloc(n * sizeof(double));
	struct CSR_RE Ap_RE_NW;
	alloc_CSR_RE (&Ap_RE_NW, n, nnz);
	uint64_t bias64[8] = {0, 0, 0, 0, 0, 0, 0, 0};
	uint32_t bias32[8] = {0, 0, 0, 0, 0, 0, 0, 0};
	init_CSR_RE(&Ap_RE_NW, bias64, bias32);
	convert_CSR_RE(&Ap_RE_NW, ia, nnz, A_scaled, ja, epsi, bias64, bias32);//false = NW
	for(int i = 0; i<8; i++){
		bias32[i] = bias32[i] << 23;
		bias64[i] = bias64[i] << 52;
	}
	for (i = 0; i < repetition; i++) {
		t0 = gettime();
		ax_rpre ( n, Ap_RE_NW, x, yAp_RE, bias64);
		#pragma omp parallel for
		for(j=0; j<n;j++)
			yAp_RE[j] = yAp_RE[j]*normA;
		timesRPRE[i] = gettime() - t0;
	}
	time = getMinTime (timesRPRE, repetition);
	error_NW = spmv_error_NW (n, yAp_RE, yq, normAnormx);
	//CALCUL BYTES
	uint64_t bytes = 1*Ap_RE_NW.nnz8 + 2*Ap_RE_NW.nnz16 + 3*Ap_RE_NW.nnz24 + 4*Ap_RE_NW.nnzfp32 + 4*Ap_RE_NW.nnz32 + 5*Ap_RE_NW.nnz40 + 6*Ap_RE_NW.nnz48 + 7*Ap_RE_NW.nnz56 + 8*Ap_RE_NW.nnz64; //vecteur d'éléments de A
	bytes += 4*Ap_RE_NW.nnz; //vecteur d'indices j
	bytes += 4*n* ((Ap_RE_NW.nnz8!=0) + (Ap_RE_NW.nnz16!=0) + (Ap_RE_NW.nnz24!=0) + (Ap_RE_NW.nnzfp32!=0) + (Ap_RE_NW.nnz32!=0) + (Ap_RE_NW.nnz40!=0) + (Ap_RE_NW.nnz48!=0) + (Ap_RE_NW.nnz56!=0) + (Ap_RE_NW.nnz64!=0)); //vecteurs d'indices i
  //END CALCUL BYTES
	gbs = 1.0e-9 * (double) bytes / time;
	printf("%zd\t%zd\t%zd\t%1.3e\t%1.3e\t%zd\t%1.3e\t", n, m, nnz, time, gbs, bytes, error_NW);
	printf("%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", Ap_RE_NW.nnz0, Ap_RE_NW.nnz8, Ap_RE_NW.nnz16, Ap_RE_NW.nnz24, Ap_RE_NW.nnzfp32, Ap_RE_NW.nnz32, Ap_RE_NW.nnz40, Ap_RE_NW.nnz48, Ap_RE_NW.nnz56, Ap_RE_NW.nnz64, Ap_RE_NW.nnz);
	free_CSR_RE (&Ap_RE_NW);
	free(yAp_RE);

  free(yq);
  free(x);
  free(absAabsx);
  free(absAe);
	destroy_sparse_matrix(hst_A__);

  return 0;
}

