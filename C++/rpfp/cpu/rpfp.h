#ifndef RPFP_H
#define RPFP_H

#define __STDC_FORMAT_MACROS
#include <inttypes.h>
// ATTENTION:
// Do not use "signed" int due to the behavior of bit-shift operations
// mask is required to use unsigned
// e.g.
// sa.i8[i] = (uint8_t)((u32.i >> 8) & 0xFF); // |0(24)f(8)| 
// sa.i16[i] = (uint16_t)((u64.i >> 16) & 0xFFFF); // |0(48)f(16)| 

// -------------------------------
// Type Definition (rpfp)
// -------------------------------
// 16-bit
// s(1)e(8)f(7)
struct rpfp32in16b {
	uint16_t i16;
};
struct rpfp32in16barray {
	uint16_t* i16;
};
// s(1)e(11)f(4)
struct rpfp64in16b {
	uint16_t i16;
};
struct rpfp64in16barray {
	uint16_t* i16;
};
// 24-bit
// s(1)e(8)f(15)
struct rpfp32in24b {
	uint16_t i16;
	uint8_t  i8;
};
struct rpfp32in24barray {
	uint16_t* i16;
	uint8_t*  i8;
};
// s(1)e(11)f(12)
struct rpfp64in24b {
	uint16_t i16;
	uint8_t  i8;
};
struct rpfp64in24barray {
	uint16_t* i16;
	uint8_t*  i8;
};
// 32-bit
// s(1)e(8)f(23)
struct rpfp32in32b {
	float f32;
};
struct rpfp32in32barray {
	float* f32;
};
// s(1)e(11)f(20)
struct rpfp64in32b {
	uint32_t i32;
};
struct rpfp64in32barray {
	uint32_t* i32;
};
struct rpfp64infp32 {
	float f32;
};
struct rpfp64infp32array {
	float* f32;
};
// 40-bit
// s(1)e(11)f(28)
struct rpfp64in40b {
	uint32_t i32;
	uint8_t  i8;
};
struct rpfp64in40barray {
	uint32_t* i32;
	uint8_t*  i8;
};
// 48-bit
// s(1)e(11)f(36)
struct rpfp64in48b {
	uint32_t i32;
	uint16_t i16;
};
struct rpfp64in48barray {
	uint32_t* i32;
	uint16_t* i16;
};
// 56-bit
// s(1)e(11)f(44)
struct rpfp64in56b {
	uint32_t i32;
	uint16_t i16;
	uint8_t  i8;
};
struct rpfp64in56barray {
	uint32_t* i32;
	uint16_t* i16;
	uint8_t*  i8;
};
// 64-bit
// s(1)e(11)f(52)
struct rpfp64in64b {
	double f64;
};
struct rpfp64in64barray {
	double* f64;
};
// s(1)e(11)f(52)
struct rpfp64in64bfp32round {
	uint64_t i64;
};
struct rpfp64in64bfp32roundarray {
	uint64_t* i64;
};



// Reduced exponent reduced precision formats
// 8-bit
// s(1)e(3)f(4)
struct rpre8b {
	uint8_t i8;
};
struct rpre8barray {
	uint8_t* i8;
};
// 16-bit
// s(1)e(3)f(12)
struct rpre16b {
	uint16_t i16;
};
struct rpre16barray {
	uint16_t* i16;
};
// 24-bit
// s(1)e(3)f(20)
struct rpre24b {
	uint16_t i16;
	uint8_t i8;
};
struct rpre24barray {
	uint16_t* i16;
	uint8_t* i8;
};
// 32-bit
// s(1)e(3)f(28)
struct rpre32b {
	uint32_t i32;
};
struct rpre32barray {
	uint32_t* i32;
};
// 40-bit
// s(1)e(3)f(36)
struct rpre40b {
	uint32_t i32;
	uint8_t  i8;
};
struct rpre40barray {
	uint32_t* i32;
	uint8_t*  i8;
};
// 48-bit
// s(1)e(3)f(44)
struct rpre48b {
	uint32_t i32;
	uint16_t  i16;
};
struct rpre48barray {
	uint32_t* i32;
	uint16_t*  i16;
};
// 56-bit
// s(1)e(3)f(52)
struct rpre56b {
	uint32_t i32;
	uint16_t  i16;
	uint8_t  i8;
};
struct rpre56barray {
	uint32_t* i32;
	uint16_t*  i16;
	uint8_t*  i8;
};
// 64-bit
// s(1)e(11)f(52)
struct rpre64b {
	double f64;
};
struct rpre64barray {
	double* f64;
};

// malloc
void rpfpMalloc (rpfp32in16barray &sa, size_t n);
void rpfpMalloc (rpfp64in16barray &sa, size_t n);
void rpfpMalloc (rpfp32in24barray &sa, size_t n);
void rpfpMalloc (rpfp64in24barray &sa, size_t n);
void rpfpMalloc (rpfp32in32barray &sa, size_t n);
void rpfpMalloc (rpfp64in32barray &sa, size_t n);
void rpfpMalloc (rpfp64infp32array &sa, size_t n);
void rpfpMalloc (rpfp64in40barray &sa, size_t n);
void rpfpMalloc (rpfp64in48barray &sa, size_t n);
void rpfpMalloc (rpfp64in56barray &sa, size_t n);
void rpfpMalloc (rpfp64in64barray &sa, size_t n);
void rpfpMalloc (rpfp64in64bfp32roundarray &sa, size_t n);

void rpreMalloc (rpre8barray &sa, size_t n);
void rpreMalloc (rpre16barray &sa, size_t n);
void rpreMalloc (rpre24barray &sa, size_t n);
void rpreMalloc (rpre32barray &sa, size_t n);
void rpreMalloc (rpre40barray &sa, size_t n);
void rpreMalloc (rpre48barray &sa, size_t n);
void rpreMalloc (rpre56barray &sa, size_t n);
void rpreMalloc (rpre64barray &sa, size_t n);

// free
void rpfpFree (rpfp32in16barray &sa);
void rpfpFree (rpfp64in16barray &sa);
void rpfpFree (rpfp32in24barray &sa);
void rpfpFree (rpfp64in24barray &sa);
void rpfpFree (rpfp32in32barray &sa);
void rpfpFree (rpfp64in32barray &sa);
void rpfpFree (rpfp64infp32array &sa);
void rpfpFree (rpfp64in40barray &sa);
void rpfpFree (rpfp64in48barray &sa);
void rpfpFree (rpfp64in56barray &sa);
void rpfpFree (rpfp64in64barray &sa);
void rpfpFree (rpfp64in64bfp32roundarray &sa);

void rpreFree (rpre8barray &sa);
void rpreFree (rpre16barray &sa);
void rpreFree (rpre24barray &sa);
void rpreFree (rpre32barray &sa);
void rpreFree (rpre40barray &sa);
void rpreFree (rpre48barray &sa);
void rpreFree (rpre56barray &sa);
void rpreFree (rpre64barray &sa);

// -------------------------------
// Conversion
// -------------------------------

union union32 {
	uint32_t i;
	float f;
};

union union64 {
	uint64_t i;
	double f;
};

// -------------------------------
// RPFP32IN16B
// -------------------------------
// to FP
__inline__ float
RpToFp (rpfp32in16b sa){
	union union32 u32;
	u32.i = (uint32_t)sa.i16; // |0(16)s(1)e(8)f(7)| 
	u32.i = u32.i << 16; // |s(1)e(8)f(7)0(16)| 
	return u32.f;
}
__inline__ float
RpArrayToFp (rpfp32in16barray sa, size_t i){
	union union32 u32;
	u32.i = (uint32_t)sa.i16[i]; // |0(16)s(1)e(8)f(7)| 
	u32.i = u32.i << 16; // |s(1)e(8)f(7)0(16)| 
	return u32.f;
}
__inline__ rpfp32in16b
RpArrayToRp (rpfp32in16barray sa, size_t i){
	rpfp32in16b sv;
	sv.i16 = sa.i16[i];
	return sv;
}
// to RP
__inline__ rpfp32in16b
FpToRpfp32in16b (float f32){
	rpfp32in16b sa;
	union union32 u32;
	u32.f = f32; // |s(1)e(8)f(23)|
#ifdef RN
	uint32_t i32odd, in32border;
	i32odd = u32.i & 0xFFFF;	// lower part (16bits)
	in32border = 0x8000;			// 10...0 (16bits)
	if ((i32odd > in32border) || ((i32odd == in32border) && (u32.i & 0x1)))
		u32.i+=0x10000;
#endif
	sa.i16 = (uint16_t)(u32.i >> 16); // |0(16)s(1)e(8)f(7)| 
	return sa;
}
__inline__ void
FpToRpArray (float f32, rpfp32in16barray sa, size_t i){
	union union32 u32;
	u32.f = f32; // |s(1)e(8)f(23)|
#ifdef RN
	uint32_t i32odd, in32border;
	i32odd = u32.i & 0xFFFF;	// lower part (16bits)
	in32border = 0x8000;			// 10...0 (16bits)
	if ((i32odd > in32border) || ((i32odd == in32border) && (u32.i & 0x1)))
		u32.i+=0x10000;
#endif
	sa.i16[i] = (uint16_t)(u32.i >> 16); // |0(16)s(1)e(8)f(7)| 
}
__inline__ void
RpToRpArray (rpfp32in16b sv, rpfp32in16barray sa, size_t i){
	sa.i16[i] = sv.i16; 
}

// -------------------------------
// RPFP32IN24B
// -------------------------------
// to FP
__inline__ float
RpToFp (rpfp32in24b sa){
	union union32 u32;
	uint32_t i32h, i32l;
	i32h = (uint32_t)sa.i16; // |0(16)s(1)e(8)f(7)| 
	i32h = i32h << 16; // |s(1)e(8)f(7)0(16)| 
	i32l = (uint32_t)sa.i8; // |0(24)f(8)| 
	i32l = i32l << 8; // |0(16)f(8)0(8)| 
	u32.i = i32h | i32l; // |s(1)e(8)f(15)0(8)|
	return u32.f;
}
__inline__ float
RpArrayToFp (rpfp32in24barray sa, size_t i){
	union union32 u32;
	uint32_t i32h, i32l;
	i32h = (uint32_t)sa.i16[i]; // |0(16)s(1)e(8)f(7)| 
	i32h = i32h << 16; // |s(1)e(8)f(7)0(16)| 
	i32l = (uint32_t)sa.i8[i]; // |0(24)f(8)| 
	i32l = i32l << 8; // |0(16)f(8)0(8)| 
	u32.i = i32h | i32l; // |s(1)e(8)f(15)0(8)|
	return u32.f;
}
/*
struct float2 {
	float x;
	float y;
};
__inline__ float2
RpArrayToFpV2 (rpfp32in24barray sa, size_t i){
	union union32 u32[2];
	uint32_t i32h[2], i32l[2];
	i32h[0] = (uint32_t)sa.i16[i+0]; // |0(16)s(1)e(8)f(7)| 
	i32h[0] = i32h[0] << 16; // |s(1)e(8)f(7)0(16)| 
	i32h[1] = (uint32_t)sa.i16[i+1]; // |0(16)s(1)e(8)f(7)| 
	i32h[1] = i32h[1] << 16; // |s(1)e(8)f(7)0(16)| 

	uint16_t *i16a = (uint16_t*)&sa.i8[i]; // |f1(8)f2(8)| 
	uint16_t i16 = *i16a;
//	printf ("%x\n", i16);
//	i32l[0] = (uint32_t)(sa.i8[i]); // |0(24)f1(8)| 
//	printf ("32: %x\n", i32l[0]);
//	printf ("16: %x\n", (uint8_t)i16);//(uint32_t)(i16 << 24)); // |0(24)f1(8)| 
	i32l[0] = (uint32_t)((uint8_t)i16); // |0(24)f1(8)| 
	i32l[0] = i32l[0] << 8; // |0(16)f(8)0(8)| 
//	i32l[1] = (uint32_t)(sa.i8[i+1]); // |0(24)f2(8)| 
//	printf ("32: %x\n", i32l[1]);
//	printf ("16: %x\n", (uint16_t)i16 >> 8);//(uint32_t)(i16 << 24)); // |0(24)f1(8)| 
	i32l[1] = (uint32_t)((uint16_t)i16 >> 8); // |0(24)f2(8)| 
	i32l[1] = i32l[1] << 8; // |0(16)f(8)0(8)| 

	u32[0].i = i32h[0] | i32l[0]; // |s(1)e(8)f(15)0(8)|
	u32[1].i = i32h[1] | i32l[1]; // |s(1)e(8)f(15)0(8)|
	float2 ret;
	ret.x = u32[0].f;
	ret.y = u32[1].f;
	return ret;
}
*/
__inline__ rpfp32in24b
RpArrayToRp (rpfp32in24barray sa, size_t i){
	rpfp32in24b sv;
	sv.i16 = sa.i16[i];
	sv.i8  = sa.i8[i];
	return sv;
}
// to RP
__inline__ rpfp32in24b
FpToRpfp32in24b (float f32){
	rpfp32in24b sa;
	union union32 u32;
	u32.f = f32; // |s(1)e(8)f(23)|
#ifdef RN
	uint32_t i32odd, in32border;
	i32odd = u32.i & 0xFF;	// lower part (8bits)
	in32border = 0x80;			// 10...0 (8bits)
	if ((i32odd > in32border) || ((i32odd == in32border) && (u32.i & 0x1)))
		u32.i+=0x100;
#endif
	sa.i16 = (uint16_t)(u32.i >> 16); // |0(16)s(1)e(8)f(7)| 
	sa.i8  = (uint8_t)(u32.i >> 8); // |0(24)f(8)| 
	return sa;
}
__inline__ void
FpToRpArray (float f32, rpfp32in24barray sa, size_t i){
	union union32 u32;
	u32.f = f32; // |s(1)e(8)f(23)|
#ifdef RN
	uint32_t i32odd, in32border;
	i32odd = u32.i & 0xFF;	// lower part (8bits)
	in32border = 0x80;			// 10...0 (8bits)
	if ((i32odd > in32border) || ((i32odd == in32border) && (u32.i & 0x1)))
		u32.i+=0x100;
#endif
	sa.i16[i] = (uint16_t)(u32.i >> 16); // |0(16)s(1)e(8)f(7)| 
	sa.i8[i] = (uint8_t)(u32.i >> 8); // |0(24)f(8)| 
}
__inline__ void
RpToRpArray (rpfp32in24b sv, rpfp32in24barray sa, size_t i){
	sa.i16[i] = sv.i16; 
	sa.i8[i]  = sv.i8; 
}

// -------------------------------
// RPFP32IN32B
// -------------------------------
// to FP
__inline__ float
RpToFp (rpfp32in32b sa){
	return sa.f32;
}
__inline__ float
RpArrayToFp (rpfp32in32barray sa, size_t i){
	return sa.f32[i];
}
__inline__ rpfp32in32b
RpArrayToRp (rpfp32in32barray sa, size_t i){
	rpfp32in32b sv;
	sv.f32 = sa.f32[i];
	return sv;
}
// to RP
__inline__ rpfp32in32b
FpToRpfp32in32b (float f32){
	rpfp32in32b sa;
	sa.f32 = f32;
	return sa;
}
__inline__ void
FpToRpArray (float f32, rpfp32in32barray sa, size_t i){
	sa.f32[i] = f32;
}
__inline__ void
RpToRpArray (rpfp32in32b sv, rpfp32in32barray sa, size_t i){
	sa.f32[i] = sv.f32; 
}

// -------------------------------
// RPFP64IN16B
// -------------------------------
// to FP
__inline__ double
RpToFp (rpfp64in16b sa){
	union union64 u64;
	u64.i = (uint64_t)sa.i16; // |0(48)s(1)e(11)f(4)| 
	u64.i = u64.i << 48; // |s(1)e(11)f(4)0(48)| 
	return u64.f;
}
__inline__ double
RpArrayToFp (rpfp64in16barray sa, size_t i){
	union union64 u64;
	u64.i = (uint64_t)sa.i16[i]; // |0(48)s(1)e(11)f(4)| 
	u64.i = u64.i << 48; // |s(1)e(11)f(4)0(48)| 
	return u64.f;
}
__inline__ rpfp64in16b
RpArrayToRp (rpfp64in16barray sa, size_t i){
	rpfp64in16b sv;
	sv.i16 = sa.i16[i];
	return sv;
}
// to RP
__inline__ rpfp64in16b
FpToRpfp64in16b (double f64){
	rpfp64in16b sa;
	union union64 u64;
	u64.f = f64; // |s(1)e(11)f(52)|
#ifdef RN
	uint64_t i64odd, in64border;
	i64odd = u64.i & 0xFFFFFFFFFFFF;	// lower part (48bits)
	in64border = 0x800000000000;			// 10...0 (48bits)
	if ((i64odd > in64border) || ((i64odd == in64border) && (u64.i & 0x1)))
		u64.i+=0x1000000000000;
#endif
	sa.i16 = (uint16_t)(u64.i >> 48); // |0(48)s(1)e(11)f(4)| 
	return sa;
}
__inline__ void
FpToRpArray (double f64, rpfp64in16barray sa, size_t i){
	union union64 u64;
	u64.f = f64; // |s(1)e(11)f(52)|
#ifdef RN
	uint64_t i64odd, in64border;
	i64odd = u64.i & 0xFFFFFFFFFFFF;	// lower part (48bits)
	in64border = 0x800000000000;			// 10...0 (48bits)
	if ((i64odd > in64border) || ((i64odd == in64border) && (u64.i & 0x1)))
		u64.i+=0x1000000000000;
#endif
	sa.i16[i] = (uint16_t)(u64.i >> 48); // |0(48)s(1)e(11)f(4)| 
}
__inline__ void
RpToRpArray (rpfp64in16b sv, rpfp64in16barray sa, size_t i){
	sa.i16[i] = sv.i16; 
}

// -------------------------------
// RPFP64IN24B
// -------------------------------
// to FP
__inline__ double
RpToFp (rpfp64in24b sa){
	union union64 u64;
	uint64_t i64h, i64l;
	i64h = (uint64_t)sa.i16; // |0(48)s(1)e(11)f(4)| 
	i64h = i64h << 48; // |s(1)e(11)f(4)0(48)| 
	i64l = (uint64_t)sa.i8; // |0(56)f(8)| 
	i64l = i64l << 40; // |0(16)f(8)0(40)| 
	u64.i = i64h | i64l; // |s(1)e(11)f(12)0(40)|
	return u64.f;
}
__inline__ double
RpArrayToFp (rpfp64in24barray sa, size_t i){
	union union64 u64;
	uint64_t i64h, i64l;
	i64h = (uint64_t)sa.i16[i]; // |0(48)s(1)e(11)f(4)| 
	i64h = i64h << 48; // |s(1)e(11)f(4)0(48)| 
	i64l = (uint64_t)sa.i8[i]; // |0(56)f(8)| 
	i64l = i64l << 40; // |0(16)f(8)0(40)| 
	u64.i = i64h | i64l; // |s(1)e(11)f(12)0(40)|
	return u64.f;
}
__inline__ rpfp64in24b
RpArrayToRp (rpfp64in24barray sa, size_t i){
	rpfp64in24b sv;
	sv.i16 = sa.i16[i];
	sv.i8  = sa.i8[i];
	return sv;
}
// to RP
__inline__ rpfp64in24b
FpToRpfp64in24b (double f64){
	rpfp64in24b sa;
	union union64 u64;
	u64.f = f64; // |s(1)e(11)f(52)|
#ifdef RN
	uint64_t i64odd, in64border;
	i64odd = u64.i & 0xFFFFFFFFFF;	// lower part (40bits)
	in64border = 0x8000000000;			// 10...0 (40bits)
	if ((i64odd > in64border) || ((i64odd == in64border) && (u64.i & 0x1)))
		u64.i+=0x10000000000;
#endif
	sa.i16 = (uint16_t)(u64.i >> 48); // |0(48)s(1)e(11)f(4)| 
	sa.i8  = (uint8_t)(u64.i >> 40); // |0(56)f(8)| 
	return sa;
}
__inline__ void
FpToRpArray (double f64, rpfp64in24barray sa, size_t i){
	union union64 u64;
	u64.f = f64; // |s(1)e(11)f(52)|
#ifdef RN
	uint64_t i64odd, in64border;
	i64odd = u64.i & 0xFFFFFFFFFF;	// lower part (40bits)
	in64border = 0x8000000000;			// 10...0 (40bits)
	if ((i64odd > in64border) || ((i64odd == in64border) && (u64.i & 0x1)))
		u64.i+=0x10000000000;
#endif
	sa.i16[i] = (uint16_t)(u64.i >> 48); // |0(48)s(1)e(11)f(4)| 
	sa.i8[i] = (uint8_t)(u64.i >> 40); // |0(56)f(8)| 
}
__inline__ void
RpToRpArray (rpfp64in24b sv, rpfp64in24barray sa, size_t i){
	sa.i16[i] = sv.i16; 
	sa.i8[i] = sv.i8; 
}

// -------------------------------
// RPFP64INFP32
// -------------------------------
// to FP
__inline__ double
RpToFp (rpfp64infp32 sa){
	return (double)sa.f32;
}
__inline__ double
RpArrayToFp (rpfp64infp32array sa, size_t i){
	return (double)sa.f32[i];
}
__inline__ rpfp64infp32
RpArrayToRp (rpfp64infp32array sa, size_t i){
	rpfp64infp32 sv;
	sv.f32 = sa.f32[i];
	return sv;
}
// to RP
__inline__ rpfp64infp32
FpToRpfp64infp32 (double f64){
	rpfp64infp32 sa;
	sa.f32 = (float)f64;
	return sa;
}
__inline__ void
FpToRpArray (double f64, rpfp64infp32array sa, size_t i){
	sa.f32[i] = (float)f64; 
}
__inline__ void
RpToRpArray (rpfp64infp32 sv, rpfp64infp32array sa, size_t i){
	sa.f32[i] = sv.f32; 
}

// -------------------------------
// RPFP64IN32B
// -------------------------------
// to FP
__inline__ double
RpToFp (rpfp64in32b sa){
	union union64 u64;
	u64.i = (uint64_t)sa.i32; // |0(32)s(1)e(11)f(20)| 
	u64.i = u64.i << 32; // |s(1)e(11)f(20)0(32)| 
	return u64.f;
}
__inline__ double
RpArrayToFp (rpfp64in32barray sa, size_t i){
	union union64 u64;
	u64.i = (uint64_t)sa.i32[i]; // |0(32)s(1)e(11)f(20)| 
	u64.i = u64.i << 32; // |s(1)e(11)f(20)0(32)| 
	return u64.f;
}
__inline__ rpfp64in32b
RpArrayToRp (rpfp64in32barray sa, size_t i){
	rpfp64in32b sv;
	sv.i32 = sa.i32[i];
	return sv;
}
// to RP
__inline__ rpfp64in32b
FpToRpfp64in32b (double f64){
	rpfp64in32b sa;
	union union64 u64;
	u64.f = f64; // |s(1)e(11)f(52)|
#ifdef RN
	uint64_t i64odd, in64border;
	i64odd = u64.i & 0xFFFFFFFF;	// lower part (32bits)
	in64border = 0x80000000;			// 10...0 (32bits)
	if ((i64odd > in64border) || ((i64odd == in64border) && (u64.i & 0x1)))
		u64.i+=0x100000000;
#endif
	sa.i32 = (uint32_t)(u64.i >> 32); // |0(32)s(1)e(11)f(20)| 
	return sa;
}
__inline__ void
FpToRpArray (double f64, rpfp64in32barray sa, size_t i){
	union union64 u64;
	u64.f = f64; // |s(1)e(11)f(52)|
#ifdef RN
	uint64_t i64odd, in64border;
	i64odd = u64.i & 0xFFFFFFFF;	// lower part (32bits)
	in64border = 0x80000000;			// 10...0 (32bits)
	if ((i64odd > in64border) || ((i64odd == in64border) && (u64.i & 0x1)))
		u64.i+=0x100000000;
#endif
	sa.i32[i] = (uint32_t)(u64.i >> 32); // |0(32)s(1)e(11)f(20)| 
}
__inline__ void
RpToRpArray (rpfp64in32b sv, rpfp64in32barray sa, size_t i){
	sa.i32[i] = sv.i32; 
}

// -------------------------------
// RPFP64IN40B
// -------------------------------
// to FP
__inline__ double
RpToFp (rpfp64in40b sa){
	union union64 u64;
	uint64_t i64h, i64l;
	i64h = (uint64_t)sa.i32; // |0(32)s(1)e(11)f(20)| 
	i64h = i64h << 32; // |s(1)e(11)f(20)0(32)| 
	i64l = (uint64_t)sa.i8; // |0(56)f(8)| 
	i64l = i64l << 24; // |0(32)f(8)0(24)| 
	u64.i = i64h | i64l; // |s(1)e(11)f(28)0(24)|
	return u64.f;
}
__inline__ double
RpArrayToFp (rpfp64in40barray sa, size_t i){
	union union64 u64;
	uint64_t i64h, i64l;
	i64h = (uint64_t)sa.i32[i]; // |0(32)s(1)e(11)f(20)| 
	i64h = i64h << 32; // |s(1)e(11)f(20)0(32)| 
	i64l = (uint64_t)sa.i8[i]; // |0(56)f(8)| 
	i64l = i64l << 24; // |0(32)f(8)0(24)| 
	u64.i = i64h | i64l; // |s(1)e(11)f(28)0(24)|
	return u64.f;
}
__inline__ rpfp64in40b
RpArrayToRp (rpfp64in40barray sa, size_t i){
	rpfp64in40b sv;
	sv.i32 = sa.i32[i];
	sv.i8  = sa.i8[i];
	return sv;
}
// to RP
__inline__ rpfp64in40b
FpToRpfp64in40b (double f64){
	rpfp64in40b sa;
	union union64 u64;
	u64.f = f64; // |s(1)e(11)f(52)|
#ifdef RN
	uint64_t i64odd, in64border;
	i64odd = u64.i & 0xFFFFFF;	// lower part (24bits)
	in64border = 0x800000;			// 10...0 (24bits)
	if ((i64odd > in64border) || ((i64odd == in64border) && (u64.i & 0x1)))
		u64.i+=0x1000000;
#endif
	sa.i32 = (uint32_t)(u64.i >> 32); // |0(32)s(1)e(11)f(20)| 
	sa.i8  = (uint8_t)(u64.i >> 24); // |0(56)f(8)| 
	return sa;
}
__inline__ void
FpToRpArray (double f64, rpfp64in40barray sa, size_t i){
	union union64 u64;
	u64.f = f64; // |s(1)e(11)f(52)|
#ifdef RN
	uint64_t i64odd, in64border;
	i64odd = u64.i & 0xFFFFFF;	// lower part (24bits)
	in64border = 0x800000;			// 10...0 (24bits)
	if ((i64odd > in64border) || ((i64odd == in64border) && (u64.i & 0x1)))
		u64.i+=0x1000000;
#endif
	sa.i32[i] = (uint32_t)(u64.i >> 32); // |0(32)s(1)e(11)f(20)| 
	sa.i8[i] = (uint8_t)(u64.i >> 24); // |0(56)f(8)| 
}
__inline__ void
RpToRpArray (rpfp64in40b sv, rpfp64in40barray sa, size_t i){
	sa.i32[i] = sv.i32; 
	sa.i8[i] = sv.i8; 
}

// -------------------------------
// RPFP64IN48B
// -------------------------------
// to FP
__inline__ double
RpToFp (rpfp64in48b sa){
	union union64 u64;
	uint64_t i64h, i64l;
	i64h = (uint64_t)sa.i32; // |0(32)s(1)e(11)f(20)| 
	i64h = i64h << 32; // |s(1)e(11)f(20)0(32)| 
	i64l = (uint64_t)sa.i16; // |0(48)f(16)| 
	i64l = i64l << 16; // |0(32)f(16)0(16)| 
	i64h = i64h | i64l; // |s(1)e(11)f(36)0(16)|
	u64.i = i64h;
	return u64.f;
}
__inline__ double
RpArrayToFp (rpfp64in48barray sa, size_t i){
	union union64 u64;
	uint64_t i64h, i64l;
	i64h = (uint64_t)sa.i32[i]; // |0(32)s(1)e(11)f(20)| 
	i64h = i64h << 32; // |s(1)e(11)f(20)0(32)| 
	i64l = (uint64_t)sa.i16[i]; // |0(48)f(16)| 
	i64l = i64l << 16; // |0(32)f(16)0(16)| 
	u64.i = i64h | i64l; // |s(1)e(11)f(36)0(16)|
	return u64.f;
}
__inline__ rpfp64in48b
RpArrayToRp (rpfp64in48barray sa, size_t i){
	rpfp64in48b sv;
	sv.i32 = sa.i32[i];
	sv.i16 = sa.i16[i];
	return sv;
}
// to RP
__inline__ rpfp64in48b
FpToRpfp64in48b (double f64){
	rpfp64in48b sa;
	union union64 u64;
	u64.f = f64;				// |s(1)e(11)f(52)|
#ifdef RN
	uint64_t i64odd, in64border;
	i64odd = u64.i & 0xFFFF;	// lower part (16bits)
	in64border = 0x8000;			// 10...0 (16bits)
	if ((i64odd > in64border) || ((i64odd == in64border) && (u64.i & 0x1)))
		u64.i+=0x10000;
#endif
	sa.i32 = (uint32_t)(u64.i >> 32); // |0(32)s(1)e(11)f(20)| 
	sa.i16 = (uint16_t)(u64.i >> 16); // |0(48)f(16)| 
	return sa;
}
__inline__ void
FpToRpArray (double f64, rpfp64in48barray sa, size_t i){
	union union64 u64;
	u64.f = f64; // |s(1)e(11)f(52)|
#ifdef RN
	uint64_t i64odd, in64border;
	i64odd = u64.i & 0xFFFF;	// lower part (16bits)
	in64border = 0x8000;			// 10...0 (16bits)
	if ((i64odd > in64border) || ((i64odd == in64border) && (u64.i & 0x1)))
		u64.i+=0x10000;
#endif
	sa.i32[i] = (uint32_t)(u64.i >> 32); // |0(32)s(1)e(11)f(20)| 
	sa.i16[i] = (uint16_t)(u64.i >> 16); // |0(48)f(16)| 
}
__inline__ void
RpToRpArray (rpfp64in48b sv, rpfp64in48barray sa, size_t i){
	sa.i32[i] = sv.i32; 
	sa.i16[i] = sv.i16; 
}

// -------------------------------
// RPFP64IN56B
// -------------------------------
// to FP
__inline__ double
RpToFp (rpfp64in56b sa){
	union union64 u64;
	uint64_t i64h, i64m, i64l;
	i64h = (uint64_t)sa.i32; // |0(32)s(1)e(11)f(20)| 
	i64h = i64h << 32; // |s(1)e(11)f(20)0(32)| 
	i64m = (uint64_t)sa.i16; // |0(48)f(16)| 
	i64m = i64m << 16; // |0(32)f(16)0(16)| 
	i64l = (uint64_t)sa.i8; // |0(56)f(8)| 
	i64l = i64l << 8; // |0(48)f(8)0(8)| 
	u64.i = (i64h | i64m) | i64l; // |s(1)e(11)f(44)0(8)|
	return u64.f;
}
__inline__ double
RpArrayToFp (rpfp64in56barray sa, size_t i){
	union union64 u64;
	uint64_t i64h, i64m, i64l;
	i64h = (uint64_t)sa.i32[i]; // |0(32)s(1)e(11)f(20)| 
	i64h = i64h << 32; // |s(1)e(11)f(20)0(32)| 
	i64m = (uint64_t)sa.i16[i]; // |0(48)f(16)| 
	i64m = i64m << 16; // |0(32)f(16)0(16)| 
	i64l = (uint64_t)sa.i8[i]; // |0(56)f(8)| 
	i64l = i64l << 8; // |0(48)f(8)0(8)| 
	u64.i = (i64h | i64m) | i64l; // |s(1)e(11)f(44)0(8)|
	return u64.f;
}
__inline__ rpfp64in56b
RpArrayToRp (rpfp64in56barray sa, size_t i){
	rpfp64in56b sv;
	sv.i32 = sa.i32[i];
	sv.i16 = sa.i16[i];
	sv.i8 = sa.i8[i];
	return sv;
}
// to RP
__inline__ rpfp64in56b
FpToRpfp64in56b (double f64){
	rpfp64in56b sa;
	union union64 u64;
	u64.f = f64;				// |s(1)e(11)f(52)|
#ifdef RN
	uint64_t i64odd, in64border;
	i64odd = u64.i & 0xFF;	// lower part (8bits)
	in64border = 0x80;			// 10...0 (8bits)
	if ((i64odd > in64border) || ((i64odd == in64border) && (u64.i & 0x1)))
		u64.i+=0x100;
#endif
	sa.i32 = (uint32_t)(u64.i >> 32); // |0(32)s(1)e(11)f(20)| 
	sa.i16 = (uint16_t)(u64.i >> 16); // |0(48)f(16)| 
	sa.i8 = (uint8_t)(u64.i >> 8); // |0(56)f(8)| 
	return sa;
}
__inline__ void
FpToRpArray (double f64, rpfp64in56barray sa, size_t i){
	union union64 u64;
	u64.f = f64; // |s(1)e(11)f(52)|
#ifdef RN
	uint64_t i64odd, in64border;
	i64odd = u64.i & 0xFF;	// lower part (8bits)
	in64border = 0x80;			// 10...0 (8bits)
	if ((i64odd > in64border) || ((i64odd == in64border) && (u64.i & 0x1)))
		u64.i+=0x100;
#endif
	sa.i32[i] = (uint32_t)(u64.i >> 32); // |0(32)s(1)e(11)f(20)| 
	sa.i16[i] = (uint16_t)(u64.i >> 16); // |0(48)f(16)| 
	sa.i8[i]  = (uint8_t)(u64.i >> 8); // |0(56)f(8)| 
}
__inline__ void
RpToRpArray (rpfp64in56b sv, rpfp64in56barray sa, size_t i){
	sa.i32[i] = sv.i32; 
	sa.i16[i] = sv.i16; 
	sa.i8[i] = sv.i8; 
}

// -------------------------------
// RPFP64IN64B
// -------------------------------
// to FP
__inline__ double
RpToFp (rpfp64in64b sa){
	return sa.f64;
}
__inline__ double
RpArrayToFp (rpfp64in64barray sa, size_t i){
	return sa.f64[i];
}
__inline__ rpfp64in64b
RpArrayToRp (rpfp64in64barray sa, size_t i){
	rpfp64in64b sv;
	sv.f64 = sa.f64[i];
	return sv;
}
// to RP
__inline__ rpfp64in64b
FpToRpfp64in64b (double f64){
	rpfp64in64b sa;
	sa.f64 = f64;
	return sa;
}
__inline__ void
FpToRpArray (double f64, rpfp64in64barray sa, size_t i){
	sa.f64[i] = f64;
}
__inline__ void
RpToRpArray (rpfp64in64b sv, rpfp64in64barray sa, size_t i){
	sa.f64[i] = sv.f64; 
}

// -------------------------------
// sizeof
// -------------------------------
template <typename T> uint32_t sizeof_rp ();
template <> uint32_t sizeof_rp <rpfp64in64barray> ();
template <> uint32_t sizeof_rp <rpfp64in56barray> ();
template <> uint32_t sizeof_rp <rpfp64in48barray> ();
template <> uint32_t sizeof_rp <rpfp64in40barray> ();
template <> uint32_t sizeof_rp <rpfp64in32barray> ();
template <> uint32_t sizeof_rp <rpfp64in24barray> ();
template <> uint32_t sizeof_rp <rpfp64in16barray> ();
template <> uint32_t sizeof_rp <rpfp32in32barray> ();
template <> uint32_t sizeof_rp <rpfp32in24barray> ();
template <> uint32_t sizeof_rp <rpfp32in16barray> ();

template <> uint32_t sizeof_rp <rpre8barray> ();
template <> uint32_t sizeof_rp <rpre16barray> ();
template <> uint32_t sizeof_rp <rpre24barray> ();
template <> uint32_t sizeof_rp <rpre32barray> ();
template <> uint32_t sizeof_rp <rpre40barray> ();
template <> uint32_t sizeof_rp <rpre48barray> ();
template <> uint32_t sizeof_rp <rpre56barray> ();

#endif
