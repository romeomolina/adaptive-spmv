#include <stdio.h>
#include <stdlib.h>
#include "rpfp.h"

// malloc
void rpfpMalloc (rpfp32in16barray &sa, size_t n) {
	sa.i16 = (uint16_t *) malloc (2*n);
}
void rpfpMalloc (rpfp64in16barray &sa, size_t n) {
	sa.i16 = (uint16_t *) malloc (2*n);
}
void rpfpMalloc (rpfp32in24barray &sa, size_t n) {
	sa.i16 = (uint16_t *) malloc (2*n);
	sa.i8  = (uint8_t *) malloc (1*n);
}
void rpfpMalloc (rpfp64in24barray &sa, size_t n) {
	sa.i16 = (uint16_t *) malloc (2*n);
	sa.i8  = (uint8_t *) malloc (1*n);
}
void rpfpMalloc (rpfp32in32barray &sa, size_t n) {
	sa.f32 = (float *) malloc (4*n);
}
void rpfpMalloc (rpfp64infp32array &sa, size_t n) {
	sa.f32 = (float *) malloc (4*n);
}
void rpfpMalloc (rpfp64in32barray &sa, size_t n) {
	sa.i32 = (uint32_t *) malloc (4*n);
}
void rpfpMalloc (rpfp64in40barray &sa, size_t n) {
	sa.i32 = (uint32_t *) malloc (4*n);
	sa.i8  = (uint8_t *) malloc (1*n);
}
void rpfpMalloc (rpfp64in48barray &sa, size_t n) {
	sa.i32 = (uint32_t *) malloc (4*n);
	sa.i16 = (uint16_t *) malloc (2*n);
}
void rpfpMalloc (rpfp64in56barray &sa, size_t n) {
	sa.i32 = (uint32_t *) malloc (4*n);
	sa.i16 = (uint16_t *) malloc (2*n);
	sa.i8  = (uint8_t *) malloc (1*n);
}
void rpfpMalloc (rpfp64in64barray &sa, size_t n) {
	sa.f64 = (double *) malloc (8*n);
}

// RPRE MALLOC
void rpreMalloc (rpre8barray &sa, size_t n) {
	sa.i8 = (uint8_t *) malloc (1*n);
}
void rpreMalloc (rpre16barray &sa, size_t n) {
	sa.i16 = (uint16_t *) malloc (2*n);
}
void rpreMalloc (rpre24barray &sa, size_t n) {
	sa.i8 = (uint8_t *) malloc (1*n);
	sa.i16 = (uint16_t *) malloc (2*n);
}
void rpreMalloc (rpre32barray &sa, size_t n) {
	sa.i32 = (uint32_t *) malloc (4*n);
}
void rpreMalloc (rpre40barray &sa, size_t n) {
	sa.i8 = (uint8_t *) malloc (1*n);
	sa.i32 = (uint32_t *) malloc (4*n);
}
void rpreMalloc (rpre48barray &sa, size_t n) {
	sa.i16 = (uint16_t *) malloc (2*n);
	sa.i32 = (uint32_t *) malloc (4*n);
}
void rpreMalloc (rpre56barray &sa, size_t n) {
	sa.i8 = (uint8_t *) malloc (1*n);
	sa.i16 = (uint16_t *) malloc (2*n);
	sa.i32 = (uint32_t *) malloc (4*n);
}

// free
void rpfpFree (rpfp32in16barray &sa) {
	free (sa.i16);
}
void rpfpFree (rpfp64in16barray &sa) {
	free (sa.i16);
}
void rpfpFree (rpfp32in24barray &sa) {
	free (sa.i16);
	free (sa.i8);
}
void rpfpFree (rpfp64in24barray &sa) {
	free (sa.i16);
	free (sa.i8);
}
void rpfpFree (rpfp32in32barray &sa) {
	free (sa.f32);
}
void rpfpFree (rpfp64infp32array &sa) {
	free (sa.f32);
}
void rpfpFree (rpfp64in32barray &sa) {
	free (sa.i32);
}
void rpfpFree (rpfp64in40barray &sa) {
	free (sa.i32);
	free (sa.i8);
}
void rpfpFree (rpfp64in48barray &sa) {
	free (sa.i32);
	free (sa.i16);
}
void rpfpFree (rpfp64in56barray &sa) {
	free (sa.i32);
	free (sa.i16);
	free (sa.i8);
}
void rpfpFree (rpfp64in64barray &sa) {
	free (sa.f64);
}

// RPRE FREE
void rpreFree (rpre8barray &sa) {
	free (sa.i8);
}
void rpreFree (rpre16barray &sa) {
	free (sa.i16);
}
void rpreFree (rpre24barray &sa) {
	free (sa.i8);
	free (sa.i16);
}
void rpreFree (rpre32barray &sa) {
	free (sa.i32);
}
void rpreFree (rpre40barray &sa) {
	free (sa.i8);
	free (sa.i32);
}
void rpreFree (rpre48barray &sa) {
	free (sa.i16);
	free (sa.i32);
}
void rpreFree (rpre56barray &sa) {
	free (sa.i8);
	free (sa.i16);
	free (sa.i32);
}

// sizeof
template <typename T> uint32_t sizeof_rp () { return 0; }
template <> uint32_t sizeof_rp <rpfp64in64barray> () { return 64; }
template <> uint32_t sizeof_rp <rpfp64in56barray> () { return 56; }
template <> uint32_t sizeof_rp <rpfp64in48barray> () { return 48; }
template <> uint32_t sizeof_rp <rpfp64in40barray> () { return 40; }
template <> uint32_t sizeof_rp <rpfp64in32barray> () { return 32; }
template <> uint32_t sizeof_rp <rpfp64in24barray> () { return 24; }
template <> uint32_t sizeof_rp <rpfp64in16barray> () { return 16; }
template <> uint32_t sizeof_rp <rpfp32in32barray> () { return 32; }
template <> uint32_t sizeof_rp <rpfp32in24barray> () { return 24; }
template <> uint32_t sizeof_rp <rpfp32in16barray> () { return 16; }

template uint32_t sizeof_rp <rpfp64in64barray> ();
template uint32_t sizeof_rp <rpfp64in56barray> ();
template uint32_t sizeof_rp <rpfp64in48barray> ();
template uint32_t sizeof_rp <rpfp64in40barray> ();
template uint32_t sizeof_rp <rpfp64in32barray> ();
template uint32_t sizeof_rp <rpfp64in24barray> ();
template uint32_t sizeof_rp <rpfp64in16barray> ();
template uint32_t sizeof_rp <rpfp32in32barray> ();
template uint32_t sizeof_rp <rpfp32in24barray> ();
template uint32_t sizeof_rp <rpfp32in16barray> ();

template <> uint32_t sizeof_rp <rpre56barray> () { return 56; }
template <> uint32_t sizeof_rp <rpre48barray> () { return 48; }
template <> uint32_t sizeof_rp <rpre40barray> () { return 40; }
template <> uint32_t sizeof_rp <rpre32barray> () { return 32; }
template <> uint32_t sizeof_rp <rpre24barray> () { return 24; }
template <> uint32_t sizeof_rp <rpre16barray> () { return 16; }
template <> uint32_t sizeof_rp <rpre8barray> () { return 8; }

template uint32_t sizeof_rp <rpre56barray> ();
template uint32_t sizeof_rp <rpre48barray> ();
template uint32_t sizeof_rp <rpre40barray> ();
template uint32_t sizeof_rp <rpre32barray> ();
template uint32_t sizeof_rp <rpre24barray> ();
template uint32_t sizeof_rp <rpre16barray> ();
template uint32_t sizeof_rp <rpre8barray> ();

