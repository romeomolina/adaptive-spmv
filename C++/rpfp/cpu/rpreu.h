#include <rpre.h>

// -------------------------------
// RPRE8B FROM FP32
// 5 bits de mantisse
// -------------------------------
// to FP

__inline__ float
RpArrayToFp32_s (rpre8barray sa, size_t i){
	union union32 u32;
	u32.i = b_expfp32 | ((uint32_t) sa.i8[i] ) << 18;
	return u32.f;
}
// to RP
__inline__ void
Fp32ToRpArray_s (float f32, rpre8barray sa, size_t i, uint32_t bias){
	union union32 u32;
	u32.f = f32; // |s(1)e(8)f(23)|
#ifdef RN
	// TODO
#endif
	uint8_t e = (uint8_t) (((u32.i >> 23) & 0xFF) - bias); 
	uint8_t m = (uint8_t) ((u32.i >> 18) & 0x1F);
	sa.i8[i] = (e<<5) | m;
}

// -------------------------------
// RPRE16B FROM FP32
// 13 bits de mantisse
// -------------------------------
// to FP
__inline__ float
RpArrayToFp32_s (rpre16barray sa, size_t i){
	union union32 u32;
	u32.i = b_expfp32 | ((uint32_t) sa.i16[i] ) << 10;
	return u32.f;
}
// to RP
__inline__ void
Fp32ToRpArray_s (float f32, rpre16barray sa, size_t i, uint32_t bias){
	union union32 u32;
	u32.f = f32; // |s(1)e(8)f(23)|
#ifdef RN
	// TODO
#endif
	uint16_t e = (uint16_t) (((u32.i >> 23) & 0xFF) - bias); 
	uint16_t m = (uint16_t) ((u32.i >> 10) & 0x1FFF);
	sa.i16[i] = (e<<13) | m;
}

// -------------------------------
// RPRE24B FROM FP32
// 21 bits de mantisse
// -------------------------------
// to FP
__inline__ float
RpArrayToFp32_s (rpre24barray sa, size_t i){
	union union32 u32;
	u32.i = b_expfp32 | ((uint32_t) sa.i8[i] ) << (2+16) |  ((uint32_t)sa.i16[i]) << 2;
	return u32.f;
}
// to RP
__inline__ void
Fp32ToRpArray_s (float f32, rpre24barray sa, size_t i, uint32_t bias){
	union union32 u32;
	u32.f = f32; 
#ifdef RN
	// TODO
#endif
	uint8_t e = (uint8_t) (((u32.i >> 23) & 0xFF) - bias); 
	uint8_t mh = (uint8_t) ((u32.i >> 18) & 0x1F);
	sa.i8[i] = (e<<5) | mh;
	sa.i16[i] = (uint16_t) ((u32.i >> 2) & 0xFFFF);
}

// -------------------------------
// RPRE32B
// 29 bits de mantisse
// -------------------------------
// to FP
__inline__ double
RpArrayToFp_s (rpre32barray sa, size_t i){
	union union64 u64;
	u64.i = b_expfp64 | (uint64_t) (sa.i32[i]) << 23;
	return u64.f;
}
// to RP
__inline__ void
FpToRpArray_s (double f64, rpre32barray sa, size_t i, uint64_t bias){
	union union64 u64;
	u64.f = f64;
#ifdef RN
	// TODO
#endif
	uint32_t e = (uint32_t) (((u64.i >> 52) & 0x7FF) - bias); 
	uint32_t m = (uint32_t) ((u64.i >> 23) & 0x1FFFFFFF);
	sa.i32[i] = (e<<29) | m;
}

// -------------------------------
// RPRE40B
// 37 bits de mantisse
// -------------------------------
// to FP
__inline__ double
RpArrayToFp_s (rpre40barray sa, size_t i){
	union union64 u64;
	u64.i = b_expfp64 | ((uint64_t) sa.i8[i]) << (32+15) | ((uint64_t) sa.i32[i])<<15;
	return u64.f;
}
// to RP
__inline__ void
FpToRpArray_s (double f64, rpre40barray sa, size_t i, uint64_t bias){
	union union64 u64;
	u64.f = f64; // |s(1)e(8)f(23)|
#ifdef RN
	// TODO
#endif
	uint8_t e = (uint8_t) (((u64.i >> 52) & 0x7FF) - bias); 
	uint8_t mh = (uint8_t) ((u64.i >> (32+15)) & 0x1F);
	sa.i8[i] = (e<<5) | mh;
	sa.i32[i] = (uint32_t) ((u64.i >> 15) & 0xFFFFFFFF);
}

// -------------------------------
// RPRE48B
// 45 bits de mantisse
// -------------------------------
// to FP
__inline__ double
RpArrayToFp_s (rpre48barray sa, size_t i){
	union union64 u64;
	u64.i = b_expfp64 | ((uint64_t) sa.i16[i]) <<(32+7) | ((uint64_t) sa.i32[i])<<7;
	return u64.f;
}
// to RP
__inline__ void
FpToRpArray_s (double f64, rpre48barray sa, size_t i, uint64_t bias){
	union union64 u64;
	u64.f = f64; 
#ifdef RN
	// TODO
#endif
	uint16_t e = (uint16_t) (((u64.i >> 52) & 0x7FF) - bias); // |0(13)e(3)|)
	uint16_t mh = (uint16_t) ((u64.i >> 39) & 0x1FFF); // |0(4)mh(12)|
	sa.i16[i] = (e<<13) | mh; // |s(1)e(3)mh(12)|
	sa.i32[i] = (uint32_t) ((u64.i >> 7) & 0xFFFFFFFF); // |ml(32)|
}

// -------------------------------
// RPREU56
// 52 bits de mantisse
// -------------------------------
// to FP
__inline__ double
RpArrayToFp_s (rpre56barray sa, size_t i){
	union union64 u64;
	u64.i = b_expfp64 | (uint64_t (sa.i8[i] & 0x7F)) << (32+16) | ((uint64_t) sa.i16[i]) << 32 | ((uint64_t) sa.i32[i]);
	return u64.f;
}
// to RP
__inline__ void
FpToRpArray_s (double f64, rpre56barray sa, size_t i, uint64_t bias){
	union union64 u64;
	u64.f = f64; 
	uint8_t s = (uint8_t) (u64.i >> 63); // |0(63)s(1)|
	uint8_t e = (uint8_t) (((u64.i >> 52) & 0x7FF) - bias); // |0(13)e(3)|)
	uint8_t mhh = ((uint8_t) (u64.i >> 48) & 0xF);
	sa.i8[i] = (s<<7) | (e<<4) | mhh;
	sa.i16[i] = ((uint16_t) (u64.i >> 32) & 0xFFFF);
	sa.i32[i] = (uint32_t) (u64.i & 0xFFFFFFFF);
}

