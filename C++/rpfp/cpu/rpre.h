uint32_t b_expfp32 = 1<<30;
uint64_t b_expfp64 = 0x4000000000000000;

__inline__ uint64_t 
rpre_getbias64(double tmp){
	union union64 u64;
	u64.f = tmp;
	u64.i = (u64.i >> 52) & 0x7FF;
	return u64.i;
}
__inline__ uint32_t 
rpre_getbias32(float tmp){
	union union32 u32;
	u32.f = tmp;
	u32.i = ((u32.i >> 23) & 0xFF);
	return u32.i;
}

__inline__ double 
rpre_rebias32(float tmp, uint32_t bias){
	union union32 u32;
	if (tmp == 0.)
		return 0.;
	u32.f = tmp;
	u32.i = u32.i - b_expfp32 + bias;
	return double(u32.f);
}

__inline__ double 
rpre_rebias64(double tmp, uint64_t bias){
	union union64 u64;
	if (tmp == 0.)
		return 0.;
	u64.f = tmp;
	u64.i = u64.i - b_expfp64 + bias;
	return u64.f;
}
// Gros travail ici pour bien convertir les exposants.
// Besoin d'un paramètre supplémentaire indiquant le biais correspondant pour retrouver le vrai exposant

// -------------------------------
// RPRE8B FROM FP32
// 4 bits de mantisse
// -------------------------------
// to FP

__inline__ float
RpArrayToFp32 (rpre8barray sa, size_t i){
	union union32 u32;
//	u32.i = (((uint32_t) (sa.i8[i] & 0x80)) << 24 | ((uint32_t) (sa.i8[i] & 0x7F)) << 19) + bias;
//	u32.i = (((uint32_t) (sa.i8[i] & 0x80)) << 24 | ((uint32_t) (sa.i8[i] & 0x7F)) << 19) | b_expfp32;
	u32.i = (((uint32_t) ((sa.i8[i] & 0x80) | 0x40)) << 24 | ((uint32_t) (sa.i8[i] & 0x7F)) << 19);
	return u32.f;
}
// to RP
__inline__ void
Fp32ToRpArray (float f32, rpre8barray sa, size_t i, uint32_t bias){
	union union32 u32;
	u32.f = f32; // |s(1)e(8)f(23)|
#ifdef RN
	// TODO
#endif
	uint8_t s = (uint8_t) (u32.i >> 31);
	uint8_t e = (uint8_t) (((u32.i >> 23) & 0xFF) - bias); 
	uint8_t m = (uint8_t) ((u32.i >> 19) & 0xF);
	sa.i8[i] = (s<<7) | (e<<4) | m;
}

// -------------------------------
// RPRE16B FROM FP32
// 12 bits de mantisse
// -------------------------------
// to FP
__inline__ float
RpArrayToFp32 (rpre16barray sa, size_t i){
	union union32 u32;
//	u32.i = (((uint32_t) (sa.i16[i] & 0x8000)) << 16 | ((uint32_t) (sa.i16[i] & 0x7FFF)) << 11) + bias;
//	u32.i = (((uint32_t) (sa.i16[i] & 0x8000)) << 16 | ((uint32_t) (sa.i16[i] & 0x7FFF)) << 11) | b_expfp32;
	u32.i = (((uint32_t) ((sa.i16[i] & 0x8000) | 0x4000)) << 16 | ((uint32_t) (sa.i16[i] & 0x7FFF)) << 11);
	return u32.f;
}
// to RP
__inline__ void
Fp32ToRpArray (float f32, rpre16barray sa, size_t i, uint32_t bias){
	union union32 u32;
	u32.f = f32; // |s(1)e(8)f(23)|
#ifdef RN
	// TODO
#endif
	uint16_t s = (uint16_t) (u32.i >> 31);
	uint16_t e = (uint16_t) (((u32.i >> 23) & 0xFF) - bias); 
	uint16_t m = (uint16_t) ((u32.i >> 11) & 0xFFF);
	sa.i16[i] = (s<<15) | (e<<12) | m;
}

// -------------------------------
// RPRE24B FROM FP32
// 20 bits de mantisse
// -------------------------------
// to FP
__inline__ float
RpArrayToFp32 (rpre24barray sa, size_t i){
	union union32 u32;
//	u32.i = (((uint32_t) (sa.i8[i] & 0x80)) << 24 | ((uint32_t) (sa.i8[i] & 0x7F)) << (3+16) |  ((uint32_t)sa.i16[i]) << 3) + bias;
//	u32.i = (((uint32_t) (sa.i8[i] & 0x80)) << 24 | ((uint32_t) (sa.i8[i] & 0x7F)) << (3+16) |  ((uint32_t)sa.i16[i]) << 3) | b_expfp32;
	u32.i = (((uint32_t) ((sa.i8[i] & 0x80) | 0x40)) << 24 | ((uint32_t) (sa.i8[i] & 0x7F)) << (3+16) |  ((uint32_t)sa.i16[i]) << 3);
	return u32.f;
}
// to RP
__inline__ void
Fp32ToRpArray (float f32, rpre24barray sa, size_t i, uint32_t bias){
	union union32 u32;
	u32.f = f32; 
#ifdef RN
	// TODO
#endif
	uint8_t s = (uint8_t) (u32.i >> 31);
	uint8_t e = (uint8_t) (((u32.i >> 23) & 0xFF) - bias); 
	uint8_t mh = (uint8_t) ((u32.i >> 19) & 0xF);
	sa.i8[i] = (s<<7) | (e<<4) | mh;
	sa.i16[i] = (uint16_t) ((u32.i >> 3) & 0xFFFF);
}

// -------------------------------
// RPRE32B
// 28 bits de mantisse
// -------------------------------
// to FP
__inline__ double
RpArrayToFp (rpre32barray sa, size_t i){
	union union64 u64;
//	u64.i = (((uint64_t) (sa.i32[i] & 0x80000000)) << 32 | ((uint64_t) (sa.i32[i] & 0x7FFFFFFF)) << 24) + bias;
//	u64.i = (((uint64_t) (sa.i32[i] & 0x80000000)) << 32 | ((uint64_t) (sa.i32[i] & 0x7FFFFFFF)) << 24) | b_expfp64;
	u64.i = (((uint64_t) ((sa.i32[i] & 0x80000000) | 0x40000000)) << 32 | ((uint64_t) (sa.i32[i] & 0x7FFFFFFF)) << 24);
	return u64.f;
}
// to RP
__inline__ void
FpToRpArray (double f64, rpre32barray sa, size_t i, uint64_t bias){
	union union64 u64;
	u64.f = f64;
#ifdef RN
	// TODO
#endif
	uint32_t s = (uint32_t) (u64.i >> 63);
	uint32_t e = (uint32_t) (((u64.i >> 52) & 0x7FF) - bias); 
	uint32_t m = (uint32_t) ((u64.i >> 24) & 0xFFFFFFF);
	sa.i32[i] = (s<<31) | (e<<28) | m;
}

// -------------------------------
// RPRE40B
// 36 bits de mantisse
// -------------------------------
// to FP
__inline__ double
RpArrayToFp (rpre40barray sa, size_t i){
	union union64 u64;
//	u64.i = (((uint64_t) (sa.i8[i] & 0x80)) << 56 | ((uint64_t) (sa.i8[i] & 0x7F)) << (32+16) | ((uint64_t) sa.i32[i])<<16) + bias;
//	u64.i = (((uint64_t) (sa.i8[i] & 0x80)) << 56 | ((uint64_t) (sa.i8[i] & 0x7F)) << (32+16) | ((uint64_t) sa.i32[i])<<16) | b_expfp64;
	u64.i = (((uint64_t) ((sa.i8[i] & 0x80) | 0x40)) << 56 | ((uint64_t) (sa.i8[i] & 0x7F)) << (32+16) | ((uint64_t) sa.i32[i])<<16);
	return u64.f;
}
// to RP
__inline__ void
FpToRpArray (double f64, rpre40barray sa, size_t i, uint64_t bias){
	union union64 u64;
	u64.f = f64; // |s(1)e(8)f(23)|
#ifdef RN
	// TODO
#endif
	uint8_t s = (uint8_t) (u64.i >> 63);
	uint8_t e = (uint8_t) (((u64.i >> 52) & 0x7FF) - bias); 
	uint8_t mh = (uint8_t) ((u64.i >> 48) & 0xF);
	sa.i8[i] = (s<<7) | (e<<4) | mh;
	sa.i32[i] = (uint32_t) ((u64.i >> 16) & 0xFFFFFFFF);
}

// -------------------------------
// RPRE48B
// 44 bits de mantisse
// -------------------------------
// to FP
__inline__ double
RpArrayToFp (rpre48barray sa, size_t i){
	union union64 u64;
//	u64.i = (((uint64_t) (sa.i16[i] & 0x8000)) <<48 | ((uint64_t) (sa.i16[i] & 0x7FFF)) <<(32+8) | ((uint64_t) sa.i32[i])<<8) + bias;
//	u64.i = (((uint64_t) (sa.i16[i] & 0x8000)) <<48 | ((uint64_t) (sa.i16[i] & 0x7FFF)) <<(32+8) | ((uint64_t) sa.i32[i])<<8) | b_expfp64;
	u64.i = (((uint64_t) ((sa.i16[i] & 0x8000) | 0x4000)) << 48 | ((uint64_t) (sa.i16[i] & 0x7FFF)) << (32+8) | ((uint64_t) sa.i32[i]) << 8);
	return u64.f;
}
// to RP
__inline__ void
FpToRpArray (double f64, rpre48barray sa, size_t i, uint64_t bias){
	union union64 u64;
	u64.f = f64; 
#ifdef RN
	// TODO
#endif
	uint16_t s = (uint16_t) (u64.i >> 63); // |0(63)s(1)|
	uint16_t e = (uint16_t) (((u64.i >> 52) & 0x7FF) - bias); // |0(13)e(3)|)
	uint16_t mh = (uint16_t) ((u64.i >> 40) & 0xFFF); // |0(4)mh(12)|
	sa.i16[i] = (s<<15) | (e<<12) | mh; // |s(1)e(3)mh(12)|
	sa.i32[i] = (uint32_t) ((u64.i >> 8) & 0xFFFFFFFF); // |ml(32)|
}


// -------------------------------
// RPRE56B
// 52 bits de mantisse
// -------------------------------
// to FP
__inline__ double
RpArrayToFp (rpre56barray sa, size_t i){
	union union64 u64;
	u64.i = ((uint64_t) ((sa.i8[i] & 0x80) | 0x40)) << 56 | (uint64_t (sa.i8[i] & 0x7F)) << (32+16) | ((uint64_t) sa.i16[i]) << 32 | ((uint64_t) sa.i32[i]);
	return u64.f;
}
// to RP
__inline__ void
FpToRpArray (double f64, rpre56barray sa, size_t i, uint64_t bias){
	union union64 u64;
	u64.f = f64; 
#ifdef RN
	// TODO
#endif
	uint8_t s = (uint8_t) (u64.i >> 63); // |0(63)s(1)|
	uint8_t e = (uint8_t) (((u64.i >> 52) & 0x7FF) - bias); // |0(13)e(3)|)
	uint8_t mhh = ((uint8_t) (u64.i >> 48) & 0xF);
	sa.i8[i] = (s<<7) | (e<<4) | mhh;
	sa.i16[i] = ((uint16_t) (u64.i >> 32) & 0xFFFF);
	sa.i32[i] = (uint32_t) (u64.i & 0xFFFFFFFF);
}
