module csr
    type intCSR
        ! possible de passer allocat en pointer mais à creuser
        integer*1, dimension(:), allocatable :: a
        integer*8, dimension(:), allocatable :: ia
        integer, dimension(:), allocatable :: ja
    end type intCSR     
    
    type realCSR
        real, dimension(:), allocatable :: a
        integer*8, dimension(:), allocatable :: ia
        integer, dimension(:), allocatable :: ja
    end type realCSR     

    type doubleCSR
        double precision, dimension(:), allocatable :: a
        integer*8, dimension(:), allocatable :: ia
        integer, dimension(:), allocatable :: ja
    end type doubleCSR     
    
    type intMultiCSR
        integer :: n, nbprec
        integer, dimension(:), allocatable :: availPrec
        type(intCSR), dimension(7) :: tab
    end type intMultiCSR
    
    type realDoubleCSR
        integer :: n
        type(realCSR) :: a_s
        type(doubleCSR) :: a_d
        integer, dimension(3) :: availPrec=(/ 0, 32-8, 64-11 /)
    end type realDoubleCSR
    
end module
