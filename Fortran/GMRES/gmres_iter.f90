

double precision function norminf(a,ia,n,nnz)
	integer :: n
	integer*8 :: nnz
	integer*8 :: ia(n+1)
	double precision :: a(nnz)
	! local variables
	integer :: i
	integer*8 :: k
	double precision :: somme
	norminf=0.d0
	do i=1,n
		somme=0.d0
		do k=ia(i),ia(i+1)-1
			somme = somme+abs(a(k))
		enddo
		norminf=max(norminf,somme)
	enddo
	return
end

module gmres_iter
	contains


! rhs : right-hand side
! itm_max : maximum de restart
! mr : maximum d'itération par par restart
! tol_abs : tolérance absolue
! tol_rel : tolérance relative
! tsolve : temps de résolution
subroutine mgmres_uniform(n, nnz, ia, ja, A_in, A_in_s, A_out, x, x_s, rhs, D, iter_in, iter_out, tol_in, tol_out, prec)
!subroutine mgmres_uniform (n, nnz, ia, ja, A_in, A_out, x, rhs, D, ax_fun, iter_in, iter_out, tol_in, tol_out)
!  MGMRES_UNIFORM applies restarted GMRES to an UNIFORM MATRIX
  use spmv
  use csr
  implicit none
  ! arguments
  integer, intent(in) :: n, iter_in, iter_out, ja(nnz)
  integer*8, intent(in) :: nnz, ia(n+1)
  double precision, intent(in) :: A_in(nnz), A_out(nnz), rhs(n), D(n)
  double precision, intent(in) :: tol_in, tol_out
  double precision, intent(inout) :: x(n)
  real, intent(inout) :: x_s(n)
  real, intent(in) :: A_in_s(nnz)
  character*5, intent(in) :: prec
  ! function
  double precision :: norminf
  ! local variables
  double precision :: t_spmv, t_gmres
  integer :: t1, t2, cr
  integer :: i, j, k, k_copy, itr, itr_used
  integer, parameter :: verbose = 1
  double precision, parameter :: delta = 1.0D-03
  double precision :: av, htmp, mu, rho
  double precision, allocatable, dimension(:) :: c, g, r, s, y
  double precision, allocatable, dimension(:,:) :: v, h
  ! RESVEC
  double precision :: resvec(iter_out)
  double precision :: init_rho, rho_tol
  ! BACKWARD ERROR
  double precision :: Ax(n)
  integer :: k1, itr_vec(iter_out+1)
  double precision :: norm_A_out, denom, bw

  norm_A_out = norminf(A_out,ia,n,nnz)
  itr_used = 0
  itr_vec = 0
  
  if ( n < iter_in ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'MGiter_inES_ST - Fatal error!'
    write ( *, '(a)' ) '  N < iter_in.'
    write ( *, '(a,i8)' ) '  N = ', n
    write ( *, '(a,i8)' ) '  iter_in = ', iter_in
    stop
  end if

  allocate(v(n,iter_in+1), c(iter_in), g(iter_in+1), h(iter_in+1,iter_in), r(n), s(iter_in), y(iter_in+1))
  t_spmv=0.d0; t_gmres=0.d0

  call system_clock(t1)
  
  do itr = 1, iter_out+1
  
    denom = 1.d0/(norm_A_out*maxval(abs(x))+maxval(abs(rhs)))

    call ax_fp64(n, nnz, ia, ja, A_out, x, r, t_spmv)
    r(1:n) = rhs(1:n) - r(1:n)
    resvec(itr)=maxval(abs(r))*denom
    if ( verbose .ge. 2 ) then
    	write(*,*) "Backward error ", itr, resvec(itr)
    end if
    if ( resvec(itr) <= tol_out .OR. itr>iter_out) then
      exit
    end if
    
    r = D*r
    init_rho = sqrt ( dot_product ( r(1:n), r(1:n) ) )
    rho=init_rho
    
    v(1:n,1) = r(1:n) / rho
    g(1) = rho
    g(2:iter_in+1) = 0.0D+00
    h(1:iter_in+1,1:iter_in) = 0.0D+00
    do k = 1, iter_in
      k_copy = k
      if(prec=="UFP64") then
      	call ax_fp64(n, nnz, ia, ja, A_in, v(1:n,k), v(1:n,k+1), t_spmv)
      elseIf(prec=="UFP32") then
      	call ax_fp32(n, nnz, ia, ja, A_in_s, real(v(1:n,k)), v(1:n,k+1), t_spmv)
      elseIF(prec=="UBF16") then
        call ax_bf16(n, nnz, ia, ja, A_in_s, real(v(1:n,k)), v(1:n,k+1), t_spmv)
      else
      	write(*,*) "Erreur !"
      	exit
      endif
      av = sqrt ( dot_product ( v(1:n,k+1), v(1:n,k+1) ) )
      do j = 1, k
        h(j,k) = dot_product ( v(1:n,k+1), v(1:n,j) )
        v(1:n,k+1) = v(1:n,k+1) - h(j,k) * v(1:n,j)
      end do
      h(k+1,k) = sqrt ( dot_product ( v(1:n,k+1), v(1:n,k+1) ) )
      if ( av + delta * h(k+1,k) == av ) then
        do j = 1, k
          htmp = dot_product ( v(1:n,k+1), v(1:n,j) )
          h(j,k) = h(j,k) + htmp
          v(1:n,k+1) = v(1:n,k+1) - htmp * v(1:n,j)
        end do
        h(k+1,k) = sqrt ( dot_product ( v(1:n,k+1), v(1:n,k+1) ) )
      end if
      if ( h(k+1,k) /= 0.0D+00 ) then
        v(1:n,k+1) = v(1:n,k+1) / h(k+1,k)
      end if
      if ( 1 < k ) then
        y(1:k+1) = h(1:k+1,k)
        do j = 1, k - 1
          call mult_givens ( c(j), s(j), j, y(1:k+1) )
        end do
        h(1:k+1,k) = y(1:k+1)
      end if
      mu = sqrt ( h(k,k)**2 + h(k+1,k)**2 )
      c(k) = h(k,k) / mu
      s(k) = -h(k+1,k) / mu
      h(k,k) = c(k) * h(k,k) - s(k) * h(k+1,k)
      h(k+1,k) = 0.0D+00
      call mult_givens ( c(k), s(k), k, g(1:k+1) )
      rho = abs ( g(k+1) )
      itr_used = itr_used + 1
      if ( verbose .ge. 2 ) then
        write ( *, '(a,i8,a,g14.6)' ) '  K =   ', k, '  Residual = ', rho / init_rho
      end if
      if ( rho / init_rho <= tol_in ) then
        exit
      end if
      if ( rho .NE. rho) then
      	exit
      end if
    end do
    
    itr_vec(itr+1)=itr_used
    k = k_copy - 1
    y(k+1) = g(k+1) / h(k+1,k+1)
    do i = k, 1, -1
      y(i) = ( g(i) - dot_product ( h(i,i+1:k+1), y(i+1:k+1) ) ) / h(i,i)
    end do
    do i = 1, n
      x(i) = x(i) + dot_product ( v(i,1:k+1), y(1:k+1) )
    end do
   
  end do

  call system_clock(t2,cr)
  t_gmres = dble(t2-t1)/dble(cr)

  if ( verbose .ge. 1 ) then
    write ( *, * ) ' Total iterations = ', itr_used
    write ( *, * ) ' Outside iterations = ', itr - 1
    write ( *, * ) ' tol_in ', tol_in, ', tol_out = ', tol_out
    write ( *, * ) ' Cumulated iterations', itr_vec(1:itr)
    write ( *, * ) ' Final backward error = ', resvec(itr)
    write ( *, * ) ' Backward error vector = ', resvec(1:itr)
    write ( *, * ) ' SpMV Time = ', t_spmv
    write ( *, * ) ' GMRES global time = ', t_gmres
  end if

  deallocate(v, c, g, h, r, s, y)

end

subroutine mgmres_adapt(n, nnz, ia, ja, A_in, A_out, x, rhs, D, iter_in, iter_out, tol_in, tol_out, ax_fun)
!subroutine mgmres_uniform (n, nnz, ia, ja, A_in, A_out, x, rhs, D, ax_fun, iter_in, iter_out, tol_in, tol_out)
!  MGMRES_UNIFORM applies restarted GMRES to an UNIFORM MATRIX
  use spmv
  use csr
  implicit none
  external ax_fun
  ! arguments
  integer, intent(in) :: n, iter_in, iter_out, ja(nnz)
  integer*8, intent(in) :: nnz, ia(n+1)
  double precision, intent(in) :: A_out(nnz), rhs(n), D(n)
  type(realDoubleCSR) :: A_in
  double precision, intent(in) :: tol_in, tol_out
  double precision, intent(inout) :: x(n)
  ! function
  double precision :: norminf
  ! local variables
  double precision :: t_spmv, t_gmres
  integer :: t1, t2, cr
  integer :: i, j, k, k_copy, itr, itr_used
  integer, parameter :: verbose = 1
  double precision, parameter :: delta = 1.0D-03
  double precision :: av, htmp, mu, rho
  double precision, allocatable, dimension(:) :: c, g, r, s, y
  double precision, allocatable, dimension(:,:) :: v, h
  ! RESVEC
  double precision :: resvec(iter_in*iter_out)
  double precision :: init_rho, rho_tol
  ! BACKWARD ERROR
  double precision :: Ax(n)
  integer :: k1, itr_vec(iter_out+1)
  double precision :: norm_A_out, denom, bw

  norm_A_out = norminf(A_out,ia,n,nnz)
  itr_used = 0
  itr_vec = 0
  
  if ( n < iter_in ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'MGiter_inES_ST - Fatal error!'
    write ( *, '(a)' ) '  N < iter_in.'
    write ( *, '(a,i8)' ) '  N = ', n
    write ( *, '(a,i8)' ) '  iter_in = ', iter_in
    stop
  end if

  allocate(v(n,iter_in+1), c(iter_in), g(iter_in+1), h(iter_in+1,iter_in), r(n), s(iter_in), y(iter_in+1))
  t_spmv=0.d0; t_gmres=0.d0

  call system_clock(t1)
  
  do itr = 1, iter_out+1
  
    denom = 1.d0/(norm_A_out*maxval(abs(x))+maxval(abs(rhs)))

    call ax_fp64(n, nnz, ia, ja, A_out, x, r, t_spmv)
    r(1:n) = rhs(1:n) - r(1:n)
    resvec(itr)=maxval(abs(r))*denom
    if ( verbose .ge. 2 ) then
    	write(*,*) "Backward error ", itr, resvec(itr)
    end if
    if ( resvec(itr) <= tol_out .OR. itr>iter_out) then
      exit
    end if
    
    r = D*r
    init_rho = sqrt ( dot_product ( r(1:n), r(1:n) ) )
    rho=init_rho
    
    v(1:n,1) = r(1:n) / rho
    g(1) = rho
    g(2:iter_in+1) = 0.0D+00
    h(1:iter_in+1,1:iter_in) = 0.0D+00
    do k = 1, iter_in
      k_copy = k
      call ax_fun(n, nnz, A_in, v(1:n,k), v(1:n,k+1), t_spmv)
      av = sqrt ( dot_product ( v(1:n,k+1), v(1:n,k+1) ) )
      do j = 1, k
        h(j,k) = dot_product ( v(1:n,k+1), v(1:n,j) )
        v(1:n,k+1) = v(1:n,k+1) - h(j,k) * v(1:n,j)
      end do
      h(k+1,k) = sqrt ( dot_product ( v(1:n,k+1), v(1:n,k+1) ) )
      if ( av + delta * h(k+1,k) == av ) then
        do j = 1, k
          htmp = dot_product ( v(1:n,k+1), v(1:n,j) )
          h(j,k) = h(j,k) + htmp
          v(1:n,k+1) = v(1:n,k+1) - htmp * v(1:n,j)
        end do
        h(k+1,k) = sqrt ( dot_product ( v(1:n,k+1), v(1:n,k+1) ) )
      end if
      if ( h(k+1,k) /= 0.0D+00 ) then
        v(1:n,k+1) = v(1:n,k+1) / h(k+1,k)
      end if
      if ( 1 < k ) then
        y(1:k+1) = h(1:k+1,k)
        do j = 1, k - 1
          call mult_givens ( c(j), s(j), j, y(1:k+1) )
        end do
        h(1:k+1,k) = y(1:k+1)
      end if
      mu = sqrt ( h(k,k)**2 + h(k+1,k)**2 )
      c(k) = h(k,k) / mu
      s(k) = -h(k+1,k) / mu
      h(k,k) = c(k) * h(k,k) - s(k) * h(k+1,k)
      h(k+1,k) = 0.0D+00
      call mult_givens ( c(k), s(k), k, g(1:k+1) )
      rho = abs ( g(k+1) )
      itr_used = itr_used + 1
      if ( verbose .ge. 2 ) then
        write ( *, '(a,i8,a,g14.6)' ) '  K =   ', k, '  Residual = ', rho / init_rho
      end if
      if ( rho / init_rho <= tol_in ) then
        exit
      end if
    end do
    itr_vec(itr+1)=itr_used
    k = k_copy - 1
    y(k+1) = g(k+1) / h(k+1,k+1)
    do i = k, 1, -1
      y(i) = ( g(i) - dot_product ( h(i,i+1:k+1), y(i+1:k+1) ) ) / h(i,i)
    end do
    do i = 1, n
      x(i) = x(i) + dot_product ( v(i,1:k+1), y(1:k+1) )
    end do
   
  end do

  call system_clock(t2,cr)
  t_gmres = dble(t2-t1)/dble(cr)

  if ( verbose .ge. 1 ) then
    write ( *, * ) ' Total iterations = ', itr_used
    write ( *, * ) ' Outside iterations = ', itr - 1
    write ( *, * ) ' tol_in ', tol_in, ', tol_out = ', tol_out
    write ( *, * ) ' Cumulated iterations', itr_vec(1:itr)
    write ( *, * ) ' Final backward error = ', resvec(itr)
    write ( *, * ) ' Backward error vector = ', resvec(1:itr)
    write ( *, * ) ' SpMV Time = ', t_spmv
    write ( *, * ) ' GMRES global time = ', t_gmres
  end if

  deallocate(v, c, g, h, r, s, y)

end

subroutine mgmres_adapt_XPREC(n, nnz, A_in, A_out, x, rhs, D, iter_in, iter_out, tol_in, tol_out, norm_A_out)
  use spmv
  use csr
  implicit none
  ! arguments
  integer, intent(in) :: n, iter_in, iter_out
  integer*8, intent(in) :: nnz
  double precision, intent(in) :: rhs(n), D(n)
  type(intMultiCSR), intent(in) :: A_in, A_out
  double precision, intent(in) :: tol_in, tol_out
  double precision, intent(inout) :: x(n)
  double precision, intent(in) :: norm_A_out
  ! function
  double precision :: norminf
  ! local variables
  double precision :: t_spmv, t_gmres
  integer :: t1, t2, cr
  integer :: i, j, k, k_copy, itr, itr_used
  integer, parameter :: verbose = 1
  double precision, parameter :: delta = 1.0D-03
  double precision :: av, htmp, mu, rho
  double precision, allocatable, dimension(:) :: c, g, r, s, y
  double precision, allocatable, dimension(:,:) :: v, h
  ! RESVEC
  double precision :: resvec(iter_in*iter_out)
  double precision :: init_rho, rho_tol
  ! BACKWARD ERROR
  double precision :: Ax(n)
  integer :: k1, itr_vec(iter_out+1)
  double precision :: denom, bw

  itr_used = 0
  itr_vec = 0
  if ( n < iter_in ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'MGiter_inES_ST - Fatal error!'
    write ( *, '(a)' ) '  N < iter_in.'
    write ( *, '(a,i8)' ) '  N = ', n
    write ( *, '(a,i8)' ) '  iter_in = ', iter_in
    stop
  end if
  allocate(v(n,iter_in+1), c(iter_in), g(iter_in+1), h(iter_in+1,iter_in), r(n), s(iter_in), y(iter_in+1))
  t_spmv=0.d0; t_gmres=0.d0
  call system_clock(t1)
  do itr = 1, iter_out+1
    denom = 1.d0/(norm_A_out*maxval(abs(x))+maxval(abs(rhs)))
    call ax_adapt_7p(n, nnz, A_out, x, r, t_spmv)
    r(1:n) = rhs(1:n) - r(1:n)
    resvec(itr)=maxval(abs(r))*denom
    if ( verbose .ge. 2 ) then
    	write(*,*) "Backward error ", itr, resvec(itr)
    end if
    if ( resvec(itr) <= tol_out .OR. itr>iter_out) then
      exit
    end if
    r = D*r
    init_rho = sqrt ( dot_product ( r(1:n), r(1:n) ) )
    rho=init_rho
    v(1:n,1) = r(1:n) / rho
    g(1) = rho
    g(2:iter_in+1) = 0.0D+00
    h(1:iter_in+1,1:iter_in) = 0.0D+00
    do k = 1, iter_in
      k_copy = k
      call ax_adapt_7p(n, nnz, A_in, v(1:n,k), v(1:n,k+1), t_spmv)
      av = sqrt ( dot_product ( v(1:n,k+1), v(1:n,k+1) ) )
      do j = 1, k
        h(j,k) = dot_product ( v(1:n,k+1), v(1:n,j) )
        v(1:n,k+1) = v(1:n,k+1) - h(j,k) * v(1:n,j)
      end do
      h(k+1,k) = sqrt ( dot_product ( v(1:n,k+1), v(1:n,k+1) ) )
      if ( av + delta * h(k+1,k) == av ) then
        do j = 1, k
          htmp = dot_product ( v(1:n,k+1), v(1:n,j) )
          h(j,k) = h(j,k) + htmp
          v(1:n,k+1) = v(1:n,k+1) - htmp * v(1:n,j)
        end do
        h(k+1,k) = sqrt ( dot_product ( v(1:n,k+1), v(1:n,k+1) ) )
      end if
      if ( h(k+1,k) /= 0.0D+00 ) then
        v(1:n,k+1) = v(1:n,k+1) / h(k+1,k)
      end if
      if ( 1 < k ) then
        y(1:k+1) = h(1:k+1,k)
        do j = 1, k - 1
          call mult_givens ( c(j), s(j), j, y(1:k+1) )
        end do
        h(1:k+1,k) = y(1:k+1)
      end if
      mu = sqrt ( h(k,k)**2 + h(k+1,k)**2 )
      c(k) = h(k,k) / mu
      s(k) = -h(k+1,k) / mu
      h(k,k) = c(k) * h(k,k) - s(k) * h(k+1,k)
      h(k+1,k) = 0.0D+00
      call mult_givens ( c(k), s(k), k, g(1:k+1) )
      rho = abs ( g(k+1) )
      itr_used = itr_used + 1
      if ( verbose .ge. 2 ) then
        write ( *, '(a,i8,a,g14.6)' ) '  K =   ', k, '  Residual = ', rho / init_rho
      end if
      if ( rho / init_rho <= tol_in ) then
        exit
      end if
      if ( rho .NE. rho) then
      	exit
      end if
    end do
      if ( rho .NE. rho) then
      	exit
      end if
    
    itr_vec(itr+1)=itr_used
    k = k_copy - 1
    y(k+1) = g(k+1) / h(k+1,k+1)
    do i = k, 1, -1
      y(i) = ( g(i) - dot_product ( h(i,i+1:k+1), y(i+1:k+1) ) ) / h(i,i)
    end do
    do i = 1, n
      x(i) = x(i) + dot_product ( v(i,1:k+1), y(1:k+1) )
    end do
  end do
  call system_clock(t2,cr)
  t_gmres = dble(t2-t1)/dble(cr)
  if ( verbose .ge. 1 ) then
    write ( *, * ) ' Total iterations = ', itr_used
    write ( *, * ) ' Outside iterations = ', itr - 1
    write ( *, * ) ' tol_in ', tol_in, ', tol_out = ', tol_out
    write ( *, * ) ' Cumulated iterations', itr_vec(1:itr)
    write ( *, * ) ' Final backward error = ', resvec(itr)
    write ( *, * ) ' Backward error vector = ', resvec(1:itr)
    write ( *, * ) ' SpMV Time = ', t_spmv
    write ( *, * ) ' GMRES global time = ', t_gmres
  end if
  deallocate(v, c, g, h, r, s, y)
end

subroutine mgmres_adapt_B(n, nnz, A_in, A_out, x, rhs, D, iter_in, iter_out, tol_in, tol_out, ax_fun, norm_A_out)
!  MGMRES_ADAPT_X2 applies iterative GMRES using ADAPTFP64 MATRIX in outside iterations and ADAPTFP32 MATRIX for inside iterations
  use spmv
  use csr
  implicit none
  external ax_fun
  ! arguments
  integer, intent(in) :: n, iter_in, iter_out
  integer*8, intent(in) :: nnz
  double precision, intent(in) :: rhs(n), D(n)
  type(realDoubleCSR) :: A_in, A_out
  double precision, intent(in) :: tol_in, tol_out
  double precision, intent(inout) :: x(n)
  double precision, intent(in) :: norm_A_out
  ! function
  double precision :: norminf
  ! local variables
  double precision :: t_spmv, t_gmres
  integer :: t1, t2, cr
  integer :: i, j, k, k_copy, itr, itr_used
  integer, parameter :: verbose = 1
  double precision, parameter :: delta = 1.0D-03
  double precision :: av, htmp, mu, rho
  double precision, allocatable, dimension(:) :: c, g, r, s, y
  double precision, allocatable, dimension(:,:) :: v, h
  ! RESVEC
  double precision :: resvec(iter_in*iter_out)
  double precision :: init_rho, rho_tol
  ! BACKWARD ERROR
  double precision :: Ax(n)
  integer :: k1, itr_vec(iter_out+1)
  double precision :: denom, bw

  itr_used = 0
  itr_vec = 0
  if ( n < iter_in ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'MGiter_inES_ST - Fatal error!'
    write ( *, '(a)' ) '  N < iter_in.'
    write ( *, '(a,i8)' ) '  N = ', n
    write ( *, '(a,i8)' ) '  iter_in = ', iter_in
    stop
  end if
  allocate(v(n,iter_in+1), c(iter_in), g(iter_in+1), h(iter_in+1,iter_in), r(n), s(iter_in), y(iter_in+1))
  t_spmv=0.d0; t_gmres=0.d0
  call system_clock(t1)
  do itr = 1, iter_out+1
    denom = 1.d0/(norm_A_out*maxval(abs(x))+maxval(abs(rhs)))
    !call ax_fp64(n, nnz, ia, ja, A_out, x, r, t_spmv)
    call ax_adapt_rd(n, nnz, A_out, x, r, t_spmv)
    r(1:n) = rhs(1:n) - r(1:n)
    resvec(itr)=maxval(abs(r))*denom
    if ( verbose .ge. 2 ) then
    	write(*,*) "Backward error ", itr, resvec(itr)
    end if
    if ( resvec(itr) <= tol_out .OR. itr>iter_out) then
      exit
    end if
    r = D*r
    init_rho = sqrt ( dot_product ( r(1:n), r(1:n) ) )
    rho=init_rho
    v(1:n,1) = r(1:n) / rho
    g(1) = rho
    g(2:iter_in+1) = 0.0D+00
    h(1:iter_in+1,1:iter_in) = 0.0D+00
    do k = 1, iter_in
      k_copy = k
      call ax_fun(n, nnz, A_in, v(1:n,k), v(1:n,k+1), t_spmv)
      av = sqrt ( dot_product ( v(1:n,k+1), v(1:n,k+1) ) )
      do j = 1, k
        h(j,k) = dot_product ( v(1:n,k+1), v(1:n,j) )
        v(1:n,k+1) = v(1:n,k+1) - h(j,k) * v(1:n,j)
      end do
      h(k+1,k) = sqrt ( dot_product ( v(1:n,k+1), v(1:n,k+1) ) )
      if ( av + delta * h(k+1,k) == av ) then
        do j = 1, k
          htmp = dot_product ( v(1:n,k+1), v(1:n,j) )
          h(j,k) = h(j,k) + htmp
          v(1:n,k+1) = v(1:n,k+1) - htmp * v(1:n,j)
        end do
        h(k+1,k) = sqrt ( dot_product ( v(1:n,k+1), v(1:n,k+1) ) )
      end if
      if ( h(k+1,k) /= 0.0D+00 ) then
        v(1:n,k+1) = v(1:n,k+1) / h(k+1,k)
      end if
      if ( 1 < k ) then
        y(1:k+1) = h(1:k+1,k)
        do j = 1, k - 1
          call mult_givens ( c(j), s(j), j, y(1:k+1) )
        end do
        h(1:k+1,k) = y(1:k+1)
      end if
      mu = sqrt ( h(k,k)**2 + h(k+1,k)**2 )
      c(k) = h(k,k) / mu
      s(k) = -h(k+1,k) / mu
      h(k,k) = c(k) * h(k,k) - s(k) * h(k+1,k)
      h(k+1,k) = 0.0D+00
      call mult_givens ( c(k), s(k), k, g(1:k+1) )
      rho = abs ( g(k+1) )
      itr_used = itr_used + 1
      if ( verbose .ge. 2 ) then
        write ( *, '(a,i8,a,g14.6)' ) '  K =   ', k, '  Residual = ', rho / init_rho
      end if
      if ( rho / init_rho <= tol_in ) then
        exit
      end if
    end do
    itr_vec(itr+1)=itr_used
    k = k_copy - 1
    y(k+1) = g(k+1) / h(k+1,k+1)
    do i = k, 1, -1
      y(i) = ( g(i) - dot_product ( h(i,i+1:k+1), y(i+1:k+1) ) ) / h(i,i)
    end do
    do i = 1, n
      x(i) = x(i) + dot_product ( v(i,1:k+1), y(1:k+1) )
    end do
  end do
  call system_clock(t2,cr)
  t_gmres = dble(t2-t1)/dble(cr)
  if ( verbose .ge. 1 ) then
    write ( *, * ) ' Total iterations = ', itr_used
    write ( *, * ) ' Outside iterations = ', itr - 1
    write ( *, * ) ' tol_in ', tol_in, ', tol_out = ', tol_out
    write ( *, * ) ' Cumulated iterations', itr_vec(1:itr)
    write ( *, * ) ' Final backward error = ', resvec(itr)
    write ( *, * ) ' Backward error vector = ', resvec(1:itr)
    write ( *, * ) ' SpMV Time = ', t_spmv
    write ( *, * ) ' GMRES global time = ', t_gmres
  end if
  deallocate(v, c, g, h, r, s, y)
end

subroutine mult_givens (c, s, k, g)
! MULT_GIVENS applies a Givens rotation to two successive entries of a vector.
  implicit none
  ! arguments
  integer, intent(in) :: k
  real (kind(1.d0)), intent(in) :: c, s
  real (kind(1.d0)), intent(inout) :: g(k+1)
  ! local variables
  real (kind(1.d0)) :: g1, g2

  g1 = c * g(k) - s * g(k+1)
  g2 = s * g(k) + c * g(k+1)

  g(k)   = g1
  g(k+1) = g2

  return
end

end module gmres_iter
