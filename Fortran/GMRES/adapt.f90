       !!!!!!!!!!!!!!!!!!!
        !!!! REDUCTION !!!!
        !!!!!!!!!!!!!!!!!!!

        SUBROUTINE TRUNCATE_16(A)
        !      31 30 .... 16 15 14  ... 6 5 4 3 2 1 0
        !DEC$ ATTRIBUTES NO_ARG_CHECK ::A
        !      =============
       INTEGER*4 A
       INTEGER*4 TMP
       TMP = 0
       CALL MVBITS(TMP, 0, 8, A, 0)
       CALL MVBITS(TMP, 0, 8, A, 8)
       END SUBROUTINE TRUNCATE_16 

        SUBROUTINE CONVERT_32_16(A, B)
        !      31 30 .... 16 15 14  ... 6 5 4 3 2 1 0
        !DEC$ ATTRIBUTES NO_ARG_CHECK ::A
        !      =============
       INTEGER*4 A
       INTEGER*4 TMP
       INTEGER*1 B(2) 
       TMP = 0
       CALL MVBITS(A, 16, 8, TMP, 0)
       B(1)=TMP
       CALL MVBITS(A, 24, 8, TMP, 0)
       B(2)=TMP
       END SUBROUTINE CONVERT_32_16 

       SUBROUTINE CONVERT_32_24(A, B)
!      31 30 .... 16 15 14  ... 6 5 4 3 2 1 0
        !DEC$ ATTRIBUTES NO_ARG_CHECK ::A
!      =============
       INTEGER*4 A
       INTEGER*4 TMP
       INTEGER*1 B(3) 
       TMP = 0
       CALL MVBITS(A, 8, 8, TMP, 0)
       B(1)=TMP
       CALL MVBITS(A, 16, 8, TMP, 0)
       B(2)=TMP
       CALL MVBITS(A, 24, 8, TMP, 0)
       B(3)=TMP
       END SUBROUTINE CONVERT_32_24
       
       SUBROUTINE CONVERT_32I_32F(A, B)
       !DEC$ ATTRIBUTES NO_ARG_CHECK ::A, B
       INTEGER*4 A
       INTEGER*4 B 
       B = A
       END SUBROUTINE CONVERT_32I_32F
       
        SUBROUTINE CONVERT_64_40(A, B)
       !DEC$ ATTRIBUTES NO_ARG_CHECK ::A
!      =============
       INTEGER*8 A
       INTEGER*8 TMP
       INTEGER*1 B(5) 
       TMP = 0
       CALL MVBITS(A, 24, 8, TMP, 0)
       B(1)=TMP
       CALL MVBITS(A, 32, 8, TMP, 0)
       B(2)=TMP
       CALL MVBITS(A, 40, 8, TMP, 0)
       B(3)=TMP
       CALL MVBITS(A, 48, 8, TMP, 0)
       B(4)=TMP
       CALL MVBITS(A, 56, 8, TMP, 0)
       B(5)=TMP
       END SUBROUTINE CONVERT_64_40
       
       SUBROUTINE CONVERT_64_48(A, B)
       !DEC$ ATTRIBUTES NO_ARG_CHECK ::A
       !convertis A(dble) en B(int*1)
!      =============
       INTEGER*8 A
       INTEGER*8 TMP
       INTEGER*1 B(6) 
       TMP = 0
       CALL MVBITS(A, 16, 8, TMP, 0)
       B(1)=TMP       
       CALL MVBITS(A, 24, 8, TMP, 0)
       B(2)=TMP
       CALL MVBITS(A, 32, 8, TMP, 0)
       B(3)=TMP
       CALL MVBITS(A, 40, 8, TMP, 0)
       B(4)=TMP
       CALL MVBITS(A, 48, 8, TMP, 0)
       B(5)=TMP
       CALL MVBITS(A, 56, 8, TMP, 0)
       B(6)=TMP
       END SUBROUTINE CONVERT_64_48
       
       SUBROUTINE CONVERT_64_56(A, B)
       !DEC$ ATTRIBUTES NO_ARG_CHECK ::A
!      =============
       INTEGER*8 A
       INTEGER*8 TMP
       INTEGER*1 B(7) 
       TMP = 0
       CALL MVBITS(A, 8, 8, TMP, 0)
       B(1)=TMP 
       CALL MVBITS(A, 16, 8, TMP, 0)
       B(2)=TMP       
       CALL MVBITS(A, 24, 8, TMP, 0)
       B(3)=TMP
       CALL MVBITS(A, 32, 8, TMP, 0)
       B(4)=TMP
       CALL MVBITS(A, 40, 8, TMP, 0)
       B(5)=TMP
       CALL MVBITS(A, 48, 8, TMP, 0)
       B(6)=TMP
       CALL MVBITS(A, 56, 8, TMP, 0)
       B(7)=TMP
       END SUBROUTINE CONVERT_64_56
    
       SUBROUTINE CONVERT_64I_64F(A, B)
       !DEC$ ATTRIBUTES NO_ARG_CHECK ::A, B
       INTEGER*8 A
       INTEGER*8 B
       B = A
       END SUBROUTINE CONVERT_64I_64F
       
       
       !!!!!!!!!!!!!!!!!!!!!!!!!
       !!!! AUGMENTATION !!!!!!!
       !!!!!!!!!!!!!!!!!!!!!!!!!
       
       SUBROUTINE CONVERT_16_32(A, B)
       !DEC$ ATTRIBUTES NO_ARG_CHECK ::A
       INTEGER*4 A
       INTEGER*4 TMP
       INTEGER*1 B(3) 
       A = 0
       TMP=B(1)
       CALL MVBITS(TMP, 0, 8, A, 16)
       TMP=B(2)
       CALL MVBITS(TMP, 0, 8, A, 24)
       END SUBROUTINE CONVERT_16_32
       
       SUBROUTINE CONVERT_24_32(A, B)
       !DEC$ ATTRIBUTES NO_ARG_CHECK ::A
       INTEGER*4 A
       INTEGER*4 TMP
       INTEGER*1 B(3) 
       A = 0
       TMP=B(1)
       CALL MVBITS(TMP, 0, 8, A, 8)
       TMP=B(2)
       CALL MVBITS(TMP, 0, 8, A, 16)
       TMP=B(3)
       CALL MVBITS(TMP, 0, 8, A, 24)
       END SUBROUTINE CONVERT_24_32
       
       SUBROUTINE CONVERT_32F_32I(A, B)
       !DEC$ ATTRIBUTES NO_ARG_CHECK ::A, B
       INTEGER*4 A
       INTEGER*4 B 
       A = B
       END SUBROUTINE CONVERT_32F_32I

       SUBROUTINE CONVERT_40_64(A, B)
       !DEC$ ATTRIBUTES NO_ARG_CHECK ::A
       INTEGER*8 A
       INTEGER*8 TMP
       INTEGER*1 B(5) 
       A =0
       TMP=B(1)
       CALL MVBITS(TMP, 0, 8, A, 24)
       TMP=B(2)
       CALL MVBITS(TMP, 0, 8, A, 32)
       TMP=B(3)
       CALL MVBITS(TMP, 0, 8, A, 40)
       TMP=B(4)
       CALL MVBITS(TMP, 0, 8, A, 48)
       TMP=B(5)
       CALL MVBITS(TMP, 0, 8, A, 56)
       END SUBROUTINE CONVERT_40_64

       SUBROUTINE CONVERT_48_64(A, B)
       !DEC$ ATTRIBUTES NO_ARG_CHECK ::A
       ! convertis B(int*1) en A(double)
       INTEGER*8 A
       INTEGER*8 TMP
       INTEGER*1 B(6) 
       A =0
       TMP=B(1)
       CALL MVBITS(TMP, 0, 8, A, 16)
       TMP=B(2)
       CALL MVBITS(TMP, 0, 8, A, 24)
       TMP=B(3)
       CALL MVBITS(TMP, 0, 8, A, 32)
       TMP=B(4)
       CALL MVBITS(TMP, 0, 8, A, 40)
       TMP=B(5)
       CALL MVBITS(TMP, 0, 8, A, 48)
       TMP=B(6)
       CALL MVBITS(TMP, 0, 8, A, 56)
       END SUBROUTINE CONVERT_48_64

       SUBROUTINE CONVERT_56_64(A, B)
       !DEC$ ATTRIBUTES NO_ARG_CHECK ::A
       INTEGER*8 A
       INTEGER*8 TMP
       INTEGER*1 B(7) 
       A =0
       TMP=B(1)
       CALL MVBITS(TMP, 0, 8, A, 8)
       TMP=B(2)
       CALL MVBITS(TMP, 0, 8, A, 16)
       TMP=B(3)
       CALL MVBITS(TMP, 0, 8, A, 24)
       TMP=B(4)
       CALL MVBITS(TMP, 0, 8, A, 32)
       TMP=B(5)
       CALL MVBITS(TMP, 0, 8, A, 40)
       TMP=B(6)
       CALL MVBITS(TMP, 0, 8, A, 48)
       TMP=B(7)
       CALL MVBITS(TMP, 0, 8, A, 56)
       END SUBROUTINE CONVERT_56_64
       
       SUBROUTINE CONVERT_64F_64I(A, B)
       !DEC$ ATTRIBUTES NO_ARG_CHECK ::A, B
       INTEGER*8 A
       INTEGER*8 B
       A = B
       END SUBROUTINE CONVERT_64F_64I
       
       SUBROUTINE READ_48(A, B)
       !DEC$ ATTRIBUTES NO_ARG_CHECK ::A
       INTEGER*8 A
       INTEGER*8 TMP
       INTEGER*1 B(8) 
       A =0
       TMP=B(3)
       CALL MVBITS(TMP, 0, 8, A, 16)
       TMP=B(4)
       CALL MVBITS(TMP, 0, 8, A, 24)
       TMP=B(5)
       CALL MVBITS(TMP, 0, 8, A, 32)
       TMP=B(6)
       CALL MVBITS(TMP, 0, 8, A, 40)
       TMP=B(7)
       CALL MVBITS(TMP, 0, 8, A, 48)
       TMP=B(8)
       CALL MVBITS(TMP, 0, 8, A, 56)
       END SUBROUTINE READ_48
       
module adapt
    use csr
    contains
        subroutine init_intCSR(x)
            type(intCSR) :: x
            x%ia = 1
            x%ja = 0
            x%a = 0
        end subroutine
      
        subroutine init_realCSR(x)
            type(realCSR) :: x
            x%ia = 1
            x%ja = 0
            x%a = 0.0
        end subroutine

        subroutine init_doubleCSR(x)
            type(doubleCSR) :: x
            x%ia = 1
            x%ja = 0
            x%a = 0.d0
        end subroutine
            
        subroutine init_A_Xprec(A_Xprec)
          type(intMultiCSR) :: A_Xprec
          if(A_Xprec%nbprec == 2) then !RD : deux precisions fp32, fp64
              do k=3,7,4
                  call init_intCSR(A_Xprec%tab(k))
              enddo 
          elseIf(A_Xprec%nbprec == 3) then ! 3 précisions : fp64, FP32, bf16
              call init_intCSR(A_Xprec%tab(1))
              call init_intCSR(A_Xprec%tab(3))
              call init_intCSR(A_Xprec%tab(7))
          elseIf(A_Xprec%nbprec == 7) then ! 7 précisions : multiples de 8 bits
              do k=1,7
              	call init_intCSR(A_Xprec%tab(k))
              enddo 
          endif
        end subroutine
        
        subroutine init_A_real_dble(A_rd)
        	type(realDoubleCSR) :: A_rd
        	call init_realCSR(A_rd%a_s)
        	call init_doubleCSR(A_rd%a_d)
        end subroutine
        	
        subroutine adapt_XPREC(epsi, i, jcnk, Ak, A_Xprec, nbread,DL)
		      !parameter
		      integer, intent(in) :: epsi, i, jcnk
		      double precision, intent(in) :: Ak
		      integer, intent(in) :: DL
		      !output
		      type(intMultiCSR), intent(inout) :: A_Xprec
					integer*8, intent(inout) :: nbread
		      !local variables
		      integer :: ind
		      integer*8 :: kX

					if (epsi<=24) then
						ind=int((epsi+8)/8)-1
					else
						ind=int((epsi+11)/8)-1
					endif
		      if(ind<1) then
		      	if(DL==1) then ! Si le DropLess est activé, on stocke les éléments les plus faibles en bf16
		      		ind=1
							kX = A_Xprec%tab(ind)%ia(i+1)
							A_Xprec%tab(ind)%ia(i+1)= A_Xprec%tab(ind)%ia(i+1) + 1
							A_Xprec%tab(ind)%ja(kX) = jcnk
							call convert_32_16(real(Ak), A_Xprec%tab(ind)%a((ind+1)*kX-ind))
							nbread = nbread+16
						endif
		        return
          endif

				  kX = A_Xprec%tab(ind)%ia(i+1)
				  A_Xprec%tab(ind)%ia(i+1)= A_Xprec%tab(ind)%ia(i+1) + 1
				  A_Xprec%tab(ind)%ja(kX) = jcnk
				    
          if (ind <= 3) then
          	nbread = nbread+epsi+8
          elseif (ind <8) then
          	nbread = nbread+epsi+11
          else
          	write(*,*) "Error"
          endif
            
          if(ind==1) then
              call convert_32_16(real(Ak), A_Xprec%tab(ind)%a((ind+1)*kX-ind))
		elseif(ind==2) then
              call convert_32_24(real(Ak), A_Xprec%tab(ind)%a((ind+1)*kX-ind))
		elseif(ind==3) then
              call convert_32I_32F(real(Ak), A_Xprec%tab(ind)%a((ind+1)*kX-ind))
		elseif(ind==4) then
              call convert_64_40(Ak, A_Xprec%tab(ind)%a((ind+1)*kX-ind))
		elseif(ind==5) then
              call convert_64_48(Ak, A_Xprec%tab(ind)%a((ind+1)*kX-ind))
		elseif(ind==6) then
              call convert_64_56(Ak, A_Xprec%tab(ind)%a((ind+1)*kX-ind))
		elseif(ind==7) then
              call convert_64I_64F(Ak, A_Xprec%tab(ind)%a((ind+1)*kX-ind))
		else
			write(*,*) "Error"
          endif
            
        end subroutine
        
        !Pour A_real_dble
        subroutine adapt_rd(epsi, i, jcnk, Ak, A_adapt, nbread)
            !parameter
            integer, intent(in) :: epsi, i, jcnk
            double precision, intent(in) :: Ak
            !output
            type(realDoubleCSR) :: A_adapt
            integer*8, intent(inout) :: nbread
            !local variables
            integer*8 :: kX

			if(epsi == 32-8) then
				kX = A_adapt%a_s%ia(i+1)
				A_adapt%a_s%ia(i+1) = A_adapt%a_s%ia(i+1) +1
				A_adapt%a_s%ja(kX) = jcnk
				A_adapt%a_s%a(kX) = real(Ak)
				nbread = nbread+32
			elseIf(epsi == 64-11) then
				kX = A_adapt%a_d%ia(i+1)
				A_adapt%a_d%ia(i+1) = A_adapt%a_d%ia(i+1) +1
				A_adapt%a_d%ja(kX) = jcnk
				A_adapt%a_d%a(kX) = Ak
				nbread = nbread+64
			endIf
        end subroutine
        
	    !Pour A_real_dble sans dropping
	    subroutine adapt_rd_DL(epsi, i, jcnk, Ak, A_adapt, nbread)
	        !parameter
	        integer, intent(in) :: epsi, i, jcnk
	        double precision, intent(in) :: Ak
	        !output
	        type(realDoubleCSR), intent(inout) :: A_adapt
	        integer*8, intent(inout) :: nbread
	        !local variables
	        integer :: ind, kX

			if(epsi <= 32-8) then
				kX = A_adapt%a_s%ia(i+1)
				A_adapt%a_s%ia(i+1) = A_adapt%a_s%ia(i+1) +1
				A_adapt%a_s%ja(kX) = jcnk
				A_adapt%a_s%a(kX) = real(Ak)
				nbread = nbread+32
			elseIf(epsi == 64-11) then
				kX = A_adapt%a_d%ia(i+1)
				A_adapt%a_d%ia(i+1) = A_adapt%a_d%ia(i+1) +1
				A_adapt%a_d%ja(kX) = jcnk
				A_adapt%a_d%a(kX) = Ak
				nbread = nbread+64
			endIf
    	end subroutine
        
        subroutine intern_loop(A_Xprec, i)
            type(intMultiCSR), intent(inout) :: A_Xprec
            integer, intent(in) :: i
            if(A_Xprec%nbprec == 2) then 
                A_Xprec%tab(3)%ia(i+1) = A_Xprec%tab(3)%ia(i)
                A_Xprec%tab(7)%ia(i+1) = A_Xprec%tab(7)%ia(i)
            elseIf(A_Xprec%nbprec == 3) then
                A_Xprec%tab(1)%ia(i+1) = A_Xprec%tab(1)%ia(i)
                A_Xprec%tab(3)%ia(i+1) = A_Xprec%tab(3)%ia(i)
                A_Xprec%tab(7)%ia(i+1) = A_Xprec%tab(7)%ia(i)
            elseIf(A_Xprec%nbprec == 4) then
                do k=1,7,2
                    A_Xprec%tab(k)%ia(i+1) = A_Xprec%tab(k)%ia(i)
                enddo
            elseIf(A_Xprec%nbprec == 7) then
                do k=1,7
                    A_Xprec%tab(k)%ia(i+1) = A_Xprec%tab(k)%ia(i)
                enddo
            endif
        endsubroutine
        
        double precision function critCW(absAabsxi,normA) 
        	double precision :: absAabsxi, normA
        	critCW=absAabsxi
        	return
        end
        
        double precision function critNW(absAabsxi,normA) 
        	double precision :: absAabsxi, normA
        	critNW=normA
        	return
        end

        subroutine adapt_wrap(A_Xprec, nbread, irn, nnz, A, jcn, absAabsx, normA, targetPrec, crit, DL)
          type(intMultiCSR), intent(inout) :: A_Xprec
          integer*8, intent(out) :: nbread
          integer*8, dimension(A_Xprec%n+1), intent(in) :: irn
          integer*8, intent(in) :: nnz
          double precision, dimension(nnz), intent(in) :: A
          integer, dimension(nnz), intent(in) :: jcn
          double precision, dimension(A_Xprec%n+1), intent(in) :: absAabsx
          integer, intent(in) :: targetPrec
          double precision, intent(in) :: normA
          double precision :: crit
          integer, intent(in) :: DL
          !local variable
          integer :: i, epsi
          
          nbread=0
		    	do i=1,A_Xprec%n
		        call intern_loop(A_Xprec, i)
		        do k=irn(i),irn(i+1)-1
		        	if(jcn(k) .NE. k) then
								epsi=targetPrec - ceiling(log(crit(absAabsx(i),normA) / abs(A(k)))/log(2.d0))
				        epsi=minval(A_Xprec%availPrec, 1, A_Xprec%availPrec>=epsi)
				    	else
				    		epsi=53
				    	endif		     
		          call adapt_XPREC(epsi, i, jcn(k), A(k), A_Xprec, nbread, DL)
		       enddo
		    enddo

	     	nbread=nbread/8
      end subroutine

      subroutine adapt_rd_wrap(A_adapt, nbread, irn, nnz, A, jcn, absAabsx, normA, targetPrec, crit, applyAdapt)
		    implicit none
				external applyAdapt
				type(realDoubleCSR) :: A_adapt
				integer*8, intent(out) :: nbread
				integer*8, dimension(A_adapt%n+1) :: irn
				integer*8 :: nnz
				double precision, dimension(nnz), intent(in) :: A
				integer, dimension(nnz) :: jcn
				double precision, dimension(A_adapt%n+1) :: absAabsx
				integer :: targetPrec
				double precision :: normA
				double precision :: crit
				integer :: kX
			
			!local variable
			integer :: i, epsi, k	
			
			nbread=0
			do i=1,A_adapt%n
				A_adapt%a_s%ia(i+1) = A_adapt%a_s%ia(i)
				A_adapt%a_d%ia(i+1) = A_adapt%a_d%ia(i)
				do k=irn(i),irn(i+1)-1
					if (irn(i) .NE. jcn(i)) then
						epsi=targetPrec - ceiling(log(crit(absAabsx(i),normA) / abs(A(k)))/log(2.d0))
						epsi=minval(A_adapt%availPrec,1,A_adapt%availPrec>=epsi)
					else
						epsi=53
					endif
					call applyAdapt(epsi, i, jcn(k), A(k), A_adapt, nbread)
				enddo
			enddo 
			nbread=nbread/8
		end subroutine
		    
		subroutine jacobi_precond(a, ia, ja, n, nnz, D)
			implicit none
			double precision, intent(in) :: a(nnz)
			integer*8, intent(in) :: ia(n+1)
			integer, intent(in) :: ja(nnz)
			integer, intent(in) :: n
			integer*8, intent(in) :: nnz
			double precision, intent(out) :: D(n)
			!local variables
			integer :: i
			integer*8 :: k
			
			D=1.d0
			do i=1,n
				do k=ia(i), ia(i+1)-1
					if (ja(k).eq.i .AND. A(k).NE.0.0) then
						D(i) = 1.d0/A(k)
					endif
				enddo
			enddo
        end subroutine jacobi_precond

		subroutine jacobi_dble(a, ia, ja, n, nnz, D, a_jacobi)
			implicit none
			double precision, intent(in) :: a(nnz)
			integer*8, intent(in) :: ia(n+1)
			integer, intent(in) :: ja(nnz)
			integer, intent(in) :: n
			integer*8, intent(in) :: nnz
			double precision, intent(in) :: D(n)
			double precision, intent(out) :: A_jacobi(nnz)
			!local variables
			integer :: i
			integer*8 :: k
		
			a_jacobi = 0.d0
			do i=1,n
				do k=ia(i), ia(i+1)-1
					a_jacobi(k) = A(k)*D(i)
				enddo
			enddo
			
        end subroutine jacobi_dble
        
		subroutine jacobi_adapt_rd(a, n, nnz, D, a_jacobi)
			type(realDoubleCSR), intent(in) :: A
			integer, intent(in) :: n
			integer*8, intent(in) :: nnz
			double precision, intent(in) :: D(n)
			type(realDoubleCSR), intent(out) :: a_jacobi
			!local variables
			integer :: i
			integer*8 :: k

			a_jacobi%n = n
			allocate(a_jacobi%a_s%a(nnz), a_jacobi%a_d%a(nnz))
			allocate(a_jacobi%a_s%ia(n+1), a_jacobi%a_d%ia(n+1))
			allocate(a_jacobi%a_s%ja(nnz), a_jacobi%a_d%ja(nnz))
			a_jacobi%a_s%ia = a%a_s%ia
			a_jacobi%a_s%ja = a%a_s%ja
			a_jacobi%a_d%ia = a%a_d%ia
			a_jacobi%a_d%ja = a%a_d%ja

			do i=1,n
				do k=a_jacobi%a_s%ia(i), a_jacobi%a_s%ia(i+1)-1
					a_jacobi%a_s%a(k) = a%a_s%a(k)*D(i)
				enddo
				do k=a_jacobi%a_d%ia(i), a_jacobi%a_d%ia(i+1)-1
					a_jacobi%a_d%a(k) = a%a_d%a(k)*D(i)
				enddo
			enddo
			
			end subroutine jacobi_adapt_rd
			
        
	subroutine jacobi_adapt_7PREC(a_adapt, n, nnz, D, a_jacobi)
		type(intMultiCSR), intent(in) :: A_adapt
		integer, intent(in) :: n
		integer*8, intent(in) :: nnz
		double precision, intent(in) :: D(n)
		type(intMultiCSR), intent(out) :: a_jacobi
		!local variables
		integer :: i,p
		integer*8 :: k
		double precision :: aij
		real :: aij_r
        allocate(a_jacobi%availPrec(8))
        a_jacobi%availPrec =(/ 0, 16-8, 24-8, 32-8, 40-11, 48-11, 56-11, 64-11 /)
        a_jacobi%nbprec = 7      
        a_jacobi%n = n

        do p=1,7
            allocate(a_jacobi%tab(p)%a((p+1)*nnz), a_jacobi%tab(p)%ja(nnz), a_jacobi%tab(p)%ia(n+1))
            a_jacobi%tab(p)%ia=a_adapt%tab(p)%ia
            a_jacobi%tab(p)%ja=a_adapt%tab(p)%ja
        enddo
            
	do i=1,n
	    do k=A_adapt%tab(1)%ia(i), A_adapt%tab(1)%ia(i+1)-1
	    	ind=1
	        call convert_16_32(aij_r, A_adapt%tab(1)%a((1+1)*k-1))
	        call convert_32_16(aij_r*real(D(i)), A_jacobi%tab(ind)%a((ind+1)*k-ind))
	    enddo
	    do k=A_adapt%tab(2)%ia(i), A_adapt%tab(2)%ia(i+1)-1
	    	ind=2
	        call convert_24_32(aij_r, A_adapt%tab(2)%a((2+1)*k-2))
	        call convert_32_24(aij_r*real(D(i)), A_jacobi%tab(ind)%a((ind+1)*k-ind))
	    enddo
	    do k=A_adapt%tab(3)%ia(i), A_adapt%tab(3)%ia(i+1)-1
	    	ind=3
	        call convert_32F_32I(aij_r, A_adapt%tab(3)%a((3+1)*k-3))
	        call convert_32I_32F(aij_r*real(D(i)), A_jacobi%tab(ind)%a((ind+1)*k-ind))
	    enddo
	    do k=A_adapt%tab(4)%ia(i), A_adapt%tab(4)%ia(i+1)-1
	    	ind=4
	        call convert_40_64(aij, A_adapt%tab(4)%a((4+1)*k-4))
	        call convert_64_40(aij*D(i), A_jacobi%tab(ind)%a((ind+1)*k-ind))
	    enddo
	    do k=A_adapt%tab(5)%ia(i), A_adapt%tab(5)%ia(i+1)-1
	    	ind=5
	        call convert_48_64(aij, A_adapt%tab(5)%a((5+1)*k-5))
	        call convert_64_48(aij*D(i), A_jacobi%tab(ind)%a((ind+1)*k-ind))
	    enddo
	    do k=A_adapt%tab(6)%ia(i), A_adapt%tab(6)%ia(i+1)-1
	    	ind=6
	        call convert_56_64(aij, A_adapt%tab(6)%a((6+1)*k-6))
	        call convert_64_56(aij*D(i), A_jacobi%tab(ind)%a((ind+1)*k-ind))
	    enddo
	    do k=A_adapt%tab(7)%ia(i), A_adapt%tab(7)%ia(i+1)-1
	    	ind=7
	        call convert_64F_64I(aij, A_adapt%tab(7)%a((7+1)*k-7))
	        call convert_64I_64F(aij*D(i), A_jacobi%tab(ind)%a((ind+1)*k-ind))
	    enddo
	enddo

	end subroutine jacobi_adapt_7PREC

end module
