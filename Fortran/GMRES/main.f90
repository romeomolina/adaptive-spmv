program main
	use omp_lib
	use initmod
	use csr
	use adapt
	use spmv
	use gmres_iter
	implicit none

	character :: ifile*128
	character*10 :: arg
	integer :: n, iunit, i, j
	integer, allocatable :: ja(:)
	integer*8, allocatable :: ia(:)
	integer*8 :: k, nnz
	double precision, allocatable :: A(:), A_in(:), A_out(:), x(:), x0(:)
	real, allocatable :: A_in_s(:), x_s(:), A_bf(:)
	REAL(KIND=16), allocatable :: Aq(:)
	
	! normes et apparentés
	double precision :: normA, normx, normAnormx
	double precision, allocatable :: absAabsx(:), absAe(:)

	! adaptive matrices
	type(realDoubleCSR) :: A_real_dble_NW, A_real_dble_CW, A_real_dble_NW_DL, A_real_dble_CW_DL, A_real_dble_NW_no_diag
	type(realDoubleCSR) :: A_in_rd, A_out_rd
	type(intMultiCSR) :: A_in_7PREC, A_7PREC, A_out_7PREC
	
    integer*8 :: uselessint

	! à modifier
	integer :: targets(6)=(/8, 12, 16, 18, 20, 24/)
	integer :: p
    
    ! GMRES
    double precision :: tsolve
    double precision, allocatable :: rhs(:)
    ! GMRES PARAMETER
    double precision :: tol_in=dble(1E-6), tol_out=dble(1E-14)
    integer :: iter_in=80, iter_out=50
    ! Jacobi
    double precision, allocatable :: D(:)
    
    !Tests
    double precision, allocatable :: w(:), ones(:)
    
    !Couts
    integer*8 :: cout,coutFP32

    ! READ ARGUMENTS
    if (iargc() .eq. 1) then
        call getarg(1,ifile)
        iunit = 8
        open(unit=iunit,file=ifile)
    elseif (iargc() .eq. 5) then
        call getarg(1,ifile)
        iunit = 8
        open(unit=iunit,file=ifile)
        call getarg(2,arg)
        read (arg,'(I10)') iter_in
        call getarg(3,arg)
        read (arg,'(I10)') iter_out
        call getarg(4,arg)
        read (arg,*) tol_in
        call getarg(5,arg)
        read (arg,*) tol_out
    else
        write(*,*) 'wrong number of arguments'
        write(*,*) "usage : "
        write(*,*) "path/mat.mtx iter_in iter_out tol_in tol_out"
        stop
    endif

    ! CONVERT MATRIX IN CSR FORMAT
    call init_matrix(iunit, n, nnz, A, Aq, ia, ja)

    ! INITIALIZE X VECTOR
    iter_in = min(iter_in,n)
    !iter_out = min(iter_out, n)
    allocate(x(n),rhs(n),x0(n))
    call random_number(x0)
    x=x0
    rhs= 1.d0

    ! COMPUTE absAabsx = abs(A)*abs(x) and absAe = abs(A)*e
    allocate(absAabsx(n),absAe(n))
    absAabsx = 0.d0
    absAe = 0.d0
    !$omp parallel do
    do i=1,n
      do k=ia(i),ia(i+1)-1
        absAabsx(i) = absAabsx(i) + abs(A(k)*x(ja(k)))
        absAe(i) = absAe(i) + abs(A(k))
      enddo
    enddo
    !$omp end parallel do
        
    ! COMPUTE normA, normx, normAnormx
    normA = maxval(absAe,1)
    normx = maxval(abs(x),1)
    normAnormx = normA*normx

     ! ALLOCATE A_real_dble_NW STRUCTURE
    A_real_dble_NW%n = n
    allocate(A_real_dble_NW%a_s%a(nnz), A_real_dble_NW%a_d%a(nnz))
    allocate(A_real_dble_NW%a_s%ia(n+1), A_real_dble_NW%a_d%ia(n+1))
    allocate(A_real_dble_NW%a_s%ja(nnz), A_real_dble_NW%a_d%ja(nnz))
    
    ! ALLOCATE A_7PREC
    allocate(A_7PREC%availPrec(8))
    A_7PREC%availPrec =(/ 0, 16-8, 24-8, 32-8, 40-11, 48-11, 56-11, 64-11 /)
    A_7PREC%nbprec = 7      
    A_7PREC%n = n
    do p=1,7
        allocate(A_7PREC%tab(p)%a((p+1)*nnz), A_7PREC%tab(p)%ja(nnz), A_7PREC%tab(p)%ia(n+1))
    enddo
    
    ! COMPUTE JACOBI PRECONDITIONER
    allocate(D(n), A_in(nnz), A_out(nnz), A_in_s(nnz), x_s(n),ones(n))
    call jacobi_precond(a, ia, ja, n, nnz, D)
    A_out=A
	call jacobi_dble(A_out, ia, ja, n, nnz, D, A_in)
    A_in_s=real(A_in)
    ones=1.d0
    call init_A_XPREC(A_7PREC)
    call adapt_wrap(A_7PREC, uselessint, ia, nnz, A, ja, absAe, normA, 53, critCW, 1)
    call jacobi_adapt_7PREC(A_7PREC, n, nnz, ones, A_out_7PREC)
    call init_A_real_dble(A_real_dble_NW)
    call adapt_rd_wrap(A_real_dble_NW, uselessint, ia, nnz, A, ja, absAe, normA, 53, critCW, adapt_rd)
    call jacobi_adapt_rd(A_real_dble_NW, n, nnz, ones, A_out_rd)

	allocate(A_bf(nnz))
	A_bf=real(A_in)
	do i=1,nnz
		call TRUNCATE_16(A_bf(i))
	enddo
	
!	write(*,*) "================="
!	write(*,*) "UNIFORM FP64"
!	x=x0
!	x_s=real(x)
!	call mgmres_uniform(n, nnz, ia, ja, A_in, A_in_s, A_out, x, x_s, rhs, D, iter_in, iter_out, tol_in, tol_out, "UFP64")

	write(*,*) "================="
	write(*,*) "UNIFORM FP32"
	x=x0
	x_s=real(x)
	call mgmres_uniform(n, nnz, ia, ja, A_in, A_in_s, A_out, x, x_s, rhs, D, iter_in, iter_out, tol_in, tol_out, "UFP32")
	coutFP32=nnz*4

	write(*,*) "================="
	write(*,*) "UNIFORM BF16"
	x=x0
	x_s=real(x)	
	call mgmres_uniform(n, nnz, ia, ja, A_in, A_bf, A_out, x, x_s, rhs, D, iter_in, iter_out, tol_in, tol_out, "UFP32")
	cout=nnz*2
	write(*,*) "COST WRT FP32: ", 100 * cout/coutFP32," %"
	
    ! EVALUATION OF ADAPTIVE VERSIONS

    do j=1,6
		write(*,*) "================="
		write(*,*) "ADAPTIVE ",targets(j),"SIG. BITS - NORMWISE - 7 PRECISIONS"
		call init_A_Xprec(A_7PREC)
        call adapt_wrap(A_7PREC, cout, ia, nnz, A, ja, absAe, normA, targets(j), critNW, 0)
		call jacobi_adapt_7PREC(A_7PREC, n, nnz, D, A_in_7PREC)
		x=x0
		call mgmres_adapt_XPREC(n, nnz, A_in_7PREC, A_out_7PREC, x, rhs, D, iter_in, iter_out, tol_in, tol_out, normA)
		write(*,*) "COST WRT FP32: ", 100 * cout/coutFP32," %"
		
		write(*,*) "================="
		write(*,*) "ADAPTIVE ",targets(j),"SIG. BITS - COMPONENTWISE - 7 PRECISIONS"
		call init_A_Xprec(A_7PREC)
        call adapt_wrap(A_7PREC, cout, ia, nnz, A, ja, absAe, normA, targets(j), critCW, 0)
		call jacobi_adapt_7PREC(A_7PREC, n, nnz, D, A_in_7PREC)
		x=x0
		call mgmres_adapt_XPREC(n, nnz, A_in_7PREC, A_out_7PREC, x, rhs, D, iter_in, iter_out, tol_in, tol_out, normA)
		write(*,*) "COST WRT FP32: ", 100 * cout/coutFP32," %"
		
!		write(*,*) "================="
!		write(*,*) "ADAPTIVE ",targets(j),"BITS - NORMWISE - 7 PRECISIONS DROPLESS"
!		call init_A_Xprec(A_7PREC)
 !       call adapt_wrap(A_7PREC, cout, ia, nnz, A, ja, absAe, normA, targets(j), critNW, 1)
!		call jacobi_adapt_7PREC(A_7PREC, n, nnz, D, A_in_7PREC)
!		x=x0
!		call mgmres_adapt_XPREC(n, nnz, A_in_7PREC, A_out_7PREC, x, rhs, D, iter_in, iter_out, tol_in, tol_out, normA)
!		write(*,*) "COST WRT FP32: ", 100 * cout/coutFP32," %"
		
		
!		write(*,*) "================="
!		write(*,*) "ADAPTIVE ",targets(j),"BITS - NORMWISE" ! CW64-out Memes resultats que NW-IN UNIF-OUT
!		call init_A_real_dble(A_real_dble_NW)
!		call adapt_rd_wrap(A_real_dble_NW, uselessint, ia, nnz, A, ja, absAe, normA, targets(j), critNW, adapt_rd)
!		call jacobi_adapt_rd(A_real_dble_NW, n, nnz, D, A_in_rd)
!		x=x0
!		call mgmres_adapt_B(n, nnz, A_in_rd, A_out_rd, x, rhs, D, iter_in, iter_out, tol_in, tol_out, ax_adapt_rd, normA)
!		
!		write(*,*) "================="
!		write(*,*) "ADAPTIVE ",targets(j),"BITS - COMPONENTWISE" ! CW64-out 
!		call init_A_real_dble(A_real_dble_CW)
!		call adapt_rd_wrap(A_real_dble_CW, uselessint, ia, nnz, A, ja, absAe, normA, targets(j), critCW, adapt_rd)
!		call jacobi_adapt_rd(A_real_dble_CW, n, nnz, D, A_in_rd)
!		x=x0
!		call mgmres_adapt_B(n, nnz, A_in_rd, A_out_rd, x, rhs, D, iter_in, iter_out, tol_in, tol_out, ax_adapt_rd, normA)
!		
!		write(*,*) "================="
!		write(*,*) "ADAPTIVE ",targets(j),"BITS - NORMWISE DROPLESS"	
!		call init_A_real_dble(A_real_dble_NW_DL)
!		call adapt_rd_wrap(A_real_dble_NW_DL, uselessint, ia, nnz, A, ja, absAe, normA, targets(j), critNW, adapt_rd_DL)
!		call jacobi_adapt_rd(A_real_dble_NW_DL, n, nnz, D, A_in_rd)
!		x=x0
!		call mgmres_adapt(n, nnz, ia, ja, A_in_rd, A_out, x, rhs, D, iter_in, iter_out, tol_in, tol_out, ax_adapt_rd)
!		
!		write(*,*) "================="
!		write(*,*) "ADAPTIVE ",targets(j),"BITS - NORMWISE - 7 PRECISIONS"
!		call init_A_Xprec(A_7PREC)
 !       call adapt_wrap(A_7PREC, uselessint, ia, nnz, A, ja, absAe, normA, targets(j), critNW)
	!	call jacobi_adapt_7PREC(A_7PREC, n, nnz, D, A_in_7PREC)
	!	x=x0
	!	call mgmres_adapt_XPREC(n, nnz, ia, ja, A_in_7PREC, A_out, x, rhs, D, iter_in, iter_out, tol_in, tol_out, ax_adapt_7P)

	enddo

	  
    ! DEALLOCATE EVERYTHING
    deallocate(ia, ja, A, A_in, A_out, A_in_s)
    deallocate(D, rhs)
    deallocate(x, x_s)
    deallocate(absAabsx,absAe)

  end program main
