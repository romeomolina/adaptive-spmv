      module initmod
        contains
          subroutine init_matrix(iunit, n, nnz, A, Aq, irn, jcn)
            ! parameters
            integer, intent(in) :: iunit
            integer, intent(out) :: n
            integer*8, intent(out) :: nnz
            double precision, allocatable, intent(out) :: A(:)
            REAL(KIND=16), allocatable, intent(out) :: Aq(:)
            integer, allocatable, intent(out) :: jcn(:)
            integer*8, allocatable, intent(out) :: irn(:)
            ! local variables
            character :: rep*10, field*7, symm*19
            double precision, allocatable :: A_coo(:)
            integer, allocatable :: irn_coo(:), jcn_coo(:), work(:)
            integer*8 :: ind, k, i
	    integer :: ival(1)
            integer*8 :: nnz2
            integer, parameter :: nnzmax=1e9
            complex :: cval(1)
	    
	    nnz=0
	    
            ! read n/nnz info to initialize structures
            call mminfo(iunit, rep, field, symm, n, n, nnz)
            if (rep .ne. 'coordinate') then
              write(*,*) 'wrong rep',rep
              stop
            endif
            if (field .ne. 'real') then
              ! complex not supported yet
              write(*,*) 'wrong field',field
              stop
            endif
            write(*,'(A,A,A)')   ' Matrix is ', field, symm
            write(*,*) ' Matrix size     = ', n
            write(*,*) ' Matrix nonzeros = ', nnz

            ! because we unsymmetrize the matrix, nnz is almost doubled
            if (symm.eq.'symmetric') then
              nnz2 = nnz
              nnz = 2*nnz - n
            endif
            allocate(irn_coo(nnz), jcn_coo(nnz), A_coo(nnz))
            allocate(work(nnz))

            ! read matrix values and indices under COO format
            call mmread(iunit, rep, field, symm, n, n, nnz2, nnzmax, &
              irn_coo, jcn_coo, ival, A_coo, cval)

            ! unsymmetrize matrix if necessary
            if (symm.eq.'symmetric') then
              ind=1
              do k=1,nnz2
                if (jcn_coo(k) .ne. irn_coo(k)) then
!                  write(*,*) "A_coo(nnz2+ind)", A_coo(nnz2+ind), "A_coo(k)", A_coo(k), "k", k, "nnz2+ind", nnz2+ind
                  A_coo(nnz2+ind) = A_coo(k)
                  irn_coo(nnz2+ind) = jcn_coo(k)
                  jcn_coo(nnz2+ind) = irn_coo(k)
                  ind = ind+1
                endif
              enddo
            endif

            ! convert COO to CSR format
            allocate(irn(n+1), jcn(nnz), A(nnz))
            allocate(Aq(nnz))
            work = 0
            irn(1) = 0
            ind = 1
            ! first loop counts the number of nonzeros on each row row
            do k=1,nnz
              work(irn_coo(k)) = work(irn_coo(k)) + 1 
            enddo

            ! second loop computes irn pointer
            irn(1) = 1
            do k=1,n
              irn(k+1) = irn(k) + work(k)
            enddo

            ! third loop puts everything in the right place
            work = 0
            do k=1,nnz
              i = irn_coo(k)
              ind = irn(i) + work(i)
              jcn(ind) = jcn_coo(k)
              A(ind) = A_coo(k)
              Aq(ind) = A_coo(k)
              work(i) = work(i)+1
            enddo

            ! clean up
            deallocate(work, jcn_coo, A_coo, irn_coo)
          end subroutine init_matrix
          
          
          
          
           subroutine init_matrix_q(iunit, n, nnz, A, irn, jcn)
            ! parameters
            integer, intent(in) :: iunit
            integer, intent(out) :: n
            integer*8, intent(out) :: nnz
            REAL(KIND=16), allocatable, intent(out) :: A(:)
            integer, allocatable, intent(out) :: jcn(:)
            integer*8, allocatable, intent(out) :: irn(:)
            ! local variables
            character :: rep*10, field*7, symm*19
            double precision, allocatable :: A_coo(:)
            integer, allocatable :: irn_coo(:), jcn_coo(:), work(:)
            integer :: ind, k, i, ival(1)
            integer*8 :: nnz2
            integer, parameter :: nnzmax=1e9
            complex :: cval(1)
	    
	    nnz=0
	    
            ! read n/nnz info to initialize structures
            call mminfo(iunit, rep, field, symm, n, n, nnz)
            if (rep .ne. 'coordinate') then
              write(*,*) 'wrong rep',rep
              stop
            endif
            if (field .ne. 'real') then
              ! complex not supported yet
              write(*,*) 'wrong field',field
              stop
            endif
            write(*,'(A,A,A)')   ' Matrix is ', field, symm
            write(*,*) ' Matrix size     = ', n
            write(*,*) ' Matrix nonzeros = ', nnz

            ! because we unsymmetrize the matrix, nnz is almost doubled
            if (symm.eq.'symmetric') then
              nnz2 = nnz
              nnz = 2*nnz - n
            endif
            allocate(irn_coo(nnz), jcn_coo(nnz), A_coo(nnz))
            allocate(work(nnz))

            ! read matrix values and indices under COO format
            call mmread(iunit, rep, field, symm, n, n, nnz2, nnzmax, &
              irn_coo, jcn_coo, ival, A_coo, cval)

            ! unsymmetrize matrix if necessary
            if (symm.eq.'symmetric') then
              ind=1
              do k=1,nnz2
                if (jcn_coo(k) .ne. irn_coo(k)) then
                  A_coo(nnz2+ind) = A_coo(k)
                  irn_coo(nnz2+ind) = jcn_coo(k)
                  jcn_coo(nnz2+ind) = irn_coo(k)
                  ind = ind+1
                endif
              enddo
            endif

            ! convert COO to CSR format
            allocate(irn(n+1), jcn(nnz), A(nnz))
            work = 0
            irn(1) = 0
            ind = 1
            ! first loop counts the number of nonzeros on each row row
            do k=1,nnz
              work(irn_coo(k)) = work(irn_coo(k)) + 1 
            enddo

            ! second loop computes irn pointer
            irn(1) = 1
            do k=1,n
              irn(k+1) = irn(k) + work(k)
            enddo

            ! third loop puts everything in the right place
            work = 0
            do k=1,nnz
              i = irn_coo(k)
              ind = irn(i) + work(i)
              jcn(ind) = jcn_coo(k)
              A(ind) = A_coo(k)
              work(i) = work(i)+1
            enddo

            ! clean up
            deallocate(work, jcn_coo, A_coo, irn_coo)
          end subroutine init_matrix_q
        end module initmod

