program main
	use omp_lib
	use initmod
	use csr
	use adapt
	use spmv
	implicit none

	character :: ifile*128
	character*128 :: mat
	integer :: n
	integer :: iunit
	integer :: i, j
	integer, allocatable :: ja(:)
	integer*8, allocatable :: ia(:)
	integer*8 :: k, nnz
	double precision, allocatable :: A(:), x(:)
	real, allocatable :: A_s(:), x_s(:)
	double precision, allocatable :: y1(:), y2(:), y3(:), y4(:), y5(:), y6(:), y7(:), y8(:), y9(:), y10(:), y11(:)
	REAL(KIND=16), allocatable :: Aq(:), xq(:), yq(:)
	
	! normes et apparentés
	double precision :: normA, normx, normAnormx
	double precision, allocatable :: absAabsx(:), absAe(:)

	! adaptive matrices
	type(realDoubleCSR) :: A_real_dble_NW, A_real_dble_CW, A_real_dble_NW_DL, A_real_dble_CW_DL
	type(intMultiCSR) :: A_3prec_CW, A_3prec_NW, A_7prec_CW, A_7prec_NW

	! à modifier
	integer :: nbit=3
	integer :: targets(3)=(/24,37,53/)
	
	! données mesurées
	integer*8 :: read1, read2, read3
	double precision :: time1, time2, time3
	double precision :: error1_CW, error1_NW, error2_CW, error2_NW, error3_CW, error3_NW
	double precision, allocatable :: error4(:), error5(:), error6(:), error7(:), error8(:), error9(:), error10(:), error11(:)
	double precision, allocatable :: time4(:),time5(:), time6(:),time7(:), time8(:), time9(:), time10(:), time11(:)
	integer*8, allocatable :: read4(:), read5(:), read6(:), read7(:), read8(:), read9(:), read10(:), read11(:)
	
	! lissage
	integer :: repetition=100
	
	
  ! READ ARGUMENTS
  if (iargc() .eq. 1) then
      call getarg(1,ifile)
      iunit = 8
      open(unit=iunit,file=ifile)
  else
      write(*,*) 'wrong number of arguments'
      write(*,*) "usage : "
      write(*,*) "path/mat.mtx"
      stop
  endif

  ! CONVERT MATRIX IN CSR FORMAT
  call init_matrix(iunit, n, nnz, A, Aq, ia, ja)

  ! INITIALIZE X VECTOR
  allocate(x(n),xq(n))
  !iseed = (/1,1,1,1/)
  !call dlarnv(idist, iseed, n, x)
	!x=dble(1)/dble(n)
	!Plus simple pour avoir x et xq égaux
	x=1.d0
	xq=1.d0


  ! COMPUTE absAabsx = abs(A)*abs(x) and absAe = abs(A)*e
  allocate(absAabsx(n),absAe(n))
  absAabsx = 0.d0
  absAe = 0.d0
  !$omp parallel do
  do i=1,n
    do k=ia(i),ia(i+1)-1
      absAabsx(i) = absAabsx(i) + abs(A(k)*x(ja(k)))
      absAe(i) = absAe(i) + abs(A(k))
    enddo
  enddo
  !$omp end parallel do
      
  ! COMPUTE normA, normx, normAnormx
  normA = maxval(absAe,1)
  normx = maxval(abs(x),1)
  normAnormx = normA*normx
  
  ! INITIALIZE Y VECTORS
  allocate(y1(n), y2(n), y3(n), y4(n), y5(n), y6(n), y7(n), y8(n), y9(n), y10(n), y11(n)) 
  allocate(yq(n))
  allocate(A_s(n), x_s(n))
  A_s=real(A); x_s=real(x)
  
  ! ALLOCATE A_real_dble_NW STRUCTURE
  A_real_dble_NW%n = n
  allocate(A_real_dble_NW%a_s%a(nnz), A_real_dble_NW%a_d%a(nnz))
  allocate(A_real_dble_NW%a_s%ia(n+1), A_real_dble_NW%a_d%ia(n+1))
  allocate(A_real_dble_NW%a_s%ja(nnz), A_real_dble_NW%a_d%ja(nnz))
  
  ! ALLOCATE A_real_dble_CW STRUCTURE
  A_real_dble_CW%n = n
  allocate(A_real_dble_CW%a_s%a(nnz), A_real_dble_CW%a_d%a(nnz))
  allocate(A_real_dble_CW%a_s%ia(n+1), A_real_dble_CW%a_d%ia(n+1))
  allocate(A_real_dble_CW%a_s%ja(nnz), A_real_dble_CW%a_d%ja(nnz))
  
  ! ALLOCATE A_real_dble_NW_DL STRUCTURE DROPLESS
  A_real_dble_NW_DL%n = n
  allocate(A_real_dble_NW_DL%a_s%a(nnz), A_real_dble_NW_DL%a_d%a(nnz))
  allocate(A_real_dble_NW_DL%a_s%ia(n+1), A_real_dble_NW_DL%a_d%ia(n+1))
  allocate(A_real_dble_NW_DL%a_s%ja(nnz), A_real_dble_NW_DL%a_d%ja(nnz))
  
  ! ALLOCATE A_real_dble_CW_DL STRUCTURE DROPLESS
  A_real_dble_CW_DL%n = n
  allocate(A_real_dble_CW_DL%a_s%a(nnz), A_real_dble_CW_DL%a_d%a(nnz))
  allocate(A_real_dble_CW_DL%a_s%ia(n+1), A_real_dble_CW_DL%a_d%ia(n+1))
  allocate(A_real_dble_CW_DL%a_s%ja(nnz), A_real_dble_CW_DL%a_d%ja(nnz))
    
	! ALLOCATE A_3prec_NW STRUCTURE
	allocate(A_3prec_NW%availPrec(4))
	A_3prec_NW%availPrec =(/ 0, 16-8, 32-8, 64-11 /)
	A_3prec_NW%nbprec = 3
	A_3prec_NW%n = n   
	allocate(A_3prec_NW%tab(1)%a((1+1)*nnz), A_3prec_NW%tab(1)%ja(nnz), A_3prec_NW%tab(1)%ia(n+1))
	allocate(A_3prec_NW%tab(3)%a((3+1)*nnz), A_3prec_NW%tab(3)%ja(nnz), A_3prec_NW%tab(3)%ia(n+1))
	allocate(A_3prec_NW%tab(7)%a((7+1)*nnz), A_3prec_NW%tab(7)%ja(nnz), A_3prec_NW%tab(7)%ia(n+1))	
	
	! ALLOCATE A_3prec_CW STRUCTURE
	allocate(A_3prec_CW%availPrec(4))
	A_3prec_CW%availPrec =(/ 0, 16-8, 32-8, 64-11 /)
	A_3prec_CW%nbprec = 3
	A_3prec_CW%n = n   
	allocate(A_3prec_CW%tab(1)%a((1+1)*nnz), A_3prec_CW%tab(1)%ja(nnz), A_3prec_CW%tab(1)%ia(n+1))
	allocate(A_3prec_CW%tab(3)%a((3+1)*nnz), A_3prec_CW%tab(3)%ja(nnz), A_3prec_CW%tab(3)%ia(n+1))
	allocate(A_3prec_CW%tab(7)%a((7+1)*nnz), A_3prec_CW%tab(7)%ja(nnz), A_3prec_CW%tab(7)%ia(n+1))	
	 
	! ALLOCATE A_7prec_NW STRUCTURE
	allocate(A_7prec_NW%availPrec(8))
	A_7prec_NW%availPrec =(/ 0, 16-8, 24-8, 32-8, 40-11, 48-11, 56-11, 64-11 /)
	A_7prec_NW%nbprec = 7      
	A_7prec_NW%n = n
	allocate(A_7prec_NW%tab(1)%a((1+1)*nnz), A_7prec_NW%tab(1)%ja(nnz), A_7prec_NW%tab(1)%ia(n+1))
	allocate(A_7prec_NW%tab(2)%a((2+1)*nnz), A_7prec_NW%tab(2)%ja(nnz), A_7prec_NW%tab(2)%ia(n+1))
	allocate(A_7prec_NW%tab(3)%a((3+1)*nnz), A_7prec_NW%tab(3)%ja(nnz), A_7prec_NW%tab(3)%ia(n+1))
	allocate(A_7prec_NW%tab(4)%a((4+1)*nnz), A_7prec_NW%tab(4)%ja(nnz), A_7prec_NW%tab(4)%ia(n+1))
	allocate(A_7prec_NW%tab(5)%a((5+1)*nnz), A_7prec_NW%tab(5)%ja(nnz), A_7prec_NW%tab(5)%ia(n+1))
	allocate(A_7prec_NW%tab(6)%a((6+1)*nnz), A_7prec_NW%tab(6)%ja(nnz), A_7prec_NW%tab(6)%ia(n+1))
	allocate(A_7prec_NW%tab(7)%a((7+1)*nnz), A_7prec_NW%tab(7)%ja(nnz), A_7prec_NW%tab(7)%ia(n+1))	
	
	! ALLOCATE A_7prec_CW STRUCTURE
	allocate(A_7prec_CW%availPrec(8))
	A_7prec_CW%availPrec =(/ 0, 16-8, 24-8, 32-8, 40-11, 48-11, 56-11, 64-11 /)
	A_7prec_CW%nbprec = 7      
	A_7prec_CW%n = n
	allocate(A_7prec_CW%tab(1)%a((1+1)*nnz), A_7prec_CW%tab(1)%ja(nnz), A_7prec_CW%tab(1)%ia(n+1))
	allocate(A_7prec_CW%tab(2)%a((2+1)*nnz), A_7prec_CW%tab(2)%ja(nnz), A_7prec_CW%tab(2)%ia(n+1))
	allocate(A_7prec_CW%tab(3)%a((3+1)*nnz), A_7prec_CW%tab(3)%ja(nnz), A_7prec_CW%tab(3)%ia(n+1))
	allocate(A_7prec_CW%tab(4)%a((4+1)*nnz), A_7prec_CW%tab(4)%ja(nnz), A_7prec_CW%tab(4)%ia(n+1))
	allocate(A_7prec_CW%tab(5)%a((5+1)*nnz), A_7prec_CW%tab(5)%ja(nnz), A_7prec_CW%tab(5)%ia(n+1))
	allocate(A_7prec_CW%tab(6)%a((6+1)*nnz), A_7prec_CW%tab(6)%ja(nnz), A_7prec_CW%tab(6)%ia(n+1))
	allocate(A_7prec_CW%tab(7)%a((7+1)*nnz), A_7prec_CW%tab(7)%ja(nnz), A_7prec_CW%tab(7)%ia(n+1))	
    
  ! INITIALIZE time AND error
  time1 = 0.d0; time2 = 0.d0; time3 = 0.d0 
  error1_CW = 0.d0; error1_NW = 0.d0; error2_CW = 0.d0; error2_NW = 0.d0; error3_CW = 0.d0; error3_NW = 0.d0
  allocate(error4(nbit), error5(nbit), error6(nbit), error7(nbit), error8(nbit), error9(nbit), error10(nbit), error11(nbit))
  allocate(time4(nbit), time5(nbit), time6(nbit), time7(nbit), time8(nbit), time9(nbit), time10(nbit), time11(nbit))
  allocate(read4(nbit), read5(nbit), read6(nbit), read7(nbit), read8(nbit), read9(nbit), read10(nbit), read11(nbit))
  error4 = 0.d0; error5 = 0.d0; error6 = 0.d0; error7 = 0.d0; error8 = 0.d0; error9 = 0.d0; error10 = 0.d0; error11 = 0.d0
  time4 = 0.d0; time5 = 0.d0; time6 = 0.d0; time7 = 0.d0; time8 = 0.d0; time9 = 0.d0; time10 = 0.d0; time11 = 0.d0
  read4 = 0; read5 = 0; read6 = 0; read7 = 0; read8 = 0; read9 = 0; read10 = 0; read11 = 0
   
    
   
  ! 0/ COMPUTE UNIFORM QUAD
	call ax_fp128(n, nnz, ia, ja, Aq, x, yq)

  ! 1/ UNIFORMS COMPUTE
  do i=1,repetition
	call ax_fp64(n, nnz, ia, ja, A, x, y1, time1)
	call ax_fp48(n, nnz, ia, ja, A, x, y2, time2)
	call ax_fp32(n, nnz, ia, ja, A_s, x_s, y3, time3)
  enddo
  
  ! 1/ UNIFORMS TIMES
  time1=time1/repetition
  time2=time2/repetition
  time3=time3/repetition
    
    ! 1/ UNIFORMS ERRORS
	call spmv_error(n, y1, y1, yq, absAabsx, normAnormx, error1_NW, error1_CW)
	call spmv_error(n, y2, y2, yq, absAabsx, normAnormx, error2_NW, error2_CW)
	call spmv_error(n, y3, y3, yq, absAabsx, normAnormx, error3_NW, error3_CW)
	
	! 1/ UNIFORMS READS
	read1=64*nnz/8 
	read2=48*nnz/8  
	read3=32*nnz/8 

	! WRITE OUT IN STDOUT
	write(*,*) "==== UNIFORM ===="
	write(*,*) "FP64 ", time1, "s, read : ", read1, "bytes, CW-error : ", error1_CW, " NW-error : ", error1_NW 
	write(*,*) "FP48 ", time2, "s, read : ", read2, "bytes, CW-error : ", error2_CW, " NW-error : ", error2_NW
	write(*,*) "FP32 ", time3, "s, read : ", read3, "bytes, CW-error : ", error3_CW, " NW-error : ", error3_NW
	write(*,*) "=================================="
	write(*,*) ""
    
    ! EVALUATION OF ADAPTIVE VERSIONS

    do j=1,nbit
	
		! INITIALIZE ALL STRUCTURES
		call init_A_real_dble(A_real_dble_NW)
		call adapt_rd_wrap(A_real_dble_NW, read4(j), ia, nnz, A, ja, absAabsx, normA, targets(j), critNW, adapt_rd)
		call init_A_real_dble(A_real_dble_CW)
		call adapt_rd_wrap(A_real_dble_CW, read5(j), ia, nnz, A, ja, absAabsx, normA, targets(j), critCW, adapt_rd)
		
!		call init_A_real_dble(A_real_dble_NW_DL)
!		call adapt_rd_wrap(A_real_dble_NW_DL, read6(j), ia, nnz, A, ja, absAabsx, normA, targets(j), critNW, adaptDropLess)
!		call init_A_real_dble(A_real_dble_CW_DL)
!		call adapt_rd_wrap(A_real_dble_CW_DL, read7(j), ia, nnz, A, ja, absAabsx, normA, targets(j), critCW, adaptDropLess)
		
		call init_A_Xprec(A_3prec_NW)
		call adapt_wrap(A_3prec_NW, read8(j), ia, nnz, A, ja, absAabsx, normA, targets(j), critNW, 0)
		call init_A_Xprec(A_3prec_CW)
		call adapt_wrap(A_3prec_CW, read9(j), ia, nnz, A, ja, absAabsx, normA, targets(j), critCW, 0)
		
		call init_A_Xprec(A_7prec_NW)
		call adapt_wrap(A_7prec_NW, read10(j), ia, nnz, A, ja, absAabsx, normA, targets(j), critNW, 0)
		call init_A_Xprec(A_7prec_CW)
		call adapt_wrap(A_7prec_CW, read11(j), ia, nnz, A, ja, absAabsx, normA, targets(j), critCW, 0)

		!2/ ADAPTIVE COMPUTE
		do i=1,repetition
			call ax_adapt_rd(n, nnz, A_real_dble_NW, x, y4, time4(j))
			call ax_adapt_rd(n, nnz, A_real_dble_CW, x, y5, time5(j))
!			call ax_adapt_rd(n, nnz, A_real_dble_NW_DL, x, y6, time6(j))
!			call ax_adapt_rd(n, nnz, A_real_dble_CW_DL, x, y7, time7(j))
			call ax_adapt_3p(n, nnz, A_3prec_NW, x, y8, time8(j))
			call ax_adapt_3p(n, nnz, A_3prec_CW, x, y9, time9(j))
			call ax_adapt_7p(n, nnz, A_7prec_NW, x, y10, time10(j))
			call ax_adapt_7p(n, nnz, A_7prec_CW, x, y11, time11(j))
		enddo
		
		!2/ ADAPTIVE TIMES
		time4(j)=time4(j)/repetition
		time5(j)=time5(j)/repetition
		time6(j)=time6(j)/repetition
		time7(j)=time7(j)/repetition
		time8(j)=time8(j)/repetition
		time9(j)=time9(j)/repetition
		time10(j)=time10(j)/repetition
		time11(j)=time11(j)/repetition
			
		!2/ ADAPTIVE ERRORS
		call spmv_error(n, y4, y5, yq, absAabsx, normAnormx, error4(j), error5(j))
		call spmv_error(n, y6, y7, yq, absAabsx, normAnormx, error6(j), error7(j))
		call spmv_error(n, y8, y9, yq, absAabsx, normAnormx, error8(j), error9(j))
		call spmv_error(n, y10, y11, yq, absAabsx, normAnormx, error10(j), error11(j))

		! WRITE OUT IN STDOUT
		write(*,*) "=================================="
		
		write(*,*) "=== ADAPTIVE - targets : ", targets(j)
		write(*,*) "RD NW-CRITERIA ", time4(j), "s, read: ", read4(j), "bytes, Error(normwise): ", error4(j)
		write(*,*) "RD CW-CRITERIA ", time5(j), "s, read: ", read5(j), "bytes, Error(componentwise): ", error5(j)
		
		write(*,*) "RD NW-CRITERIA DROPLESS ", time6(j), "s, read: ", read6(j), "bytes, Error(normwise): ", error6(j)
		write(*,*) "RD CW-CRITERIA DROPLESS ", time7(j), "s, read: ", read7(j), "bytes, Error(componentwise): ", error7(j)

		write(*,*) "16/32/64 NW-CRITERIA ", time8(j), "s, read: ", read8(j), "bytes, Error(normwise): ", error8(j)
		write(*,*) "16/32/64 CW-CRITERIA ", time9(j), "s, read: ", read9(j), "bytes, Error(componentwise): ", error9(j)
		
		write(*,*) "16/24/32/40/48/56/64 NW-CRITERIA ", time10(j), "s, read: ", read10(j), "bytes, Error(normwise): ", error10(j)
		write(*,*) "16/24/32/40/48/56/64 CW-CRITERIA ", time11(j), "s, read: ", read11(j), "bytes, Error(componentwise): ", error11(j)
		write(*,*) "=================================="
		write(*,*) " "
	enddo
	

	
  
    ! DEALLOCATE EVERYTHING
    deallocate(ia, ja, A)
    deallocate(x, absAabsx)
    deallocate(xq, Aq)
    deallocate(yq, y1, y2, y3, y4, y5, y6, y7, y8, y9, y10, y11)
    deallocate(error4, error5, error6, error7, error8, error9, error10, error11)
    deallocate(time4, time5, time6, time7, time8, time9, time10, time11)
    deallocate(read4, read5, read6, read7, read8, read9, read10, read11)
  end program main
