module spmv
	contains

	! 1/ ax_fp128
	subroutine ax_fp128(n, nnz, ia, ja, A, x, w)

		implicit none
		! arguments
		integer, intent(in) :: n
		integer*8, intent(in) :: ia(n+1), nnz
		integer, intent(in) :: ja(nnz)
		REAL(KIND=16), intent(in) :: A(nnz)
		double precision, intent(in) :: x(n)
		REAL(KIND=16), intent(out) :: w(n)
		! local variables
		real :: xs(n)
		integer :: i
		integer*8 :: k
  
    	w(1:n) = 0.0D+00
    	!$omp parallel do
		do i=1,n
			do k=ia(i), ia(i+1)-1
			    w(i) = w(i) + A(k)*x(ja(k))
			enddo
		enddo
		!$omp end parallel do
		
	end subroutine ax_fp128

	
	! 1/ ax_fp64
	subroutine ax_fp64(n, nnz, ia, ja, A, x, w, tsolve)

		implicit none
		! arguments
		integer, intent(in) :: n
		integer*8, intent(in) :: ia(n+1), nnz
		integer, intent(in) :: ja(nnz)
		double precision, intent(in) :: A(nnz)
		double precision, intent(in) :: x(n)
		double precision, intent(inout) :: tsolve
		double precision, target, intent(out) :: w(n)
		! local variables
		real :: xs(n)
		integer :: i
		integer*8 :: k
		integer :: t1, t2, cr
  
    	w(1:n) = 0.0D+00
    	call system_clock(t1)
    	!$omp parallel do
		do i=1,n
			do k=ia(i), ia(i+1)-1
			    w(i) = w(i) + A(k)*x(ja(k))
			enddo
		enddo
		!$omp end parallel do
    	call system_clock(t2,cr)
		tsolve = tsolve + dble(t2-t1)/dble(cr)
		
	end subroutine ax_fp64
	
	! 2/ ax_fp48
	subroutine ax_fp48(n, nnz, ia, ja, A, x, w, tsolve)

		implicit none
		! arguments
		integer, intent(in) :: n
		integer*8, intent(in) :: ia(n+1), nnz
		integer, intent(in) :: ja(nnz)
		double precision, intent(in) :: A(nnz)
		double precision, intent(in) :: x(n)
		double precision, intent(inout) :: tsolve
		double precision, target, intent(out) :: w(n)
		! local variables
		real :: xs(n)
		integer :: i
		integer*8 :: k
		integer :: t1, t2, cr
		double precision :: val_48, prod
  
    	w(1:n) = 0.0D+00
    	call system_clock(t1)
    	!$omp parallel do private(prod,val_48)
		do i=1,n
			do k=ia(i), ia(i+1)-1
		    	call read_48(val_48, A(k))
		    	prod=val_48*x(ja(k))
		    	call read_48(val_48, prod)
		        w(i) = w(i) + val_48
		        call read_48(val_48, w(i))
		        w(i) = val_48
			enddo
		enddo
		!$omp end parallel do
    	call system_clock(t2,cr)
		tsolve = tsolve + dble(t2-t1)/dble(cr)
		
	end subroutine ax_fp48
	
    ! 2/ ax_fp32
    subroutine ax_fp32(n, nnz, ia, ja, a_s, x_s, w, tsolve)
		
		implicit none
		! arguments
		integer, intent(in) :: n 
		integer*8, intent(in) :: ia(n+1), nnz
		integer, intent(in) :: ja(nnz)
		real, intent(in) :: a_s(nnz), x_s(n)
		double precision, intent(inout) :: tsolve
		double precision, intent(out) :: w(n)
		! local variables
		real :: As(nnz), xs(n)
		integer :: i
		integer*8 :: k
		integer :: t1, t2, cr
  
    	w(1:n) = 0.0D+00
    	call system_clock(t1)
    	!$omp parallel do
		do i=1,n
	        do k=ia(i),ia(i+1)-1
	            w(i) = w(i) + dble(A_s(k)*x_s(ja(k)))
	        enddo
	    enddo
    	!$omp end parallel do
    	call system_clock(t2,cr)
		tsolve = tsolve + dble(t2-t1)/dble(cr)
	
	end subroutine ax_fp32
	
	
    ! 16/ ax_bf16
    subroutine ax_bf16(n, nnz, ia, ja, A, x, w, tsolve)
		
		implicit none
		! arguments
		integer, intent(in) :: n 
		integer*8, intent(in) :: ia(n+1), nnz
		integer, intent(in) :: ja(nnz)
		real, intent(in) :: A(nnz), x(n)
		double precision, intent(inout) :: tsolve
		double precision, intent(out) :: w(n)
		! local variables
		integer :: i
		integer*8 :: k
		real :: prod
		integer :: t1, t2, cr  
		real :: x_loc(n), A_loc(nnz)
  
    	w(1:n) = 0.d0
    	x_loc=real(x)
    	A_loc=real(A)
    	call system_clock(t1)
    	!$omp parallel do
		do i=1,n
			  call TRUNCATE_16(x_loc(i))
	      do k=ia(i),ia(i+1)-1
          call TRUNCATE_16(A_loc(k))
          prod = A_loc(k)*x_loc(ja(k))
          call TRUNCATE_16(prod)
          w(i) = w(i) + prod
	      enddo 
	    enddo
    	!$omp end parallel do
    	call system_clock(t2,cr)
		tsolve = tsolve + dble(t2-t1)/dble(cr)
	
	end subroutine ax_bf16
		
	!3/ ax_adapt_rd
	subroutine ax_adapt_rd(n, nnz, A_adapt, x, w, tsolve)
    	
    	use csr
		implicit none
		! arguments
		integer, intent(in) :: n
		integer*8, intent(in) :: nnz
		TYPE(realDoubleCSR), intent(in) :: A_adapt
		double precision, intent(in) :: x(n)
		double precision, intent(inout) :: tsolve
		double precision, target, intent(out) :: w(n)
		! local variables
		real :: xs(n)
		integer :: i
		integer*8 :: k
		integer :: t1, t2, cr
  
		xs=real(x)
    	w(1:n) = 0.0D+00
    	call system_clock(t1)
    	!$omp parallel do
		do i=1,n
			do k=A_adapt%a_s%ia(i), A_adapt%a_s%ia(i+1)-1
			    w(i) = w(i) + dble(A_adapt%a_s%a(k)*xs(A_adapt%a_s%ja(k)))
			enddo
			do k=A_adapt%a_d%ia(i), A_adapt%a_d%ia(i+1)-1
			    w(i) = w(i) + A_adapt%a_d%a(k)*x(A_adapt%a_d%ja(k))
			enddo
		enddo
    	!$omp end parallel do
    	call system_clock(t2,cr)
		tsolve = tsolve + dble(t2-t1)/dble(cr)
		
	end subroutine ax_adapt_rd
		
		
	!4/ ax_adapt_3p : 16 / 32 / 64
	!tab (1)16 (3)32 (7)64
	
	subroutine ax_adapt_3p(n, nnz, A_adapt, x, w, tsolve)
    	
    	use csr
		implicit none
		! arguments
		integer, intent(in) :: n
		integer*8, intent(in) :: nnz
		TYPE(intMultiCSR), intent(in) :: A_adapt
		double precision, intent(in) :: x(n)
		double precision, intent(inout) :: tsolve
		double precision, target, intent(out) :: w(n)
		! local variables
		real :: xs(n)
		integer :: i
		integer*8 :: k
		integer :: t1, t2, cr
		real :: aij_r
		double precision :: aij
		
		xs=real(x)
    	w(1:n) = 0.0D+00
    	call system_clock(t1)
		w = 0.d0
		!$OMP PARALLEL DO PRIVATE(aij,aij_r)
		do i=1,n
			do k=A_adapt%tab(1)%ia(i), A_adapt%tab(1)%ia(i+1)-1
			    call convert_16_32(aij_r, A_adapt%tab(1)%a((2)*k-1))
			    w(i) = w(i) + dble(aij_r*xs(A_adapt%tab(1)%ja(k)))
			enddo
			do k=A_adapt%tab(3)%ia(i), A_adapt%tab(3)%ia(i+1)-1
			    call convert_32F_32I(aij_r, A_adapt%tab(3)%a((4)*k-3))
			    w(i) = w(i) + dble(aij_r*xs(A_adapt%tab(3)%ja(k)))
			enddo
			do k=A_adapt%tab(7)%ia(i), A_adapt%tab(7)%ia(i+1)-1
			    call convert_64F_64I(aij, A_adapt%tab(7)%a((8)*k-7))
			    w(i) = w(i) + aij*x(A_adapt%tab(7)%ja(k))
			enddo
		enddo
		!$omp end parallel do
    	call system_clock(t2,cr)
		tsolve = tsolve + dble(t2-t1)/dble(cr)
		
	end subroutine ax_adapt_3p
	
	
		!7/ ax_adapt_7p : 16 / 24 / 32 / 40 / 48 / 56 / 64
		!tab (1)16 (2)24 (3)32 (4)40 (5)48 (6)56 (7)64
		
	subroutine ax_adapt_7p(n, nnz, A_adapt, x, w, tsolve)
    	
    	use csr
		implicit none
		! arguments
		integer, intent(in) :: n
		integer*8, intent(in) :: nnz
		TYPE(intMultiCSR), intent(in) :: A_adapt
		double precision, intent(in) :: x(n)
		double precision, intent(inout) :: tsolve
		double precision, target, intent(out) :: w(n)
		! local variables
		real :: xs(n)
		integer :: i
		integer*8 :: k
		integer :: t1, t2, cr
		real :: aij_r
		double precision :: aij
		
		xs=real(x)
    	w(1:n) = 0.0D+00
    	call system_clock(t1)
		w = 0.d0
		!$OMP PARALLEL DO PRIVATE(aij, aij_r)
		do i=1,n
		    do k=A_adapt%tab(1)%ia(i), A_adapt%tab(1)%ia(i+1)-1
		        call convert_16_32(aij_r, A_adapt%tab(1)%a((1+1)*k-1))
		        w(i) = w(i) + dble(aij_r*xs(A_adapt%tab(1)%ja(k)))
		    enddo
		    do k=A_adapt%tab(2)%ia(i), A_adapt%tab(2)%ia(i+1)-1
		        call convert_24_32(aij_r, A_adapt%tab(2)%a((2+1)*k-2))
		        w(i) = w(i) + dble(aij_r*xs(A_adapt%tab(2)%ja(k)))
		    enddo
		    do k=A_adapt%tab(3)%ia(i), A_adapt%tab(3)%ia(i+1)-1
		        call convert_32F_32I(aij_r, A_adapt%tab(3)%a((3+1)*k-3))
		        w(i) = w(i) + dble(aij_r*xs(A_adapt%tab(3)%ja(k)))
		    enddo
		    do k=A_adapt%tab(4)%ia(i), A_adapt%tab(4)%ia(i+1)-1
		        call convert_40_64(aij, A_adapt%tab(4)%a((4+1)*k-4))
		        w(i) = w(i) + aij*x(A_adapt%tab(4)%ja(k))
		    enddo
		    do k=A_adapt%tab(5)%ia(i), A_adapt%tab(5)%ia(i+1)-1
		        call convert_48_64(aij, A_adapt%tab(5)%a((5+1)*k-5))
		        w(i) = w(i) + aij*x(A_adapt%tab(5)%ja(k))
		    enddo
		    do k=A_adapt%tab(6)%ia(i), A_adapt%tab(6)%ia(i+1)-1
		        call convert_56_64(aij, A_adapt%tab(6)%a((6+1)*k-6))
		        w(i) = w(i) + aij*x(A_adapt%tab(6)%ja(k))
		    enddo
		    do k=A_adapt%tab(7)%ia(i), A_adapt%tab(7)%ia(i+1)-1
		        call convert_64F_64I(aij, A_adapt%tab(7)%a((7+1)*k-7))
		        w(i) = w(i) + aij*x(A_adapt%tab(7)%ja(k))
		    enddo
		enddo
		!$OMP END PARALLEL DO
    	call system_clock(t2,cr)
		tsolve = tsolve + dble(t2-t1)/dble(cr)
		
	end subroutine ax_adapt_7p
		
	subroutine spmv_error(n, w_nw, w_cw, refw, absAabsx, normAnormx, error_NW, error_CW)
		implicit none
		!arguments
		integer, intent(in) :: n
		double precision, intent(in) :: w_nw(n), w_cw(n)
		REAL(KIND=16), intent(in) :: refw(n)
		double precision, intent(in) :: absAabsx(n), normAnormx
		!output
		double precision, intent(out) :: error_NW, error_CW
		!local variables
		integer :: i
		
		error_CW=0.d0
		error_NW=0.d0
		do i=1,n
			error_NW = max(error_NW, abs(w_nw(i)-refw(i)))
			error_CW = max(error_CW, abs(w_cw(i)-refw(i))/absAabsx(i))
		enddo  
		error_NW=error_NW/(normAnormx)
	
	end subroutine spmv_error
end module spmv
