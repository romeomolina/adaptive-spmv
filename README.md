This repository contains fortran and C++ code to experiment the adaptive sparse matrix-vector product (apspmv).


Fortran folder contants
Folder SpMV contains the code that performs adaptive SpMV. It can be used by
- compiling with make command
- launch the programm by "main matrix.mtx" where matrix.mtx is a path to a matrix in .mtx format.

Folder GMRES contains the code that performs a GMRES algorithm using the adaptive SpMV with 2 precisions. It can be used by
- compiling with make command
- launch the programm by "main matrix.mtx" where matrix.mtx is a path to a matrix in .mtx format.


C++ folder contains two folders
Please first compile C++/rpfp/cpu/ folder then follow the instructions in the C++/apspmv/README file
